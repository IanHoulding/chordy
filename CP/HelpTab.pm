package CP::HelpTab;

use CP::Global qw/:OS :FUNC :XPM/;
use CP::Help;

sub help {
  my($win) = shift;

  if ($win eq '') {
    makeImage("checkbox", \%XPM);
    $win = CP::Help->new("Chordy Help");
    $win->add(
[
 "<O TO:H: Tab Editor >",
 "\nMy first pass at writing a Tab editor.\nAt some point I'll get round to writing the help section in full :)\n\nThe editor is split vertically with the the left hand side for editing a specific bar and the right hand side is a display of what the final PDF page will look like (more or less). The PDF page is not editable directly (except for lyrics) but individual bars can be selected for editing or multiple bars selected for duplication.\n",

 "<O To:H: Table Of Contents >",
 "<O ES:S:\nEdit Section>",
 "<O EB:B:Edit Buttons>",
 "<O ST:B:Edit Stave>",
 "<O FN:B:Fret Number/Rest>",
 "<O SH:B:Slide - Hammer>",
 "<O BR:B:Bend - Bend/Release>",
 "<O BO:B:Bar Options>",
 "<O PO:B:Page Options>",
 "<O TR:B:Transpose>",
 "<O PD:S:\nPDF Page Display>",
 "<O PB:B:Page Buttons>",
 "<O PD:B:Page Display>",
 "<O LY:B:Lyrics>",
 "<O CE:S:\nColour Editor>"
]);
    if ($OS eq "win32") {
      $win->add(
[
 "<O TP:S:\nTab Player> ",
]);
    }
    $win->add(
[
 "<T ES><H  Edit Section >",
 "\n",

 "<T EB><S Edit Buttons>\n",
 "These buttons affect what happens in or to the Edit Stave\n",
 "<b  }}}Cancel{{{ > ",
 "<M>Any items in the Edit Stave area and any selections in the PDF page are removed.",

 "<b  Clear Bar > ",
 "<M>Clears any items in the Edit Stave area.",

 "<b  Set Background > ",
 "<M>Allows you to define a background colour for this single Bar.",

 "<b  Clear Background > ",
 "<M>Clears any background colour from the Bar.",

 "<b  Insert Before > ",
 "<M>If you have selected a Bar from the PDF page, the current Bar will be saved and inserted <I before> the selected Bar.",

 "<b  Insert After > ",
 "<M>Same as above except the Bar will be inserted <I after> the selected Bar.",

 "<b  Update > ",
 "<M>Updates the Bar on the PDF page but remains in the Editor.",

 "<b  Save > ",
 "<M>If a PDF page Bar has been selected for editing it will be replaced otherwise the edited Bar will be tacked onto the end of any existing Bars.\n",

 "<T ST><S Edit Stave>\n",
 "This is where all the real work happens. The horizontal lines represent instrument strings - the lowest note string is at the bottom. The vertical lines (not either end of the stave) are the major beats in each bar - crotchets. The subdivisions then represent quavers, semiquavers and demisemiquavers.\nFrets/Rests are selected and placed by clicking on one of the horizontal/vertical intersections. They can then be deleted by right clicking on them or selected for moving by left clicking on them. If a Fret/Rest is selected it will turn <R red> and can then be moved to a new position by left clicking on the new position.\n",

 "<T FN><S Fret Number/Rest>\n",
 "Clicking on any one will select it for insertion into the Edit Stave.\n",

 "<T SH><S Slide - Hammer>\n",
 "Place 2 Fret Numbers on <I ONE> string - click on the <R Slide> or <R Hammer> button and then click each of the Fret Numbers.\n",

 "<T BR><S Bend - Bend/Release>\n",
 "These 2 are slightly different. For just a bend, click on the <R Bend> button and then click on the Fret Number you want bent. Bend/Release requires that you have 2 Fret Numbers on <I ONE> string (similar to Slide/Hammer) - click on the <R Bend/Release> button and then click each of the Fret Numbers.\n",

 "<T BO><S Bar Options>\n",
 "These are mainly to handle stuff other than Fret Numbers.\nPlace any text you want above the staff in the <D Header Text> box. This can be <D Left> or <D Right> justified and can have <D Volta brackets> included by selecting the appropriate button.\nThe <D Repeat> options place a start or end Repeat sign into the Bar.\nThe <D Note Font> option lets you reduce the size of the font - useful where frets are played very close together and would otherwise overlap.\nThe <D Bar start a New (Line/Page)> option does just that. It forces the Bar onto a new line or page.\n",

 "<T PO><S Page Options>\n",
 "The PDF page <D Tab File Name, PDF File Name> and <D Title> are inter-related in that, by default, the <D PDF File Name> is taken from the <D Tab File Name> with a <I .pdf> extension. The <D Title> is the same text but without an extension. The only immutable item is the <D Tab File Name>, both the other entries can be changed to your own preference. For instance, I use some software that displays a PDF on my tablet and links a song into its database via the file name. It's not unknown for the vocalist(s) to decide the key needs changing in which case I have a separate <D Tab File Name> which has the same <D PDF File Name> and <D Title> as the original - the database thinks the new file (with new key!) is still the original file and saves me having to manipulate the database or add a new song with a different title.",
 "The <I Key> is initially taken to be the first note played but can be over-ridden with an entry in the <D Set Key as> box.\nIf you want a short note to appear below the <I Key> entry at the top of the PDF page, edit the <D Heading Note> box.",
 "The <D Instrument> button just shows the correct number of strings in each bar.",
 "The <D Edit Scale> value makes the bar edit area bigger or smaller to make editing easier.",
 "The <D Bars per Stave> is also fairly obvious!",
 "<D Lyric Lines> sets the number of lyric lines below each Stave. They are shown as a background colour on the PDF page but that colour is <I NOT> transfered to the final PDF when generated.",
 "<D Stave Line Spacing> adjusts the distance between Stave (string) lines possibly making the Tab more (or less) readable.",
 "<D Lyric Spacing> adds extra space between Lyric lines but only if more that one is defined.",
 "<D Inter-Stave Gap> just adds extra spacing between any lyric lines and the top of the next Stave.\n",

 "<T TR><S Transpose>\n",
 "The <D Semi-tones> box lets you move the fret numbers up and down the scale in semitone increments. Normaly, if you transpose down and the fret number goes negative, the number will be shown in <R RED>. If you have the <D Adjust Negative Frets> box ticked the fret number will be moved down one string to the appropriate fret. The only exception is if you then run out of strings in which case the appropraite fret one octave higher will be shown.",
 "The <D One String> buttons move all the Fret numbers up or down one string. I put this in mainly for moving Tabs from a 4 string to a 5 string base and vice versa.\n",

 "<T PD><H  PDF Page Display >",
 "\n",

 "<T PB><S Page Buttons>\n",
 "<b  Edit > ",
 "<M>Click this after selecting a bar to transfer it to the Edit Section.",

 "<b  Clone > ",
 "<M>After selecting one or more bars this will copy the selection to the end of the current bars.",

 "<b  Copy > ",
 "<M>Copies the selected bar(s) to an internal buffer.\n",
 
 "<b  Paste Over > ",
 "<M>Any previously copied bars wil be pasted over the bars starting at the currently seleted bar.",

 "<b  Paste Before > ",
 "<M>As above but inserts the copied bars before the currently selected bar.",

 "<b  Paste After > ",
 "<M>As above but inserts the copied bars after the currently selected bar.\n",
 
 "<b  Clear Selection > ",
 "<M>If you've select one or more bars, This clears the selection.",

 "<b  Clear Bar(s) > ",
 "<M>Any selected bar(s) will be cleared of all Fret Numbers, Headings, etc.",

 "<b  Delete Bar(s) > ",
 "<M>Any selected bar(s) will be deleted and the trailing bars moved to fill the space vacated.\n",
 
 "<b  Set/Clear Background > ",
 "<M>Sets or Clears the background colour on one or more bars.\n",

 "<b  Lyrics > ",
 "<M>Moves all the Lyrics Up or Down one Stave. Any Lyrics that move up and off the first page will be placed after the last line of Lyrics.\n",

 "<b  {{{ > and <b  }}} > ",
 "<M>Move backward or forward one page.\n",

 "<T CE><H  Colour Editor > ",

 "  In the colour editor you get 3 sliders that go from 0 to 255 for each primary colour where 0 is no colour and 255 is lots. The resulting mix is shown in a box on the right of the window. This box can have both the background and foreground colours changed but what will be used depends on what mode the editor is in (see the heading top right).",
 "Below the sliders is an entry area where you can modify the Hex values for a given colour. If a colour matches one of those listed on the left, that name will appear in this entry box.\n",
 "Below this are three buttons which give you quick access to the current Chorus, Highlight and Comment colours.",
 "The \"My Colours\" box gives you the ability to mix and save 16 different colours you might want to use on a regular basis. Clicking on one of the 16 buttons will set that fore/back-ground colour. If you then change the colour with the sliders, you can change the selected colour swatch with the <R Set Colour> button. These colours are only saved if you hit the <R OK> button.\n",
]);
    if ($OS eq "win32") {
      $win->add(
[
 "<T TP><H  Tab Player >",
 "\n",
 "Use the slider to set the tempo of the piece (beats/minute). If no start or stop bar is selected then the whole piece will be played. If just the start is selected then play will continue to the end of the piece. If both are selected then the (inclusive) bars will be played. The same applies to the 'Loop' function. If you 'Pause' play, you can continue with either the 'Play' or the 'Loop' button and play will continue in that mode.\n"
]);
    }
  }
  $win->show();
  $win;
}

1;
