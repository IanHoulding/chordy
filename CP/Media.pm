package CP::Media;

# This file is part of Chordy.
#
###############################################################################
# Copyright (c) 2018 Ian Houlding
# All rights reserved.
# This program is free software.
# You can redistribute it and/or modify it under the same terms as Perl itself.
###############################################################################

use strict;

use CP::Cconst qw/:LENGTH :SMILIE :COLOUR/;
use CP::Global qw/:FUNC :OS :OPT :WIN :PRO :SETL :MEDIA :XPM/;
use Tkx;
use CP::Fonts;
use CP::Cmsg;

our %Fonts = (
  Comment   => {qw/size 16 weight normal slant roman  color #000000 family/ => "Times New Roman"},
  Highlight => {qw/size 16 weight normal slant italic color #000000 family/ => "Times New Roman"},
  Title     => {qw/size 16 weight bold   slant roman  color #700070 family/ => "Times New Roman"},
  Lyric     => {qw/size 16 weight normal slant roman  color #000000 family/ => "Arial"},
  Chord     => {qw/size 16 weight normal slant roman  color #700070 family/ => "Comic Sans MS"},
  Notes     => {qw/size  9 weight bold   slant roman  color #000000 family/ => "Tahoma"},
  SNotes    => {qw/size  5 weight bold   slant roman  color #000000 family/ => "Tahoma"},
  Header    => {qw/size  8 weight bold   slant roman  color #D00000 family/ => "Arial"},
  Words     => {qw/size  8 weight normal slant roman  color #00A000 family/ => "Times New Roman"},
);

our %BGs = (qw/commentBG   #E0E0E0
	       highlightBG #FFFF80
	       verseBG     #FFFFFF
	       chorusBG    #CDFFCD
	       bridgeBG    #FFFFFF/);

# Sizes are all stored in pt
our %Sizes = (
  'a3'        => {qw/width 842 height 1190/},
  'a4'        => {qw/width 595 height  842/},
  'a5'        => {qw/width 421 height  595/},
  'a6'        => {qw/width 297 height  421/},
  'b4'        => {qw/width 707 height 1000/},
  'b5'        => {qw/width 500 height  707/},
  'b6'        => {qw/width 353 height  500/},
  'iPad'      => {qw/width 420 height  560/},
  'iPadPro'   => {qw/width 560 height  745/},
  'letter'    => {qw/width 612 height  792/},
  'tabloid'   => {qw/width 792 height 1224/},
  'legal'     => {qw/width 612 height 1008/},
  'executive' => {qw/width 522 height  756/},
  'Samsung'   => {qw/width 465 height  792/},
);

my %Medias = ();

sub new {
  my($proto) = @_;

  my $class = ref($proto) || $proto;
  my $self = {};
  bless $self, $class;
  if (-e "$Path->{Media}") {
    load($self);
  } else {
    default($self);
    save($self);
  }
  $self;
}

sub default {
  my($self) = shift;

  %EditFont = (qw/family Arial size 14 weight normal slant roman color #A00000 background #FFF8E0 brace #008000 bracesz 12 bracket #008000 bracketsz 12 bracketoff 2/);

  copy(\%Sizes, \%Medias);
  foreach my $s (keys %Sizes) {
    copy(\%Fonts, \%{$Medias{$s}});
    copy(\%BGs, \%{$Medias{$s}});
  }
  $CurSet = '';
  # A4 is the default which is 8.27 × 11.69 inches
  # or 210mm x 297mm which we convert to points
  $Opt->{Media} = 'a4';
  copy(\%{$Medias{'a4'}}, $self);
}

sub list {
  sort keys %Medias;
}

sub change {
  my($self) = shift;

  if (defined $Medias{$Opt->{Media}}) {
    copy(\%{$Medias{$Opt->{Media}}}, $self);
  } else {
    my $m = (list())[0];
    copy(\%{$Medias{$m}}, $self);
    my $opt = CP::Opt->new();
    $opt->{Media} = $m;
    $opt->save();
    message(SAD, "Media definiton for $Opt->{Media} does not exist.\nMedia type changed to: $m", -1);
    $Opt->{Media} = $m;
  }
}

sub update {
  my($self) = shift;

  copy($self, \%{$Medias{$Opt->{Media}}});
}

sub save {
  my($self,$show) = @_;

  my $OFH = openConfig("$Path->{Media}");
  if ($OFH == 0) {
    errorPrint("Failed to open '$Path->{Media}': $@");
    return(0);
  }

  copy($self, $Medias{$Opt->{Media}});
  print $OFH "# This is the default Editor font.\n";
  print $OFH "\%EditFont = (\n";
  foreach my $k (sort keys %EditFont) {
    if ($k eq 'size' || $k =~ /sz$/ || $k =~ /off$/) {
      print $OFH "  $k => $EditFont{$k},\n";
    } else {
      print $OFH "  $k => '$EditFont{$k}',\n";
    }
  }
  print $OFH ");\n\n";

  print $OFH "#\n# width, height and size values are in 'points' (72 'points' per inch)\n#\n";
  print $OFH "\%medias = (\n";
  foreach my $s (sort keys %Medias) {
    next if ($s =~ /^tmp/);
    print $OFH "  '$s' => {\n";
    print $OFH "    width  => ".$Medias{$s}{width}.",\n";
    print $OFH "    height => ".$Medias{$s}{height}.",\n";
    foreach my $bg (qw/commentBG highlightBG verseBG chorusBG bridgeBG titleBG/) {
      printf $OFH "    %-11s => '%s',\n", $bg, $Medias{$s}{$bg};
    }
    foreach my $f (qw/Chord Comment Highlight Lyric Title Notes SNotes Header Words/) {
      my $fptr = \%{$Medias{$s}{$f}};
      printf $OFH ("    %-9s => {qw/size %-2d weight %-6s slant %-6s color %s family/ => '%s'},\n",
		  $f, $fptr->{size}, $fptr->{weight}, $fptr->{slant}, $fptr->{color}, $fptr->{family});
    }
   print $OFH "  },\n";
  }
  print $OFH ");\n\n1;\n";
  close($OFH);
  message(SMILE, "Media Config Saved", 1) if (defined $MW && $show);
}

sub copy {
  my($src,$dst) = @_;

  foreach my $k (keys %{$src}) {
    next if ($k =~ /^tmp/);
    if (ref($src->{$k}) eq 'HASH') {
      foreach my $kf (keys %{$src->{$k}}) {
	$dst->{$k}{$kf} = $src->{$k}{$kf};
      }
    } else {
      $dst->{$k} = $src->{$k};
    }
  }
}

sub load {
  my($self) = shift;

  our(%medias);
  unless (do "$Path->{Media}") {
    errorPrint("do '$Path->{Media}' failed: $@");
  } else {
    foreach my $k (keys %medias) {
      copy(\%{$medias{$k}}, \%{$Medias{$k}});
      $Medias{$k}{titleBG} = WHITE if (! defined $medias{$k}{titleBG});
      $Medias{$k}{verseBG} = WHITE if (! defined $medias{$k}{verseBG});
      $Medias{$k}{bridgeBG} = WHITE if (! defined $medias{$k}{bridgeBG});
    }
    undef %medias;
    change($self);
    my $sz = $EditFont{size} - 2;
    $EditFont{brace} = DRED if (! defined $EditFont{brace});
    $EditFont{bracesz} = $sz if (! defined $EditFont{bracesz});
    $EditFont{bracket} = DGREEN if (! defined $EditFont{bracket});
    $EditFont{bracketsz} = $sz if (! defined $EditFont{bracketsz});
    $EditFont{bracketoff} = 2 if (! defined $EditFont{bracketoff});
    $EditFont{background} = VLMWBG if (! defined $EditFont{background});
  }
}

our($Done);

sub edit {
  my($self) = @_;

  our($a,$b,$c,$d,$e,$f,$g,$h,$i,$j,$k,$m,$n,$o,$wstr,$hstr);

  my($top,$wt) = popWin(0, 'Media Editor');
  $top->g_wm_protocol(
    'WM_DELETE_WINDOW',
    sub {copy($Medias{$Opt->{Media}}, $self); $top->g_destroy();});

  my $tf = $wt->new_ttk__frame(qw/-borderwidth 2 -relief ridge/);
  $tf->g_pack(qw/-side top -expand 1 -fill x/);

  my $bf = $wt->new_ttk__frame();
  $bf->g_pack(qw/-side top -expand 1 -fill x/);

  $self->{tmpU} = $self->{tmpNewU} = 'pt';
  $self->{tmpW} = $self->{width};
  $self->{tmpH} = $self->{height};
  $self->{tmpMedia} = $Opt->{Media};
  $self->{tmpNewName} = '';
  changeUnits($self);

  $a = $tf->new_ttk__label(-text => "Media:");
  $b = $tf->new_ttk__button(
    -width => 20,
    -textvariable => \$self->{tmpMedia},
    -style => 'Menu.TButton',
    -command => sub{
      my @lst = (sort keys %Medias);
      popMenu(\$self->{tmpMedia}, sub{changeMedia($self)}, \@lst);
    });

  $c = $tf->new_ttk__button(qw/-text Delete -width 8 -command/ => sub{mdelete($self)} );

  $d = $tf->new_ttk__label(-text => "Width: ");
  $e = $tf->new_ttk__entry(qw/-width 6 -textvariable/ => \$self->{tmpW});
  $f = $tf->new_ttk__button(
    -width => 5,
    -textvariable => \$self->{tmpU},
    -style => 'Menu.TButton',
    -command => sub{
      popMenu(\$self->{tmpNewU}, sub{changeUnits($self)}, [qw/in mm pt/]);
    });

  $g = $tf->new_ttk__label(-text => "Height: ", -width => 10, -anchor => 'e');
  $h = $tf->new_ttk__entry(qw/-width 6 -textvariable/ => \$self->{tmpH});
  $i = $tf->new_ttk__button(
    -width => 5,
    -textvariable => \$self->{tmpU},
    -style => 'Menu.TButton',
    -command => sub{
      popMenu(\$self->{tmpNewU}, sub{changeUnits($self)}, [qw/in mm pt/]);
    });

  $j = $tf->new_ttk__separator(qw/-orient horizontal/);

  $k = $tf->new_ttk__label(-text => "New Media Name: ");
  $m = $tf->new_ttk__entry(qw/-width 16 -textvariable/ => \$self->{tmpNewName});

  $n = $tf->new_ttk__button(qw/-text New -width 5 -command/ => sub{mnew($self)} );
  $o = $tf->new_ttk__button(qw/-text Rename -width 8 -command/ => sub{mrename($self)} );

  $a->g_grid(qw/-row 0 -column 0 -sticky e/);
  $b->g_grid(qw/-row 0 -column 1 -sticky w -columnspan 3/);
  $c->g_grid(qw/-row 0 -column 4 -columnspan 2 -pady 4/);

  $d->g_grid(qw/-row 1 -column 0 -sticky e/);
  $e->g_grid(qw/-row 1 -column 1 -sticky we/);
  $f->g_grid(qw/-row 1 -column 2 -sticky w -padx 4/);
  $g->g_grid(qw/-row 1 -column 3 -sticky e/);
  $h->g_grid(qw/-row 1 -column 4 -sticky we/);
  $i->g_grid(qw/-row 1 -column 5 -sticky w -padx 4/);

  $j->g_grid(qw/-row 2 -column 0 -sticky we -columnspan 6 -pady 4/);

  $k->g_grid(qw/-row 3 -column 0 -sticky e/, -pady => [0,4]);
  $m->g_grid(qw/-row 3 -column 1 -columnspan 2/, -pady => [0,4]);
  $n->g_grid(qw/-row 3 -column 3/, -pady => [0,4]);
  $o->g_grid(qw/-row 3 -column 4 -sticky w -columnspan 2/, -pady => [0,4]);

  ($bf->new_ttk__button(
     -text => "Cancel",
     -command => sub{$Done = "Cancel";},
      ))->g_grid(qw/-row 0 -column 0 -sticky w -padx 40/, -pady => [4,2]);

  ($bf->new_ttk__button(
     -text => "OK",
     -command => sub{$Done = "OK";},
      ))->g_grid(qw/-row 0 -column 1 -sticky e -padx 40/, -pady => [4,2]);

  Tkx::vwait(\$Done);
  if ($Done eq "OK") {
    $Opt->{Media} = $self->{tmpMedia};
    $self->{tmpNewU} = 'pt';
    changeUnits($self);
    $self->{width} = $self->{tmpW};
    $self->{height} = $self->{tmpH};
    # The "save" copies $self into %Medias
    save($self);
    my $opt = CP::Opt->new();
    $opt->{Media} = $self->{tmpMedia};
    $opt->save();
#    message(SMILE, "Media list updated and saved.", 1);
  } else {
    copy(\%{$Medias{$Opt->{Media}}}, $self);
  }
  $top->g_destroy();
  $Done;
}

sub mediaItems {
  my($self) = shift;

  my $list = $self->{tmpMedList};
  my $x = $list->cget(-menu);
  $x->delete(0, 'end');
  foreach (list()) {
    $list->radiobutton(-label => $_, -variable => \$self->{tmpMedia}, -command => sub{changeMedia($self)} );
  }
}

sub changeMedia {
  my($self) = shift;

  copy(\%{$Medias{$self->{tmpMedia}}}, $self);
  $self->{tmpU} = $self->{tmpNewU} = 'pt';
  $self->{tmpW} = $self->{width};
  $self->{tmpH} = $self->{height};
  changeUnits($self);
}

sub changeUnits {
  my($self) = shift;

  my $newu = $self->{tmpNewU};
  my $w = $self->{tmpW};
  my $h = $self->{tmpH};
  # Convert from current units to points then to new units.
  if ($self->{tmpU} ne 'pt') {
    my $un = ($self->{tmpU} eq 'mm') ? MM : IN;
    $w = int($w / $un);
    $h = int($h / $un);
  }
  # Now in points ..
  if ($newu ne 'pt') {
    my $un = ($newu eq 'mm') ? MM : IN;
    $w *= $un;
    $h *= $un;
    if ($newu eq 'mm') {
      $w = int($w * 10) / 10;
      $h = int($h * 10) / 10;
    } else {
      $w = int($w * 100) / 100;
      $h = int($h * 100) / 100;
    }
  }
  $self->{tmpW} = $w;
  $self->{tmpH} = $h;
  $self->{tmpU} = $newu;
}

sub mdelete {
  my($self) = shift;

  if (CP::Cmsg::msgYesNo("Do you really want to\ndelete Media: $self->{tmpMedia}") eq "Yes") {
    delete($Medias{$self->{tmpMedia}});
    $self->{tmpMedia} = (list())[0];
    copy(\%{$Medias{$self->{tmpMedia}}}, $self);
    $self->{tmpU} = $self->{tmpNewU} = 'pt';
    $self->{tmpW} = $self->{width};
    $self->{tmpH} = $self->{height};
    changeUnits($self);
    save($self);
  }
}

sub mnew {
  my($self) = shift;

  my $nn = $self->{tmpNewName};
  if ($nn ne '') {
    if (defined $Medias{$nn}) {
      message(SAD, "Media type '$nn' already exists!");
    } else {
      $Medias{$nn} = {};
      copy(\%{$Medias{a4}}, \%{$Medias{$nn}});
      copy(\%{$Medias{$nn}}, $self);
      $self->{tmpU} = $self->{tmpNewU} = 'pt';
      $self->{tmpW} = $Medias{$nn}{width};
      $self->{tmpH} = $Medias{$nn}{height};
      $self->{tmpMedia} = $Opt->{Media} = $nn;
      $self->{tmpNewName} = '';
      changeUnits($self);
      save($self);
    }
  }
}

sub mrename {
  my($self) = shift;

  my $nn = $self->{tmpNewName};
  if ($nn ne '') {
    if (defined $Medias{$nn}) {
      message(SAD, "Media type '$nn' already exists!");
    } else {
      $Medias{$nn} = {};
      copy($self, \%{$Medias{$nn}});
      foreach my $k (keys %{$self}) {
	if ($k =~ /^tmp/) {
	  $Medias{$nn}{$k} = $self->{$k};
	}
      }
      delete($Medias{$self->{tmpMedia}});
      $self->{tmpMedia} = $Opt->{Media} = $nn;
      $self->{tmpNewName} = '';
      save($self);
    }
  }
}

sub fonts {
  my($self) = shift;

  my($top,$wt) = popWin(0, 'Font Selector', Tkx::winfo_rootx($MW) + 10, Tkx::winfo_rooty($MW) + 10);
  $wt->m_configure(qw/-relief raised -borderwidth 2/);

  my $tf = $wt->new_ttk__frame(qw/-borderwidth 2 -relief ridge/, -padding => [4,4,4,4]);
  $tf->g_pack(qw/-side top/);

  my $bf = $wt->new_ttk__frame();
  $bf->g_pack(qw/-side top -expand 1 -fill x/);

  makeImage("tick", \%XPM);

  CP::Fonts::fonts($tf, [qw/Title Header Notes SNotes Words/]);

  (my $can = $bf->new_ttk__button(-text => "Cancel", -command => sub{$Done = "Cancel";})
  )->g_grid(qw/-row 0 -column 0 -padx 60/, -pady => [4,0]);
  (my $ok = $bf->new_ttk__button(-text => "OK", -command => sub{$Done = "OK";})
  )->g_grid(qw/-row 0 -column 1 -sticky e -padx 60/, -pady => [4,0]);

  Tkx::vwait(\$Done);
  if ($Done eq "OK") {
    save($self);
#    message(SMILE, "Font list updated and saved.", 1);
  }
  $top->g_destroy();
  $Done;
}

sub pickClr {
  my($title,$fontp,$clr,$ent) = @_;

  $ColourEd = CP::FgBgEd->new() if (! defined $ColourEd);
  $ColourEd->title("$title Font");
  my($fg,$bg) = $ColourEd->Show($fontp->{color}, VLMWBG, FOREGRND);
  if ($fg ne '') {
    $fontp->{color} = $fg;
    $ent->m_configure(-fg => $fg);
    $clr->m_configure(-bg => $fg);
  }
}

1;
