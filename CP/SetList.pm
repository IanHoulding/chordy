package CP::SetList;

# This file is part of Chordy.
#
###############################################################################
# Copyright (c) 2018 Ian Houlding
# All rights reserved.
# This program is free software.
# You can redistribute it and/or modify it under the same terms as Perl itself.
###############################################################################

use strict;

use CP::Cconst qw/:SMILIE :BROWSE/;
use CP::Global qw/:FUNC :OS :OPT :PRO :SETL/;
use CP::Cmsg;

sub new {
  my($proto) = @_;
  my $class = ref($proto) || $proto;
  my $self = {};
  bless $self, $class;

  if (-e "$Home/SetList.cfg") {
    our %list;
    do "$Home/SetList.cfg";
    foreach (sort keys %list) {
      $self->{sets}{$_} = $list{$_};
    }
  }
  $self->{setsLB} = '';
  $self->{browser} = '';
  $CurSet = '';

  return $self;
}

sub save {
  my ($self) = shift;

  my $SL = "$Home/SetList";
  if (-e "${SL}.bak") {
    unlink("${SL}.bak");
  }
  if (-e "${SL}.cfg") {
    rename("${SL}.cfg", "${SL}.bak");
  }
  unless (open OFH, ">${SL}.cfg") {
    message(SAD, "Couldn't create Set List file. (${SL}.cfg)");
    if (-e "${SL}.bak") {
      rename( "${SL}.bak", "${SL}.cfg");
    }
    return(0);
  }
  print OFH "#!/usr/bin/perl\n\n";
  print OFH "\%list = (\n";
  foreach my $c (keys %{$self->{sets}}) {
    if ($c ne '') {
      print OFH "  \"$c\" => [\n";
      foreach (@{$self->{sets}{$c}}) {
	print OFH '    "'.$_."\",\n";
      }
      print OFH "  ],\n";
    }
  }
  print OFH ");\n1;\n";
  close(OFH);
  message(SMILE, "Set Lists updated and saved.", 1);
}

sub change {
  my($self) = shift;

  my $slb = $self->{setsLB};
  my $br = $self->{browser};
  $AllSets = CP::SetList->new();
  $slb->{array} = [sort keys %{$AllSets->{sets}}];
  $slb->a2tcl();
  $AllSets->{setsLB} = $slb;
  $AllSets->{browser} = $br;
  $br->reset();
  $CurSet = $AllSets->{curSet} = '';
}

#
# This is called from New, Rename or Clone
sub setNRC {
  my($self,$what,$newname) = @_;

  my $oldname = $CurSet;
  if (defined $self->{sets}{$newname}) {
    message(SAD, "Set List \"$newname\" already exists\nPlease choose another name.");
  } else {
    if ($newname ne '') {
      if ($what & (SLCLN | SLREN)) {
	# Copy the selected Set List into a new one
	$self->{sets}{$newname} = $self->{sets}{$oldname};
	if ($what == SLREN) {
	  delete $self->{sets}{$oldname};
	}
      } else {
	# New
	$self->{sets}{$newname} = [];
      }
      $CurSet = $newname;
      $self->{setsLB}{array} = [sort keys %{$self->{sets}}];
      $self->{setsLB}->a2tcl();
    } else {
      my $w = 'You need a Name ';
      if    ($what == SLNEW) {$w .= 'for the New Set!';}
      elsif ($what == SLCLN) {$w .= 'for the Cloned Set!';}
      elsif ($what == SLREN) {$w .= 'to Rename the Set to!';}
      message(QUIZ, $w);
      return;
    }
  }
}

sub delete {
  my($self) = @_;

  my $todel = $CurSet;
  if ($todel ne '') {
    return if (msgYesNo("Are you sure you want to delete Set '$todel'?") =~ /no/i);
    delete $self->{sets}{$todel};
    $self->{setsLB}{array} = [sort keys %{$self->{sets}}];
    $self->{setsLB}->a2tcl();
    $CurSet = '';
  }
}

sub export {
  my($self) = shift;

  return if ($CurSet eq '');
  my $newC = my $orgC = $Collection->name();
  my @lst = (keys %{$Collection});
  popMenu(
    \$newC,
    sub{
      if ($newC ne $orgC) {
	my $orgHome = $Home;
	$Home = "$Collection->{$newC}/$newC";
	my $curSet = $CurSet;  # $CurSet gets blatted by new()
	my $sl = CP::SetList->new();
	$CurSet = $curSet;
	if (exists $sl->{sets}{$CurSet}) {
	  my $ans = msgYesNoAll("The Set List:    \"$CurSet\"\nalready exists in Collection:\n    \"$newC\"\nDo you want to overwrite it?");
	  undef $sl, return if ($ans eq "No");
	}
	$sl->{sets}{$CurSet} = $self->{sets}{$CurSet};
	$sl->save();
	undef $sl;
	$Home = $orgHome;
      }
    },
    \@lst);
}

1;
