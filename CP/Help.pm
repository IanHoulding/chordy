package CP::Help;

# This file is part of Chordy.
#
###############################################################################
# Copyright (c) 2018 Ian Houlding
# All rights reserved.
# This program is free software.
# You can redistribute it and/or modify it under the same terms as Perl itself.
###############################################################################

use strict;
use warnings;

use CP::Global qw/:FUNC :OS :OPT :WIN :MEDIA/;
use CP::Cconst qw/:COLOUR/;
use Tkx;

sub new {
  my($proto,$title) = @_;
  my $class = ref($proto) || $proto;

  my $self = {};
  bless $self, $class;

  $self->{mark} = {};
  my($win,$tf) = popWin(0, $title, Tkx::winfo_rootx($MW) + 10, Tkx::winfo_rooty($MW) + 10);
  $self->{win} = $win;
  $win->g_wm_protocol('WM_DELETE_WINDOW' => sub{$win->g_wm_withdraw()} );

  my $textf = $tf->new_ttk__frame();
  $textf->g_pack(qw/-side top -expand 1 -fill both/);

  $self->{text} = $textf->new_tk__text(
    -font => "Times 13 normal",
    -bg => 'white',
    -wrap => 'word',
    -borderwidth => 2,
    -width => 80,
    -height => 30,
    -spacing1 => 1,
    -spacing3 => 1,
    -undo => 0,
    -setgrid => 'true');
  $self->{text}->g_pack(qw/-side left -expand 1 -fill both/);

  my $scv = $textf->new_ttk__scrollbar(-orient => "vertical", -command => [$self->{text}, "yview"]);
  $scv->g_pack(qw/-side left -expand 1 -fill y/);
  $self->{text}->m_configure(-yscrollcommand => [$scv, 'set']);

  my $bf = $tf->new_ttk__frame();
  $bf->g_pack(qw/-side bottom -fill x/);

  my $bc = $bf->new_ttk__button(-text => "Close", -command => sub{$win->g_wm_withdraw()});
  $bc->g_pack(qw/-side left -padx 30 -pady 4/);

  my $bt = $bf->new_ttk__button(-text => " Top ", -command => sub{$self->{text}->see("1.0")});
  $bt->g_pack(qw/-side right -padx 30 -pady 4/);

  my $textWin = $self->{text};
  my $sz = 13;
  $textWin->tag_configure('H', -font => "Times ".($sz+5)." bold", -lmargin1 => 0,
			  -foreground => HFG, -background => SELECT);
  $textWin->tag_configure('S', -font => "Arial ".($sz+1)." bold", -lmargin1 => 0,
			  -spacing3 => 4,
			  -underline => 1, -foreground => HFG);
  $textWin->tag_configure('I', -font => "Arial ".($sz-1)." bold", -lmargin1 => 0,
			  -foreground => HFG);
  $textWin->tag_configure('b', -font => "Arial ".($sz-2)." bold", -lmargin1 => 0,
			  -foreground => $Opt->{PushFG}, -background => $Opt->{PushBG},
			  -relief => 'raised', -borderwidth => 2,
			  -spacing1 => 3, -spacing3 => 2);
  $textWin->tag_configure('B', -font => "Courier ".$sz." bold");
  $textWin->tag_configure('C', -font => "Courier ".($sz-2)." bold");
  $textWin->tag_configure('U', -font => "Courier ".($sz-3)." bold",
			  -offset => int($sz/3));
  $textWin->tag_configure('A', -font => "Times $sz bold italic");
  $textWin->tag_configure('D', -font => "Times $sz bold");
  $textWin->tag_configure('R', -font => "Times $sz bold",
			  -foreground => RFG);
  $textWin->tag_configure('Y', -font => "Times $sz bold italic",
			  -background => "$Media->{highlightBG}");
  $textWin->tag_configure('L', -font => "Times $sz normal",
			  -background => "$Media->{commentBG}");
  $textWin->tag_configure('P', -font => "Times $sz normal",
			  -background => "#FF00FF");
  $textWin->tag_configure('M', -lmargin1 => 24, -lmargin2 => 0);
  $textWin->tag_configure('Mi', -lmargin1 => 24, -lmargin2 => 24);
  $textWin->tag_configure('Im', -background => $Opt->{PushBG},
			  -relief => 'solid',
			  -borderwidth => 1,
			  -spacing1 => 0, -spacing3 => 0);

  $self->{text}->configure(-state => 'disabled');
  $self;
}

sub show {
  my($self) = shift;

  $self->{win}->g_wm_deiconify();
  $self->{win}->g_raise();
  Tkx::update();
}

sub add  {
  my($self, $help) = @_;

  $self->{text}->configure(-state => 'normal');
  my $textWin = $self->{text};
  foreach (@{$help}) {
    my $ind = 'M';
    my @txt = split('', $_);
    my $text = '';
    while (@txt) {
      my $c = shift(@txt);
      if ($c eq '<') {
	my $tag = shift(@txt);
	$c = shift(@txt);
	if ($c eq ' ') {
	  my $tagged = '';
	  while (@txt && ($c = shift(@txt)) ne '>') {
	    $tagged .= $c;
	  }
	  if ($text ne '') {
	    $textWin->insert('end', $text, [$ind]);
	    $text = '';
	  }
	  if ($tag eq 'O') { # TOC
	    #    --------tagged--------
	    # <O TO:H:Table Of Contents>
	    my($tname,$htag,$hdg) = split(':', $tagged, 3);
	    my $toc = "TOC$tname";
	    $textWin->tag_configure($toc);
	    if ($tname !~ /to/i) {
	      $textWin->tag_bind(
		$toc,
		"<ButtonRelease-1>",
		sub {
		  $textWin->yview($self->{mark}{$tname});
		  $textWin->yview_scroll(-2, 'units');
		});
	    }	    
	    # Setup Bindings to change cursor when over that line
	    $textWin->tag_bind($toc, "<Enter>", sub { $textWin->configure(-cursor => 'hand2') });
	    $textWin->tag_bind($toc, "<Leave>", sub { $textWin->configure(-cursor => 'xterm') });
	    if ($htag =~ /H|S/) {
	      $ind = '';
	    }
	    $textWin->insert('end', " ", '');
	    $textWin->insert('end', "$hdg", [$htag, $toc, $ind]);
	    $textWin->insert('end', "\n", '');
	    if ($tname eq "TO") {
	      $textWin->mark_set("TopM", $textWin->index('current'));
	    }
	  } elsif ($tag eq 'T') { # TAG
	    my $ln = $textWin->index('current');
	    $textWin->mark_set($tagged, $ln);
	    $ln =~ s/\.\d+//;
	    $self->{mark}{$tagged} = $ln;
	  } elsif ($tag eq 'X') {
	    $textWin->insert('end', " ", '');
	    my $index = $textWin->index('current');
	    $textWin->image_create($index, -image => $tagged, qw/-align center -padx 2 -pady 2/);
	    my($line,$col) = split(/\./, $index);
	    $textWin->tag_add('Im', $index, $line.".".++$col);
	  } else {
	    $tagged =~ s/\{\{\{/<<</g;
	    $tagged =~ s/\}\}\}/>>>/g;
	    $textWin->insert('end', $tagged, [$tag]);
	  }
	} elsif ($c eq '>') {
	  if ($tag eq 'M') {
	    $ind = 'Mi';
	  }
	} else {
	  $textWin->insert('end', "<$tag$c", [$ind]);
	}
      } else {
	$text .= $c;
      }
    }
    $textWin->insert('end', "$text\n", [$ind]) if ($text ne '');
  }
  $self->{text}->configure(-state => 'disabled');
}

1;
