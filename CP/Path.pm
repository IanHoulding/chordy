package CP::Path;

# This file is part of Chordy.
#
###############################################################################
# Copyright (c) 2018 Ian Houlding
# All rights reserved.
# This program is free software.
# You can redistribute it and/or modify it under the same terms as Perl itself.
###############################################################################

use strict;

use File::Path qw(make_path);
use CP::Cconst qw/:PATH/;
use CP::Global qw/:FUNC :OS/;

sub new {
  my($proto) = @_;
  my $class = ref($proto) || $proto;

  #
  # The $Home paths change depending on the Collection being used.
  # The USER paths are fixed as they are global to all Collections.
  #
  my $self = {
    Temp    => "$Home/Temp",       #
    Pro     => "$Home/Pro",        #
    Tab     => "$Home/Tab",        # These are the defaults but can be
    PDF     => "$Home/PDF",        # changed on a per Collection basis
    Option  => "$Home/Option.cfg", #
    Swatch  => "$Home/Swatch.cfg", #
    SMTP    => USER."/SMTP.cfg",
    Command => USER."/Command.cfg",
    Media   => USER."/Media.cfg",
  };
  $self->{Font} = ($OS eq "win32") ? "C:/Windows/Fonts" :
      ($OS eq "aqua") ? "/Library/Fonts" : "/usr/share/fonts/truetype";
  if (! -d $self->{Font}) {
    errorPrint("Don't know where your Fonts are - tried: '$self->{Font}'\n".
	       "  Please submit a bug detailing OS type and version\n".
	       "  and this Chordy version ($Version) - thanks.");
    exit(1);
  }
  bless $self, $class;
  check_dir($self);
  if (-e PROG."/Release Notes.txt") {
    my $txt = read_file(PROG."/Release Notes.txt");
    write_file(USER."/Release Notes.txt", $txt);
    unlink(PROG."/Release Notes.txt");
  }
  return($self);
}

sub change {
  my($self,$home) = @_;

  foreach (qw/Option Swatch/) {
    $self->{$_} = "$home/$_.cfg";
  }
  foreach (qw/Pro Tab PDF/) {
    $self->{$_} = "$home/$_";
  }
  check_dir($self);
}

sub check_dir {
  my($self) = shift;

  if ($OS eq 'aqua') {
    if (! -d $self->{Pro}) {
      make_path("$self->{Pro}", {chmod => 0644});
      my $src = "/Applications/Chordy.app/Contents/Resources/Pro";
      if (-d $src) {
	opendir DIR, "$src";
	foreach my $f (grep /\.pro$/, readdir DIR) {
	  my $txt = read_file("$src/$f");
	  write_file("$self->{Pro}/$f", $txt);
	}
	closedir(DIR);
      }
    }
  } else {
    make_path("$self->{Pro}", {chmod => 0644}) if (! -d "$self->{Pro}");
  }
  foreach my $path (qw/Tab PDF Temp/) {
    make_path("$self->{$path}", {chmod => 0644}) if (! -d "$self->{$path}");
  }
}

1;
