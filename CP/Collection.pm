package CP::Collection;

# This file is part of Chordy.
#
###############################################################################
# Copyright (c) 2018 Ian Houlding
# All rights reserved.
# This program is free software.
# You can redistribute it and/or modify it under the same terms as Perl itself.
###############################################################################

use strict;
use warnings;

use CP::Global qw/:FUNC :OS :WIN :OPT :PRO :MEDIA :SETL/;
use CP::Cconst qw/:PATH :SMILIE :COLOUR/;
use Tkx;
use File::Path qw(make_path remove_tree);
use CP::Cmsg;

#
# A 'Collection' is a very simple Object and
# is a HASH of 'name/path' key/data pairs stored in Chordy.cfg
#
# Our $CurrentCollection keeps track of the currently selected Collection.
#
my $CollectionPath;
my $CurrentCollection;
my $NewCollection;

sub new {
  my($proto) = @_;
  my $class = ref($proto) || $proto;

  my $self = {};
  bless $self, $class;
  if (-e USER."/Chordy.cfg") {
    load($self);
  } else {
    $CurrentCollection = 'Chordy';
    $self->{Chordy} = $Parent;
    save($self);
  }
  return($self);
}

sub name {
  my($self,$nn) = @_;

  if (defined $nn) {
    change($self, $nn);
  }
  $CurrentCollection;
}

sub change {
  my($self,$name) = @_;

  $CurrentCollection = $name;
  $Parent = $self->{$name};
  $Home = "$Parent/$name";
  $Path->change($Home);
  $Opt->load();
  $Media->change();
  $Swatches->load();
  if (defined $MW && Tkx::winfo_exists($MW) && defined $AllSets) {
    $AllSets->change();
  }
  my @del = ();
  foreach my $idx (0..$#ProFiles) {
    if (! -e "$Path->{Pro}/$ProFiles[$idx]->{name}") {
      push(@del, $idx);
    } else {
      $ProFiles[$idx] = CP::Pro->new($ProFiles[$idx]->{name});
    }
  }
  while (@del) {
    my $idx = pop(@del);
    splice(@ProFiles, $idx, 1);
    $KeyLB->remove($idx);
    $FileLB->remove($idx);
  }
  save($self);
}

sub add {
  my($self, $name) = @_;

  if ($name eq "") {
    message(SAD, "Can't add a new Collection without a name!");
    return(0);
  } else {
    my $path = Tkx::tk___chooseDirectory(
      -initialdir => "$Home",
      -title => "Select parent folder you want\nthe Collection to be in:");
    $path =~ s/\/$//;
    if (defined $path) {
      if (-e "$path/$name") {
	if (msgYesNo("This Collection Folder already exists:\n   $path/$name\nDo you want to over-write it?") eq 'No') {
	  return(0);
	}
      }
      my $err;
      make_path("$path/$name", {chmod => 0777, error => \$err});
      if ($err && @$err) {
	my ($file, $message) = %{$err->[0]};
	errorPrint("Problem creating folder '$file':\n   $message\n");
	message(SAD, "Failed to make path:\n$path\n\n(See error log for details)");
	return(0);
      } else {
	foreach my $d (qw/ PDF Pro Tab/) {
	  make_path("$path/$name/$d", {chmod => 0777});
	}
	foreach my $f (qw/Option.cfg SetList.cfg SMTP.cfg Swatch.cfg/) {
	  if (-e "$Home/$f") {
	    my $txt = read_file("$Home/$f");
	    write_file("$path/$name/$f", $txt);
	  }
	}
	$self->{$name} = $path;
	change($self, $name);
	return(1);
      }
    }
  }
  return(0);
}

sub newname {
  my($self,$name) = @_;

  if ($name eq "") {
    message(SAD, "Can't rename a Collection without a name!");
    return(0);
  } else {
    if (exists $self->{$name}) {
      message(SAD, "Sorry, that Collection already exists!");
      return(0);
    }
    if ($CurrentCollection eq 'Chordy') {
      message(SAD, "Sorry, the one Collection you CAN'T rename is Chordy!");
      return(0);
    }
    rename("$Home", "$Parent/$name");
    # $Parent doesn't change.
    $self->{$name} = $Parent;
    delete $self->{$CurrentCollection};
    change($self, $name);
  }
  return(1);
}

sub _delete {
  my($self, $name, $delorg) = @_;

  if (scalar keys %{$self} == 1) {
    message(SAD, "You can't delete the ONLY Collection!");
  } else {
    my $msg = "Are you sure you want to delete Collection:\n   $name";
    $msg .= "\nand ALL its associated files?" if ($delorg);
    if (msgYesNo($msg) eq 'Yes') {
      if ($delorg) {
	if ($name eq 'Chordy') {
	  # Special case - just clean up the folder but leave the
	  # "Global" config files
	  remove_tree(USER."/PDF");
	  remove_tree(USER."/Pro");
	  remove_tree(USER."/Tab");
	  unlink glob USER."/SetList*";
	  unlink glob USER."/*.bak";
	} else {
	  remove_tree("$Home");
	}
      }
      delete $self->{$name};
      my @c = sort keys %{$self};
      my $n = shift(@c);
      change($self, $n);
    }
  }
}

sub _move {
  my($self,$name,$del) = @_;

  my($top,$wt) = popWin(1, '');

  my $tf = $wt->new_ttk__frame();
  $tf->g_grid(qw/-row 0 -column 0 -sticky nsew -padx 4 -pady 6/);

  my $hl = $wt->new_ttk__separator(-orient => 'horizontal');
  $hl->g_grid(qw/-row 1 -column 0 -sticky ew/);

  my $bf = $wt->new_ttk__frame();
  $bf->g_grid(qw/-row 2 -column 0 -sticky ew -padx 4 -pady 6/);

  my($done,$to);
  my $fl = $tf->new_ttk__label(-text => 'From:');
  $fl->g_grid(qw/-row 0 -column 0 -sticky e/, -pady => [0,4]);

  my $from = "$self->{$name}/$name";
  my $fe = $tf->new_ttk__entry(
    -textvariable => \$from,
    -state => 'disabled',
    -width => 50);
  $fe->g_grid(qw/-row 0 -column 1 -sticky w/, -pady => [0,4]);

  my $tl = $tf->new_ttk__label(-text => 'To:');
  $tl->g_grid(qw/-row 1 -column 0 -sticky e/, -pady => [0,4]);

  my $te = $tf->new_ttk__entry(
    -textvariable => \$to,
    -width => 50);
  $te->g_grid(qw/-row 1 -column 1 -sticky w/, -pady => [0,4]);

  my $tb = $tf->new_ttk__button(
    -text => "Browse ...",
    -command => sub {
      $to = Tkx::tk___chooseDirectory(
	-title => "Choose Destination Folder",
	-initialdir => "$Home");
      $to =~ s/\/$//;
      $top->g_raise();
    });
  $tb->g_grid(qw/-row 1 -column 2 -sticky w -padx 6/, -pady => [0,4]);

  my $cancel = $bf->new_ttk__button(-text => "Cancel", -command => sub{$done = "Cancel";});
  $cancel->g_grid(qw/-row 0 -column 0 -padx 60/);

  my $ok = $bf->new_ttk__button(-text => "Go", -command => sub{$done = "GO";});
  $ok->g_grid(qw/-row 0 -column 1 -padx 60/);

  $top->g_raise();
  Tkx::vwait(\$done);

  if ($done eq 'GO') {
    if (defined $to && $to ne '') {
      $cancel->g_destroy();
      $ok->m_configure(-text => 'OK');
      $to =~ s/\/$//;
      my $lf = $tf->new_ttk__labelframe(-text => " Folders and Files moved ");
      $lf->g_grid(qw/-row 2 -column 0 -columnspan 3 -sticky nsew -pady/ => [0,4]);

      my $txt = $lf->new_tk__text(
	-font => "\{$EditFont{family}\} $EditFont{size}",
	-bg => 'white',
	-borderwidth => 2,
	-highlightthickness => 1,
	-height => 30);
      $txt->g_grid(qw/-row 0 -column 0 -sticky nsew/, -pady => [0,4]);

      my $scv = $lf->new_ttk__scrollbar(-orient => "vertical", -command => [$txt, "yview"]);
      $scv->g_grid(qw/-row 0 -column 1 -sticky nsw/);

      my $sch = $lf->new_ttk__scrollbar(-orient => "horizontal", -command => [$txt, "xview"]);
      $sch->g_grid(qw/-row 1 -column 0 -columnspan 2 -sticky new/);

      $txt->configure(-yscrollcommand => [$scv, 'set']);
      $txt->configure(-xscrollcommand => [$sch, 'set']);

      my $sz = 12;
      $txt->tag_configure('DIR',  -font => "\{$EditFont{family}\} $sz bold");
      $txt->tag_configure('FILE', -font => "\{$EditFont{family}\} $sz");
      if (! -d "$to/$name") {
	make_path("$to/$name", {chmod => 0777});
      }
      if (rmove($txt, "$self->{$name}/$name", "$to/$name", '')) {
	remove_tree("$self->{$name}/$name") if ($del);
	$self->{$name} = $to;
	change($self, $name);
      }
      $txt->insert('end', "\nDONE\n", 'DIR');
      $txt->see('end');
      Tkx::update();
    }
    Tkx::vwait(\$done);
  }
  $top->g_destroy();
}

sub rmove {
  my($rotxt,$src,$dst,$ind) = @_;

  opendir my $dh, "$src" or die "rmove() couldn't open folder: '$src'\n";
  $rotxt->configure(-state => 'normal');
  $rotxt->insert('end', $ind.$dst."\n", 'DIR');
  $rotxt->yview('end');
  Tkx::update();
  my $ret = 1;
  foreach my $f (grep !/^\.\.?$/, readdir $dh) {
    if (-d "$src/$f") {
      make_path("$dst/$f", {chmod => 0777}) if (! -d "$dst/$f");
      $ret = rmove("$src/$f", "$dst/$f", $rotxt, '  '.$ind);
    } else {
      my $txt = read_file("$src/$f");
      if (! defined $txt) {
	errorPrint("CP::Collection::rmove failed to read in '$src/$f'\n");
	$ret = 0;
      }
      if (write_file("$dst/$f", $txt) == 0) {
	errorPrint("CP::Collection::rmove failed to write '$dst/$f'\n");
	$ret = 0;
      }
      $rotxt->insert('end', $ind."  $dst/$f\n", 'FILE');
      $rotxt->yview('end');
      Tkx::update();
    }
    last if ($ret == 0);
  }
  $rotxt->configure(-state => 'disabled');
  $ret;
}

sub load {
  my($self) = shift;

  our ($which,%coll);
  do USER."/Chordy.cfg";
  #
  # Now read the file options into our hash.
  #
  foreach my $c (keys %coll) {
    $self->{$c} = $coll{$c};
  }
  $Parent = $self->{$which};
  $Home = "$Parent/$which";
  $CurrentCollection = $which;
  undef %coll;
}

sub save {
  my($self) = shift;

  my $OFH = openConfig(USER."/Chordy.cfg");
  return(0) if ($OFH == 0);

  print $OFH "\$which = '$CurrentCollection';\n";
  print $OFH "\%coll = (\n";

  foreach my $c (sort keys %{$self}) {
    print $OFH "  '$c' => '$self->{$c}',\n";
  }

  printf $OFH ");\n1;\n";

  close($OFH);
}

sub edit {
  my($self) = shift;

  my($top,$wt) = popWin(0, 'Collections');

  my $tf = $wt->new_ttk__frame(qw/-relief raised -borderwidth 1/, -padding => [4,4,4,4]);
  $tf->g_grid(qw/-row 0 -column 0 -sticky nsew/);

  my $bf = $wt->new_ttk__frame();
  $bf->g_grid(qw/-row 1 -column 0 -sticky ew/);

  my($a,$b,$c,$ca,$cb,$d,$e,$f,$g,$h,$i,$j);

  $a = $tf->new_ttk__label(-text => "Collection: ",);
  $b = $tf->new_ttk__button(
    -width => 20,
    -textvariable => \$CurrentCollection,
    -command => sub{
      my @lst = (sort keys %{$self});
      popMenu(\$CurrentCollection, sub{change($self, $CurrentCollection)}, \@lst);
    });
  $CollectionPath = $self->{$CurrentCollection};
  my $orgcol = $CurrentCollection;
  my $delorg = 0;
  $c = $tf->new_ttk__button(
    qw/-text Delete -command/ =>
    sub{_delete($self, $CurrentCollection, $delorg);
	$top->g_raise();
    });
  $ca = $tf->new_ttk__button(
    qw/-text Move -command/ =>
    sub{_move($self, $CurrentCollection, $delorg);
	$top->g_raise();
    });
  $cb = $tf->new_ttk__checkbutton(
    -text => 'Delete original',
    -variable => \$delorg,
      );
  $d = $tf->new_ttk__label(-text => "Path: ");
  $e = $tf->new_ttk__label(
    -textvariable => \$CollectionPath,
    -width => 50);
  $a->g_grid(qw/-row 0 -column 0 -sticky e -pady/ => [0,6]);
  $b->g_grid(qw/-row 0 -column 1 -sticky w/);
  $c->g_grid(qw/-row 0 -column 2 -padx 10/);
  $ca->g_grid(qw/-row 0 -column 3 -padx 5/);
  $cb->g_grid(qw/-row 0 -column 4 -padx 5/);
  $d->g_grid(qw/-row 1 -column 0 -sticky e/, -pady => [4,0]);
  $e->g_grid(qw/-row 1 -column 1 -columnspan 4 -sticky w/, -padx => [0,4], -pady => [4,0]);

  $f = $tf->new_ttk__frame(qw/-height 1/);
  $f->g_grid(qw/-row 2 -column 0 -sticky we -columnspan 5 -pady 5/);

  $NewCollection = "";
  $g = $tf->new_ttk__label(-text => "New Name: ");
  $h = $tf->new_ttk__entry(
    -width => 20,
    -textvariable => \$NewCollection);
  $i = $tf->new_ttk__button(
    -text => 'New',
    -command => sub{
      if (add($self, $NewCollection)) {
	$CollectionPath = $self->{$CurrentCollection};
	main::selectClear();
      }
      $NewCollection = "";
      $top->g_raise();
    });
  $j = $tf->new_ttk__button(
    -text => 'Rename',
    -command => sub{
      $CollectionPath = $self->{$CurrentCollection} if (newname($self, $NewCollection));
      $NewCollection = "";
      $top->g_raise();
    });

  $g->g_grid(qw/-row 3 -column 0 -sticky e/);
  $h->g_grid(qw/-row 3 -column 1 -sticky w/);
  $i->g_grid(qw/-row 3 -column 2 -padx 5/);
  $j->g_grid(qw/-row 3 -column 3 -padx 5/);

  my $Done = '';
  my $cancel = $bf->new_ttk__button(
    -text => "Cancel",
    -command => sub{$Done = "Cancel";});
  $cancel->g_pack(qw/-side left -padx 60/, -pady => "4 2");

  my $ok = $bf->new_ttk__button(
    -text => "OK",
    -command => sub{$Done = "OK";});
  $ok->g_pack(qw/-side right -padx 60/, -pady => "4 2");

  Tkx::vwait(\$Done);
  if ($Done eq "OK") {
    change($self, $CurrentCollection);
    save($self);
  } else {
    $CurrentCollection = $orgcol;
    $CollectionPath = $self->{$CurrentCollection};
  }
  $top->g_destroy();
  $Done;
}

1;
