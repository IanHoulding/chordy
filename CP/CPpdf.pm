package CP::CPpdf;

# This file is part of Chordy.
#
###############################################################################
# Copyright (c) 2018 Ian Houlding
# All rights reserved.
# This program is free software.
# You can redistribute it and/or modify it under the same terms as Perl itself.
###############################################################################

use strict;
use warnings;

use POSIX qw/ceil/;
use CP::Cconst qw/:FONT :MUSIC :TEXT :INDEX :COLOUR/;
use CP::Global qw/:FUNC :OPT :WIN :CHORD :SCALE :SETL/;
use PDF::API2;
use CP::Chord;

my($TextPtr,$GfxPtr);

my $XSIZE = 8;
my $YSIZE = 10;
my $SPACE = 15;
my $FFSIZE = 0;

my $SUPHT = 0.6;

sub new {
  my($proto,$pro,$fn) = @_;

  my $class = ref($proto) || $proto;
  my $self = {};
  my $pdf = PDF::API2->new(-file => "$fn");
  $self->{pdf} = $pdf;
  #
  # Do all the font jiggery-pokery ...
  #
  foreach my $m (['Title',     TITLE],
		 ['Lyric',     VERSE],
		 ['Chord',     CHORD],
		 ['Comment',   CMMNT],
		 ['Highlight', HLIGHT]) {
    my($media,$idx) = @{$m};
    my $cap = substr($media, 0, 1);
    $cap .= 'M' if ($idx == CMMNT);
    my $fp = $Media->{"$media"};
    my $fam = $fp->{family};
    my $size = ceil($fp->{size});
    # return Bold, BoldItalic or Regular
    my $wt = pdfWeight($fp->{weight}, $fp->{slant});
    my $pfp = getFont($pdf, $fam, $wt);
    # Font metrics don't seem to follow the accepted standard where the total
    # height used by a font is (ascender + descender) and where the ascender
    # includes any extra height added by the composer.
    $self->{"${cap}sz"} = $size;
    $self->{"${cap}dc"} = abs(ceil(($pfp->descender * $size) / 1000));
    if ($idx == CHORD) {
      $self->{Cas} = $size;
      $self->{Ssz} = ceil($size * $SUPHT * 2) / 2;
    } else {
      if ($idx == CMMNT || $idx == HLIGHT) {
	$self->{"${cap}as"} = ceil(($pfp->ascender * $size) / 1000);
      } else {
	# This is essentially the height of a Capital.
	$self->{"${cap}as"} = $size - $self->{"${cap}dc"};
      }
      $self->{"${cap}dc"} += 2;
    }
    $self->{"${cap}clr"} = $fp->{color};
    $self->{hscale}[$idx] = 100;
    $self->{font}[$idx] = $pfp;
    if ($idx == CMMNT) {
     $self->{hscale}[CMMNTB] = $self->{hscale}[CMMNTI] = 100;
     $self->{font}[CMMNTB] = $pfp;
     $wt = ($wt eq 'Bold') ? 'BoldItalic' : 'Italic';
     $self->{font}[CMMNTI] = getFont($pdf, $fam, $wt);
    }
    $self->{"${cap}fam"} = $fam;
    $self->{"${cap}wt"} = $wt;
  }
  $self->{font}[GRID] = getFont($pdf, RESTFONT, 'Regular');

  $self->{page} = [];
  $FFSIZE = 0;

  bless $self, $class;
  return($self);
}

sub printSL {
  return if ($CurSet eq '');

  my $tmpMedia = $Opt->{Media};
  $Opt->{Media} = $Opt->{PrintMedia};
  $Media->change();
  my @xtra = getXtra();
  my $xtracnt = @xtra;
  return if ($xtracnt == 0);
  my($date,$setup,$sound,$set1,$set2) = @xtra;

  my $list = $AllSets->{browser}{selLB}{array};
  my $tmpPDF = "$Path->{Temp}/$CurSet.pdf";
  my $pdf = PDF::API2->new(-file => "$tmpPDF");
  my $pp = $pdf->page;
  #
  # Do the font jiggery-pokery ...
  #
  my $fp = $Media->{"Title"};
  my $Tfam = $fp->{family};
  my $Tsz = ceil($fp->{size});
  # return Bold, BoldItalic or Regular
  my $Twt = pdfWeight($fp->{weight}, $fp->{slant});
  my $pfp = getFont($pdf, $Tfam, $Twt);
  my $Tdc = abs(ceil(($pfp->descender * $Tsz) / 1000)) + 2;
  my $Tclr = $fp->{color};
  my $Lsz = $Media->{Lyric}{size};
  my $Lclr = $Media->{Lyric}{color};

  my $w = $Media->{width};
  my $h = $Media->{height};

  $pp->mediabox($w, $h);
  $pp->cropbox(0, 0, $w, $h);
  $GfxPtr = $pp->gfx();
  $TextPtr = $pp->text();

  if ($Opt->{PDFbg} ne WHITE) {
    $GfxPtr->fillcolor($Opt->{PDFbg});
    $GfxPtr->rect(0, 0, $w, $h);
    $GfxPtr->fill();
  }
  my $hht = $Tsz + 3;
  if ($Media->{titleBG} ne WHITE) {
    _hline(0, $h - ($hht / 2), $w, $hht, $Media->{titleBG});
  }
  _textCenter($w/2, $h - ($hht - $Tdc - 1), $CurSet, $pfp, $Tsz, $Tclr);
  if ($date ne '') {
    _textRight($w - INDENT, $h - ($hht - $Tdc - 1), $date, $pfp, $Lsz, $Tclr);
    $xtracnt--;
  }

  $h -= $hht;
  _hline(0, $h, $w, 1, DBLUE);

  my $spc = '  ';
  if ($xtracnt) {
    my $x = 15;
    $h -= $Tsz;
    foreach my $xtr (['Setup',$setup], ['Sound Check',$sound], ['Set 1',$set1], ['Set 2',$set2]) {
      my($lab,$ent) = (@{$xtr});
      if ($ent ne '') {
	$x += _textAdd($x, $h, "$lab: ", $pfp, $Lsz, $Tclr);
	$x += _textAdd($x, $h, "$ent", $pfp, $Lsz, $Lclr);
	$x += 30;
      }
    }
    $h -= $Tsz;
  }
  $h -= $Tsz;

  my $cnt = @{$list};
  while (($h - ($Tsz * $cnt)) < 0) { # deliberately use $Tsz to add extra spacing
    $Lsz--;
  }
  my $max = 0;
  my @pros = ();
  foreach my $fn (@{$list}) {
    my $pro = CP::Pro->new($fn);
    push(@pros, $pro);
    $cnt++ if ($pro->{title} =~ /^INTERVAL$/i);
    my $x = _measure("$spc$pro->{title}", $pfp, $Lsz);
    $max = $x if ($x > $max);
  }
  my $spcw = _measure($spc, $pfp, $Lsz);
  my $numw = _measure("99", $pfp, $Lsz);
  my $keyw = _measure("${spc}G#m", $pfp, $Lsz - 3);
  $max += ($spcw + $numw + $keyw);
  my $indent = int(($w - $max) / 2);
  my $num = 1;
  foreach my $pro (@pros) {
    if ($pro->{title} =~ /^INTERVAL$/i) {
      $num = 1;
      $h -= int($Lsz / 2);
      _textAdd($indent, $h, "$spc$pro->{title}", $pfp, $Lsz, RFG);
      $h -= int($Lsz / 2);
    }
    else {
      my $x = $indent + $numw;
      _textRight($x, $h, "$num", $pfp, $Lsz, $Tclr);
      $x += ($spcw * 2);
      _textAdd($x, $h, $pro->{key}, $pfp, $Lsz - 3, DRED);
      $x += ($keyw + $spcw);
      _textAdd($x, $h, $pro->{title}, $pfp, $Lsz, $Lclr);
      $num++;
    }
    $h -= $Tsz;
  }
  $pdf->update();
  $pdf->end();
  my $print = $Opt->{PDFprint};
  $Opt->{PDFprint} = 69;
  main::PDFprint($tmpPDF) if (main::PDFview($tmpPDF));
  $Opt->{PDFprint} = $print;

  $Opt->{Media} = $tmpMedia;
  $Media->change();
}

sub getXtra {
  my($top,$wt) = popWin(0, 'Run List Date/Times');

  my $tf = $wt->new_ttk__frame(qw/-borderwidth 2 -relief ridge -padding/ => [12,4,0,4]);
  $tf->g_pack(qw/-side top -expand 1 -fill x/);
  my $bf = $wt->new_ttk__frame();
  $bf->g_pack(qw/-side top -expand 1 -fill x/);

  my $done = '';
  my @ret = ();
  my $dtlab = $tf->new_ttk__label(-text => "Date: ");
  my $dtent = $tf->new_ttk__entry(qw/-width 16 -textvariable/ => \$ret[0]);

  my $sulab = $tf->new_ttk__label(-text => "Set Up: ");
  my $suent = $tf->new_ttk__entry(qw/-width 6 -textvariable/ => \$ret[1]);

  my $solab = $tf->new_ttk__label(-text => "  Sound Check: ");
  my $soent = $tf->new_ttk__entry(qw/-width 6 -textvariable/ => \$ret[2]);

  my $s1lab = $tf->new_ttk__label(-text => "Set 1: ");
  my $s1ent = $tf->new_ttk__entry(qw/-width 6 -textvariable/ => \$ret[3]);

  my $s2lab = $tf->new_ttk__label(-text => "Set 2: ");
  my $s2ent = $tf->new_ttk__entry(qw/-width 6 -textvariable/ => \$ret[4]);

  $dtlab->g_grid(qw/-row 0 -column 0 -pady 0 -sticky e/);
  $dtent->g_grid(qw/-row 0 -column 1 -pady 0 -sticky w/);
  $sulab->g_grid(qw/-row 1 -column 0 -pady 4 -sticky e/);
  $suent->g_grid(qw/-row 1 -column 1 -pady 4 -sticky w/);
  $solab->g_grid(qw/-row 2 -column 0 -pady 0 -sticky e/);
  $soent->g_grid(qw/-row 2 -column 1 -pady 0 -sticky w/);
  $s1lab->g_grid(qw/-row 3 -column 0 -pady 4 -sticky e/);
  $s1ent->g_grid(qw/-row 3 -column 1 -pady 4 -sticky w/);
  $s2lab->g_grid(qw/-row 4 -column 0 -pady 0 -sticky e/);
  $s2ent->g_grid(qw/-row 4 -column 1 -pady 0 -sticky w/);

  ($bf->new_ttk__button(
     -text => "Cancel",
     -command => sub{$done = "Cancel";},
      ))->g_grid(qw/-row 0 -column 0 -sticky w/, -padx => [30,30], -pady => [4,2]);

  ($bf->new_ttk__button(
     -text => "OK",
     -command => sub{$done = "OK";},
      ))->g_grid(qw/-row 0 -column 1 -sticky e/, -padx => [30,30], -pady => [4,2]);

  $dtent->g_focus();
  Tkx::vwait(\$done);
  if ($done eq "Cancel") {
    @ret = ();
  }
  $top->g_destroy();
  @ret;
}

sub getFont {
  my($pdf,$fam,$wt) = @_;

  my $pfp;
  if (! defined $FontList{"$fam"}) {
    $fam = 'Times New Roman';
    errorPrint("Font '$fam' not found.\nSubstituting 'Times New Roman'");
  }
  my $path = $FontList{"$fam"}{Path};
  my $file = $FontList{"$fam"}{$wt};
  if ($file eq '') {
    my %opts = ();
    if ($wt =~ /Bold/) {
      $opts{'-bold'} = $Opt->{Bold};
    }
    if ($wt =~ /Italic/) {
      $opts{'-oblique'} = $Opt->{Italic};
    }
    my $tmp = $pdf->ttfont($path."/".$FontList{"$fam"}{Regular});
    $pfp = $pdf->synfont($tmp, %opts);
  } else {
    $pfp = $pdf->ttfont($path."/".$file);
  }
  $pfp;
}

sub chordSize {
  my($self,$size) = @_;

  if ($size eq '') {
    $size = $Media->{Chord}{size};
  }
  my $fp = $self->{font}[CHORD]; 
  $self->{Cas} = $self->{Csz} = $size;
  $self->{Cdc} = abs(ceil(($fp->descender * $size) / 1000)) + 2;
  $self->{Ssz} = ceil($size * $SUPHT * 2) / 2;
  $FFSIZE = _measure("10", $fp, $self->{Csz} * $SUPHT) if (defined $TextPtr);
}

# Called in response to a {chordfont} directive.
sub chordFont {
  my($self,$font) = @_;

  if ($font eq '') {
    my $fp = $Media->{Chord};
    $font = "\{$fp->{family}\} $fp->{size} $fp->{weight} $fp->{slant}";
  }
  my ($fam,$sz,$wt) = fontAttr($self->{Cwt}, $font);

  if (defined $FontList{"$fam"}) {
    my $fp;
    if ($self->{Cfam} ne $fam || $self->{Cwt} ne $wt) {
      $fp = getFont($self->{pdf}, $fam, $wt);
      $self->{font}[CHORD] = $fp;
      $self->{Cfam} = $fam;
      $self->{Cwt} = $wt;
    } else {
      $fp = $self->{font}[CHORD];
    }
    $self->{Cas} = $self->{Csz} = $sz;
    $self->{Cdc} = abs(ceil(($fp->descender * $sz) / 1000)) + 2;
    $self->{Ssz} = ceil($sz * $SUPHT * 2) / 2;
    $FFSIZE = _measure("10", $fp, $self->{Csz} * $SUPHT) if (defined $TextPtr);
  } else {
    error("Chord", $fam, $wt);
  }
}

sub lyricSize {
  my($self,$size) = @_;

  if ($size eq '') {
    $size = $Media->{Lyric}{size};
  }
  my $fp = $self->{font}[VERSE]; 
  $self->{Lsz} = $size;
  $self->{Ldc} = abs(ceil(($fp->descender * $size) / 1000));
  $self->{Las} = $size - $self->{Ldc};
  $self->{Ldc} += 2;
}

# Called in response to a {textfont} directive.
sub lyricFont {
  my($self,$font) = @_;

  if ($font eq '') {
    my $fp = $Media->{Lyric};
    $font = "\{$fp->{family}\} $fp->{size} $fp->{weight} $fp->{slant}";
  }
  my($fam,$sz,$wt) = fontAttr($self->{Lwt},$font);

  if (defined $FontList{"$fam"}) {
    my $fp;
    if ($self->{Lfam} ne $fam || $self->{Lwt} ne $wt) {
      $fp = getFont($self->{pdf}, $fam, $wt);
      $self->{font}[VERSE] = $fp;
      $self->{Lfam} = $fam;
      $self->{Lwt} = $wt;
    } else {
      $fp = $self->{font}[VERSE];
    }
    $self->{Lsz} = $sz;
    $self->{Ldc} = abs(ceil(($fp->descender * $sz) / 1000));
    $self->{Las} = $sz - $self->{Ldc};
    $self->{Ldc} += 2;
  } else {
    error("Lyric", $fam, $wt);
  }
}

sub fontAttr {
  my($pwt,$str) = @_;

  my $owt = ($pwt =~ /bold/i) ? 'bold' : '';
  my $osl = ($pwt =~ /italic/i) ? 'italic' : '';
  my ($fam,$sz,$nwt,$nsl);
  if ($str =~ /^\s*\{([^\}]+)\}\s*(\d*)\s*(\S*)\s*(\S*)/) {
    $fam = $1;
    $sz = $2;
    $nwt = $3;
    $nsl = $4;
  }
  else {
    ($fam,$sz,$nwt,$nsl) = split(' ',$str);
    $sz = '' if (!defined $sz);
    $nwt = '' if (!defined $nwt);
    $nsl = '' if (!defined $nsl);
  }
  $nwt = $owt if ($nwt eq '');
  $nsl = $osl if ($nsl eq '');
  ($fam,$sz,pdfWeight($nwt, $nsl));
}

sub pdfWeight {
  my($wt,$sl) = @_;

  my $nwt = ($wt eq 'bold') ? 'Bold' : '';
  $nwt .= 'Italic' if ($sl eq 'italic');
  $nwt = 'Regular' if ($nwt eq '');
  $nwt;
}

sub error {
  my($type,$fam,$wt) = @_;
  errorPrint("$type Font '$fam $wt' does not exist - reverting to original font.");
}

sub newTextGfx {
  my($self,$pp) = @_;

  $GfxPtr = $pp->gfx();
  $TextPtr->textend() if (defined $TextPtr);
  $TextPtr = $pp->text();
}

sub newPage {
  my($self,$pro,$pn) = @_;

  my $w = $Media->{width};
  my $h = $Media->{height};
  my $pp = $self->{pdf}->page;
  push(@{$self->{page}}, $pp);
  $pp->mediabox($w, $h);
  $pp->cropbox(0, 0, $w, $h);
  newTextGfx($self, $pp);

  if ($Opt->{PDFbg} ne WHITE) {
    $GfxPtr->fillcolor($Opt->{PDFbg});
    $GfxPtr->rect(0, 0, $w, $h);
    $GfxPtr->fill();
  }
  my $hht = $self->{Tsz} + 3;
  if ($Media->{titleBG} ne WHITE) {
    _hline(0, $h - ($hht / 2), $w, $hht, $Media->{titleBG});
  }
  _textCenter($w/2, $h - ($hht - $self->{Tdc} - 1),
	      $pro->{title}, $self->{font}[TITLE], $self->{Tsz}, $Media->{Title}{color});

  $h -= $hht;
  _hline(0, $h, $w, 1, DBLUE);

  my $tht = ($Media->{height} - $h) / 2;
  my $th = $self->{Tsz} * KEYMUL;
  my $dc = $self->{Tdc} * KEYMUL;

  if ($pro->{key} ne '') {
    my $tw = _textAdd(INDENT, $h + $tht + $dc - 1, "Key: ", $self->{font}[TITLE], $th, RFG);
    my($ch,$cname) = CP::Chord->new($pro->{key});
    $ch = $ch->trans2obj($pro) if ($Opt->{Transpose} ne 'No');
    chordAdd($self, INDENT + $tw, $h + $tht + $dc - 1, $ch, BLACK, $th);
  }
  # Capo takes precedence over Note.
  if ($pro->{capo} ne 0) {
    my $tw = _textAdd(INDENT, $h + $dc + 1, "Capo: ", $self->{font}[TITLE], $th, MAGENT);
    _textAdd(INDENT + $tw, $h + $dc + 1, "$pro->{capo}", $self->{font}[TITLE], $th, BLACK);
  } elsif ($pro->{note} ne '') {
    _textAdd(INDENT, $h + $dc + 1, $pro->{note}, $self->{font}[TITLE], $th, MAGENT);
  }

  $h -= INDENT;
  if ($Opt->{Grid} != NONE && ($pn == 1 || $Opt->{Grid} == ALLP)) {
    $FFSIZE = _measure("10", $self->{font}[CHORD], $self->{Csz} * $SUPHT) if ($FFSIZE == 0);
    my($cnt,$xinc) = fingersWidth();
    my $margin = INDENT * 2;
    $w -= $margin;
    my $maxy = 0;
    my $linex = $margin;
    my @chidx = sort keys %{$pro->{chords}};
    while (@chidx) {
      my $ch = shift(@chidx);
      # Special case for chords like C//
      if ($ch =~ m!//!) {
	$ch =~ s!//.*!!;
	next if (exists $pro->{chords}{$ch});
      }
      my($dx,$dy) = drawFinger($self, $pro, $linex, $h, $ch);
      $linex += $xinc;
      if ($linex > $w && @chidx) {
	$linex = $margin;
	$h -= ($maxy + 5);
	$maxy = 0;
      }
      $maxy = $dy if ($dy > $maxy);
    }
    $h -= ($maxy + INDENT);
    $GfxPtr->fillcolor(DBLUE);
    $GfxPtr->rect(0, $h, $w + $margin, 1);
    $GfxPtr->fill();
    $h -= INDENT;
  }
  $h -= INDENT;
}

sub pageNum {
  my($self,$npage) = @_;

  my $h = $Media->{height} - $self->{Tsz} + $self->{Tdc};
  my $npg = 0 - $npage;
  my $pn = 1;
  my $ptext = $TextPtr;
  while ($npg) {
    my $pp = $self->{page}[$npg++];
    my $page = "Page ".$pn++." of $npage ";
    $TextPtr = $pp->text();
    _textRight($Media->{width} - INDENT, $h,
	       $page, $self->{font}[TITLE], ceil($self->{Tsz} * PAGEMUL), BROWN);
    $TextPtr->textend;
  }
  $TextPtr = $ptext;
}

#
# Return the height (in points) of one chord finger display
#
sub fingerHeight {
  my($self,$name) = @_;

  my $tht = $self->{Csz} * $SUPHT;
  my $tdc = $self->{Cdc} * $SUPHT;
  my $ly = $self->{Csz} + $tht + $tdc;
  my $max = 0;
  foreach my $fr (@{$Fingers{$name}{fret}}) {
    $max = $fr if ($fr =~ /\d+/ && $fr > $max);
  }
  $ly += (($max * $YSIZE) + ($YSIZE / 2));
}

#
# Takes an array of chord names, works out how many will
# fit on one line and from that, how high the complete
# display will be.
#
sub fingersHeight {
  my($self,@chords) = @_;

  my($nc,$inc) = fingersWidth();
  my $h = 0;
  while (@chords) {
    my $max = 0;
    foreach my $i (1..$nc) {
      last if (@chords == 0);
      my $dy = fingerHeight($self, shift(@chords));
      $max = $dy if ($dy > $max);
    }
    $h += $max;
  }
  $h;
}

#
# Return the number of chords we can display and the
# distance (in points) between the start of each chord
#
sub fingersWidth {
  my $w = $Media->{width} - (INDENT * 4);
  my $cw = (($Nstring - 1) * $XSIZE) + $FFSIZE + $SPACE; # min size of a chord diagram + spacer
  my $nc = int($w / $cw);                                # number of chords we can draw
  $cw += int(($w - ($nc * $cw) + $SPACE) / ($nc - 1));   # even out any leftover space
  ($nc,$cw);
}

sub drawFinger {
  my($self,$pro,$x,$y,$name) = @_;

  my ($ch,$cname) = CP::Chord->new($name);
  if ($KeyShift) {
    $cname = $ch->trans2str($pro);
    $ch = $ch->trans2obj($pro);
  }
  my $dx = chordLen($self, $ch) / 2;
  my $ns = $Nstring - 1;
  my $ly = $y;
  my $lmy = $y;
  my $mx = $x;
  my $fp = $self->{font}[CHORD];
  my $fc = $Media->{Chord}{color};
  my $tht = $self->{Csz} * $SUPHT;
  my $tdc = $self->{Cdc} * $SUPHT;
  my $vsz = $YSIZE/2;
  $ly -= $self->{Csz};  # minus Cdc?
  chordAdd($self, $x+(($XSIZE*$ns/2)-$dx), $ly, $ch, $fc);
  $ly -= $tht;

  my $fptr = "";
  # Fingering defined in the Pro hash takes precedence
  # as it was created with a {define:...} directive.
  #
  # For some unknown reason, perl thinks that when Fingers{'Amadd9'}
  # exists, it also thinks that Fingers{'Am(add9)'} exists - I'm
  # guessing that the hash lookup is picking up the () as part of
  # a regex. However, it DOESN'T think that Fingers{'Am(add9)'}{base}
  # exists - HUH??
  # And that, folks, is why we test for the {base} key!
  #
  if (exists $pro->{finger}{"$cname"}{base}) {
    $fptr = \%{$pro->{finger}{$cname}};
  } elsif (exists $Fingers{"$cname"}{base}) {
    $fptr = \%{$Fingers{$cname}};
  }
  if (ref($fptr) ne "") {
    my $base = $fptr->{base};
    my $frptr = $fptr->{fret};
    my $max = 0;
    foreach (0..$ns) {
      my $fr = $frptr->[$_];
      if ($fr =~ /[-xX0]/) {
	_textCenter($mx, $ly, ($fr eq '0') ? 'o' : 'x', $fp, $tht, $fc);
      }
      $mx += $XSIZE;
      $max = $fr if ($fr =~ /\d+/ && $fr > $max);
    }
    $ly -= $tdc;
    # Draw Frets
    $GfxPtr->linewidth(1);
    $GfxPtr->linecap(0);
    $lmy = $ly;
    $mx = $ns * $XSIZE;
    for (0..$max) {
      my $dx = $x + $mx;
      $dx += 2 if ($_ == 1);
      $GfxPtr->move($x, $lmy);
      $GfxPtr->hline($dx);
      $GfxPtr->stroke();
      $lmy -= $YSIZE;
    }
    # Draw the base fret number to the right of the first fret
    # NOTE: the nut is NOT considered to be a fret.
    _textAdd($x + $mx + 3, $ly - $YSIZE - $tdc, "$base", $fp, $tht, $fc);
    my $bfw = $mx + 3 + $FFSIZE;
    # Strings and finger positions
    $GfxPtr->linewidth(1);
    $GfxPtr->fillcolor(DBLUE);
    $mx = $x;
    $lmy = $ly - (($max*$YSIZE)+$vsz);
    foreach (0..$ns) {
      my $fr = $frptr->[$_];
      $GfxPtr->move($mx,$ly);
      $GfxPtr->vline($lmy);
      $GfxPtr->stroke();
      if ($fr =~ /[^-xX0]/) {
	$GfxPtr->circle($mx,$ly-($fr*$YSIZE)+$vsz, 2.5);
	$GfxPtr->fill();
      }
      $mx += $XSIZE;
    }
    $mx = $bfw;
  } else {
    $ly -= ($self->{Csz}*2);
    _textCenter($x+($XSIZE*$ns/2), $ly, 'X', $fp, $self->{Csz}*2, RED);
    $mx = (($XSIZE * $ns) + 3 + $FFSIZE);
    $lmy = $ly;
  }
  ($mx,$y-int($lmy + 1));
}

# A 'Chord' object is an array eg: C [#b] maj7 / G [#b] maj7 text
sub chordAdd {
  my($self,$x,$y,$ch,$clr,$ht) = @_;

  $ht = $self->{Csz} if (!defined $ht);
  my $fp = $self->{font}[CHORD];
  $x += _textAdd($x, $y, $ch->[0], $fp, $ht, $clr);
  if ($#$ch > 0) {
    my $sht = ceil($ht * $SUPHT);
    my $sy = $y + ceil($sht * $SUPHT);
    my $s = $ch->[1].$ch->[2];
    $x += _textAdd($x, $sy, $s, $fp, $sht, $clr) if ($s ne "");
    $x += _textAdd($x, $y, $ch->[3], $fp, $ht, $clr) if ($ch->[3] ne "");
    if ($#$ch > 3) {
      $x += _textAdd($x, $y, $ch->[4], $fp, $ht, $clr);
      $s = $ch->[5].$ch->[6];
      $x += _textAdd($x, $sy, $s, $fp, $sht, $clr) if ($s ne "");
      $x += _textAdd($x, $y, $ch->[7], $fp, $ht, $clr) if ($ch->[7] ne "");
    }
  }
}

sub chordLen {
  my($self, $ch) = @_;

  my $fp = $self->{font}[CHORD];
  my $nx = _measure($ch->[0], $fp, $self->{Csz});
  if ($#$ch > 0) {
    my $sht = ceil($self->{Csz} * $SUPHT);
    my $s = $ch->[1].$ch->[2];
    $nx += _measure($s, $fp, $sht) if ($s ne "");
    $nx += _measure($ch->[3], $fp, $self->{Csz}) if ($ch->[3] ne "");
    if ($#$ch > 3) {
      $nx += _measure($ch->[4], $fp, $self->{Csz});
      $s = $ch->[5].$ch->[6];
      $nx += _measure($s, $fp, $sht) if ($s ne "");
      $nx += _measure($ch->[7], $fp, $self->{Csz}) if ($ch->[7] ne "");
    }
  }
  $nx;
}

sub lyricAdd {
  my($self,$x,$y,$txt,$clr) = @_;
  _textAdd($x, $y, $txt, $self->{font}[VERSE], $self->{Lsz}, $clr);
}

sub lyricLen {
  my($self,$txt) = @_;
  _measure($txt, $self->{font}[VERSE], $self->{Lsz});
}

sub hline {
  my($self,$x,$y,$h,$w,$clr) = @_;

  if ($clr ne '') {
    $x = int(($Media->{width} - $w) / 2) if ($Opt->{Center});
    _bg($clr, $x, $y, $w, $h);
  }
}

#______________________________________
#____|_______________________________|_
#    |          #                    |
#    |         # #                   |
#    |        #   #                  |
#    as      #     #      #     #    ht
#    |      #########      #   #     |
#    |     #         #      # #      |
#____|____#___________#______#_______|__
#                           #     dc |
#__________________________#______|__|__
#
sub commentAdd {
  my($self,$type,$y,$txt,$fg,$bg) = @_;

  my $dc = ($type == HLIGHT) ? $self->{Hdc}: $self->{CMdc};
  my $sz = ($type == HLIGHT) ? $self->{Hsz}: $self->{CMsz};
  my $tw = _measure($txt, $self->{font}[$type], $sz);
  my($bw,$x) = (0,0);
  if ($type == HLIGHT) {
    $bw = $Media->{width} if ($Opt->{FullLineHL});
  } else {
    $bw = $Media->{width} if ($Opt->{FullLineCM});
  }
  if ($bw == 0) {
    # Not a full width background
    $bw = $tw + (INDENT * 2);
    $x = int(($Media->{width} - $bw) / 2) if ($Opt->{Center});
  }
  $self->commentBG($x, $y, $type, $bg, $bw);
  $x = int(($Media->{width} - $tw) / 2) - INDENT if ($Opt->{Center});
  _textAdd($x + INDENT, $y + $dc, $txt, $self->{font}[$type], $sz, $fg);
}

sub commentBG {
  my($self,$x,$y,$type,$bg,$w) = @_;

  if ($bg eq "") {
    # These can be changed dynamically in "Background Colours".
    $bg = ($type == HLIGHT) ? $Media->{highlightBG} : $Media->{commentBG};
  }
  my $ht = ($type == HLIGHT) ? $self->{Hdc} + $self->{Has}: $self->{CMdc} + $self->{CMas};
  _bg($bg, $x, $y, $w, $ht);
  if ($type == CMMNTB) {
    $GfxPtr->linewidth(1);
    $GfxPtr->strokecolor(BLACK);
    $GfxPtr->rect($x + 0.5, $y, $w - 1, $ht);
    $GfxPtr->stroke();
  }
}

sub _hline {
  my($x,$y,$xx,$t,$clr) = @_;

  $GfxPtr->linewidth($t);
  $GfxPtr->strokecolor($clr);
  $GfxPtr->fillcolor($clr);
  $GfxPtr->move($x, $y);
  $GfxPtr->hline($xx);
  $GfxPtr->stroke();
}

sub _vline {
  my($x,$y,$yy,$t,$clr) = @_;

  $GfxPtr->linewidth($t);
  $GfxPtr->strokecolor($clr);
  $GfxPtr->fillcolor($clr);
  $GfxPtr->move($x, $y);
  $GfxPtr->vline($yy);
  $GfxPtr->stroke();
}

sub _textAdd {
  my($x,$y,$txt,$font,$sz,$fg) = @_;

  $TextPtr->font($font, $sz);
  $TextPtr->fillcolor($fg);
  $TextPtr->translate($x, $y);
  # Returns the width of the text added
  int($TextPtr->text($txt) + 0.5);
}

sub _textRight {
  my($x,$y,$txt,$font,$sz,$fg) = @_;

  $TextPtr->font($font, $sz);
  $TextPtr->fillcolor($fg);
  $TextPtr->translate($x, $y);
  # Returns the width of the text added
  int($TextPtr->text_right($txt) + 0.5);
}

sub _textCenter {
  my($x,$y,$txt,$font,$sz,$fg) = @_;

  $TextPtr->font($font, $sz);
  $TextPtr->fillcolor($fg);

  $TextPtr->translate($x, $y);
  $TextPtr->text_center($txt);
}

sub _measure {
  my($txt,$font,$sz) = @_;

  $TextPtr->font($font, $sz);
  int($TextPtr->advancewidth($txt) + 0.5);
}

sub _bg {
  my($bg,$x,$y,$width,$ht) = @_;

  if ($bg ne '') {
    $GfxPtr->fillcolor($bg);
    $GfxPtr->rect($x, $y, $width, $ht);
    $GfxPtr->fill();
  }
}

sub close {
  my($self) = shift;
  $self->{pdf}->update();
  $self->{pdf}->end();
}

1;
