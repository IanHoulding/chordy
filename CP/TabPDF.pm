package CP::TabPDF;

# This file is part of Chordy.
#
###############################################################################
# Copyright (c) 2018 Ian Houlding
# All rights reserved.
# This program is free software.
# You can redistribute it and/or modify it under the same terms as Perl itself.
###############################################################################

use strict;
use warnings;

use CP::Cconst qw/:BROWSE :SMILIE :TEXT :MUSIC :COLOUR :FONT :TAB/;
use CP::Global qw/:FUNC :WIN :OPT :XPM :CHORD :TAB/;
use CP::Cmsg;
use PDF::API2;
use POSIX;

our($GfxPtr,$TextPtr);

sub new {
  my($proto) = @_;

  my $class = ref($proto) || $proto;
  my $self = {};
  $self->{page} = [];
  $self->{bars} = [];
  if ($Tab->{PDFname} eq '') {
    if ($Tab->{fileName} eq '') {
      main::checkSave();
      return '' if ($Tab->{fileName} eq '');
    } else {
      ($Tab->{PDFname} = $Tab->{fileName}) =~ s/.tab$/.pdf/i;
    }
  }
  $self->{tmpFile} = "$Path->{Temp}/$Tab->{PDFname}";
  unlink($self->{tmpFile}) if (-e $self->{tmpFile});
  my $pdf = $self->{pdf} = PDF::API2->new(-file => "$self->{tmpFile}");
  #
  # Do all the font jiggery-pokery ...
  #
  foreach my $m (['Title',  TITLE],
		 ['Header', HEADER],
		 ['Notes',  NOTES],
		 ['SNotes', SNOTES],
		 ['Words',  WORDS],
                 ['',       RESTS]) {
    my($media,$idx) = @{$m};
    my $pfp;
    if ($idx ne RESTS) {
      my $cap = substr($media, 0, 1);
      my $fp = $Media->{"$media"};
      my $fam = $fp->{family};
      my $size = ceil($fp->{size});
      my $wt = pdfWeight($fp->{weight}, $fp->{slant});
      $pfp = getFont($pdf, $fam, $wt);
      $self->{"${cap}dc"} = ceil((abs($pfp->descender) * $size) / 1000);
      $self->{"${cap}cap"} = $size - $self->{"${cap}dc"};
      $self->{"${cap}sz"} = $size;
      $self->{"${cap}clr"} = $fp->{color};
    } else {
      $pfp = getFont($pdf, RESTFONT, 'Regular');
    }
    $self->{hscale}[$idx] = 100;
    $self->{font}[$idx] = $pfp;
  }

  bless $self, $class;
  return($self);
}

sub pdfWeight {
  my($wt,$sl) = @_;

  my $nwt = ($wt eq 'bold') ? 'Bold' : '';
  $nwt .= 'Italic' if ($sl eq 'italic');
  $nwt = 'Regular' if ($nwt eq '');
  $nwt;
}

# We need this sub because PDF::API2 needs a PATH to a specific
# font file unlike Windows which handles it automagically.
sub getFont {
  my($pdf,$fam,$wt) = @_;

  my $pfp;
  if (! defined $FontList{"$fam"}) {
    $fam = 'Times New Roman';
    errorPrint("Font '$fam' not found.\nSubstituting 'Times New Roman'");
  }
  my $path = $FontList{"$fam"}{Path};
  my $file = $FontList{"$fam"}{$wt};
  if ($file eq '') {
    my %opts = ();
    if ($wt =~ /Bold/) {
      $opts{'-bold'} = $Opt->{Bold};
    }
    if ($wt =~ /Italic/) {
      $opts{'-oblique'} = $Opt->{Italic};
    }
    my $tmp = $pdf->ttfont($path."/".$FontList{"$fam"}{Regular});
    $pfp = $pdf->synfont($tmp, %opts);
  } else {
    $pfp = $pdf->ttfont($path."/".$file);
  }
  $pfp;
}

sub newTextGfx {
  my($self,$pp) = @_;

  $GfxPtr = $pp->gfx();
  $TextPtr->textend() if (defined $TextPtr);
  $TextPtr = $pp->text();
}

sub batch {
  my $done = '';
  my $x = Tkx::winfo_pointerx($MW);
  my $y = Tkx::winfo_pointery($MW);
  my @files = CP::Browser->new($MW, FILE, $Path->{Tab}, '.tab');
  return if (@files == 0);
  my $pl = (@files > 1) ? 's' : '';
  my($top,$frm) = popWin(0, 'Batch DPF', $x, $y);
  my $lab = $frm->new_ttk__label(
    -text => "The following tab$pl will have a PDF created:",
    -font => 'BTkDefaultFont');
  my $text = $frm->new_tk__text(
    -width => 45,
    -height => 30,
    -font => 'TkDefaultFont',
    -spacing1 => 4,
    -relief => 'raised',
    -borderwidth => 2,
    -highlightthickness => 0,
    -selectborderwidth => 0,
    -wrap=> 'none');
  my $bfrm = $frm->new_frame();
  my $can = $bfrm->new_ttk__button(
    -text => 'Cancel',
    -style => 'Red.TButton',
    -command => sub{$done='Cancel';});
  my $con = $bfrm->new_ttk__button(
    -text => 'Continue',
    -style => 'Green.TButton',
    -command => sub{$done='Continue';});
  $lab->g_pack(qw/-side top -anchor w/);
  $text->g_pack(qw/-side top -pady 4/);
  $bfrm->g_pack(qw/-side top -fill x/);
  $can->g_pack(qw/-side left -padx 8/);
  $con->g_pack(qw/-side right -padx 8/);
  foreach my $fn (@files) {
    $text->insert('end', "$fn\n");
  }
  $text->configure(-state => 'disabled');
  Tkx::vwait(\$done);
  if ($done eq 'Continue') {
    $text->tag_configure('red',   -foreground => RFG, -font => 'BTkDefaultFont');
    $text->tag_configure('green', -foreground => DGREEN, -font => 'TkDefaultFont');
    # The minus 10 & 5 take away the default values added by CP::Cmsg::message()
    $x += (Tkx::winfo_reqwidth($frm) - 10);
    $y -= 5;
    my $idx = 1;
    foreach my $fn (@files) {
      textConf($text, $idx, 'red');
      CP::Tab->new("$Path->{Tab}/$fn");
      CP::Cmsg::position($x,$y);
      make('M');
      main::cleanup();
      textConf($text, $idx++, 'green');
    }
    CP::Cmsg::position($x,$y);
    message(SMILE, " Done ", -1);
  }
  $top->g_destroy();
}

sub textConf {
  my($text,$idx,$clr) = @_;

  $text->configure(-state => 'normal');
  $text->tag_add($clr, "$idx.0", "$idx.end");
  $text->configure(-state => 'disabled');
  Tkx::update();
}

sub make {
  my($what) = @_;

  CP::Tab::getLyrics() if ($Opt->{LyricLines});
  return if ((my $pdf = CP::TabPDF->new()) eq '');
  my($x,$y) = (INDENT,0);
  my %xy = ();
  # There doesn't seem to be an easy way round this -
  # we have to do 2 passes, one for the bar backgrounds
  # and then a second to paint the actual bars otherwise
  # the background of the following bar will paint over
  # the end of the current bar.

  # PASS 1
  #
  for(my $bar = $Tab->{bars}; $bar != 0; $bar = $bar->{next}) {
    my $off = $bar->{offset};
    my $h = $off->{height};
    my $w = $off->{width};
    if ($bar->{newpage}) {
      $x = INDENT;
      $y = 0;
    }
    elsif ($bar->{newline} && $x > INDENT) {
      $x = INDENT;
      $y -= $h;
      $y = 0 if (($y - $h) < 0);
    }
    if ($y == 0) {
       $pdf->newPage();
       $y = $pdf->{topOfPage};
    }

    $pdf->barbg($bar, $x, $y) if ($bar->{bg} ne '' );

    $x += $w;
    if (($x + $w) > $Media->{width}) {
      $x = INDENT;
      $y -= $h;
      $y = 0 if (($y - $h) < 0);
    }
  }

  # PASS 2
  #
  my $lidx = my $pgidx = $y = 0;
  $x = INDENT;
  for(my $bar = $Tab->{bars}; $bar != 0; $bar = $bar->{next}) {
    my $off = $bar->{offset};
    my $h = $off->{height};
    my $w = $off->{width};
    if ($bar->{newpage}) {
      $x = INDENT;
      $y = 0;
    }
    elsif ($bar->{newline} && $x > INDENT) {
      $x = INDENT;
      $y -= $h;
      $y = 0 if (($y - $h) < 0);
    }
    if ($y == 0) {
      $lidx = $pgidx * $Tab->{rowsPP};
      $y = $pdf->{topOfPage};
      $pdf->newTextGfx($pdf->{page}[$pgidx++]);
      $pdf->pageNum($pgidx);
    }

    # Adjust the bar x,y values based on the above
    # and the fact that PDF origin is bottom left.
    $xy{x} = $xy{staffX} = $x;
    $xy{y} = $y;
    $xy{pos0} = $x + $off->{pos0};
    $xy{staffY} = $y - $off->{header};
    $xy{staff0} = $xy{staffY} - $off->{staffHeight};

    $pdf->bar($bar,\%xy);
    if ($x == INDENT) {
      $pdf->lyrics($lidx++, $x, $y);
    }

    $x += $w;
    if (($x + $w) > $Media->{width}) {
      $x = INDENT;
      $y -= $h;
      $y = 0 if (($y - $h) < 0);
    }
  }
  $pdf->close();
  #
  # At this point we have a file named tmpTab.pdf in the "Temp" folder.
  if ($what =~ /V|P/) {
    if ($what eq 'P') {
      jobSpawn("$Cmnd->{Print} \"$pdf->{tmpFile}\"");
    } else {
      jobSpawn("$Cmnd->{Acro} \"$pdf->{tmpFile}\"");
    }
  } elsif ($what eq 'M') {
    my $txt = read_file("$pdf->{tmpFile}");
    my $PDFname = "$Path->{PDF}/$Tab->{PDFname}";
    unlink("$PDFname") if (-e "$PDFname");
    write_file("$PDFname", $txt);
    if ($Opt->{PDFpath} ne '' && $Opt->{PDFpath} ne $Path->{PDF}) {
      my $copyName = "$Opt->{PDFpath}/$Tab->{PDFname}";
      unlink("$copyName") if (-e "$copyName");
      write_file("$copyName", $txt);
    }
    unlink("$pdf->{tmpFile}");
    message(SMILE, "Made", 1);
  }
}

sub newPage {
  my($self) = shift;

  my $w = $Media->{width};
  my $h = $Media->{height};
  my $pp = $self->{pdf}->page;
  push(@{$self->{page}}, $pp);
  $pp->mediabox($w, $h);
  $pp->cropbox(0, 0, $w, $h);
  newTextGfx($self, $pp);

  $h -= $Tab->{pageHeader};

  _textCenter($self, $w/2, $h + $self->{Tdc} + 2, $Tab->{title}, TITLE, $self->{Tsz}, $self->{Tclr});

  _hline(0, $h, $h + $w, 0.75, DBLUE);

  my $tht = ($Media->{height} - $h) / 2;
  my $th = int($self->{Tsz} * KEYMUL);
  my $dc = int($self->{Tdc} * KEYMUL);
  if ($Tab->{key} ne '') {
    my $tw = _textAdd($self,INDENT, $h + $tht + $dc, "Key: ", TITLE, $th, bFG);
    my $ch = [split('',$Tab->{key})];
    chordAdd($self, INDENT + $tw, $h + $tht + $dc, $ch, $Media->{Chord}{color}, $th);
  }
  if ($Tab->{note} ne '') {
    _textAdd($self, INDENT, $h + $dc + 1, $Tab->{note}, TITLE, $th, $Media->{Chord}{color});
  }

  $self->{topOfPage} = $h - INDENT;

  if (defined $Tab->{tempo} && @{$self->{page}} == 1) {
    my $ht = $Tab->{symSize} * 0.6;
    my $x = ($Media->{width} / 2) - $ht;
    $h -= $ht;
    my $wid = _textAdd($self, $x, $h + 2, 'O', RESTS, $ht, BLACK);
    _textAdd($self, $x + $wid, $h, " = ".$Tab->{tempo}, TITLE, $ht, BLACK);
  }
}

sub pageNum {
  my($self,$pn) = @_;

  my $fntht = POSIX::ceil($self->{Tsz} * PAGEMUL);
  my $y = $Media->{height} - $Tab->{pageHeader} + (($Tab->{pageHeader} - $fntht) / 2);
  my $npg = @{$self->{page}} + 0;
  my $page = "Page $pn of $npg ";
  _textRight($self, $Media->{width} - INDENT, $y, $page, TITLE, $fntht, BROWN);
}

sub chordAdd {
  my($self,$x,$y,$ch,$clr,$ht) = @_;

  my $sht = int($ht * 0.8);
  $ht = $self->{Csz} if (!defined $ht);
  $x += _textAdd($self, $x, $y, $ch->[0], NOTES, $ht, $clr);
  if (defined $ch->[1]) {
    # Make the base line for superscript 1/2 the height of a capital
    $x += _textAdd($self, $x, $y + int($ht / 2), $ch->[1], NOTES, $sht, $clr);
  }
  $x;
}

# NB: $Medium must be 1/2 of $Thick
our $Line = 1.2;
our $Thick = 1.0;
our $Fat = 0.75;
our $Medium = 0.5;
our $Thin = 0.3;

sub barbg {
  my($self,$bar,$x,$y) = @_;

  my $off = $bar->{offset};
  $y -= $off->{height};
  my $w = $off->{width};
  # This continues the background to the end of the line
  #   IF there are no more bars on the line.
  # Hmmm - not sure I want this anymore.
#  if ($bar->{next} == 0 || $bar->{next}{newline} || $bar->{next}{newpage}) {
#    for(my $idx = $bar->{pidx} + 1; $idx % $Opt->{Nbar}; $idx++) {
#      $w += $off->{width};
#    }
#  }
#  _bg($bar->{bg}, $x-2, $y+1, $w+4, $off->{height}+1);
  _bg($bar->{bg}, $x-2, $y, $w+4, $off->{height});
}

# All the absolute xy values are in the %{$xy} hash.
sub bar {
  my($self,$bar,$xy) = @_;

  my $off = $bar->{offset};
  my($x,$y,$w,$h) = ($xy->{x},$xy->{y},$off->{width},$off->{staffHeight});
  # Staff Lines
  my $ly = $xy->{staffY};
  my $lx = $x + $Medium;
  my $lw = $w + $lx;
  my $ss = $off->{staffSpace};
  foreach (1..$Nstring) {
    _hline($lx, $ly, $lw, $Medium, BLACK);
    $ly -= $ss;
  }
  # Bar Lines
  $lx = $xy->{pos0};
  $ly += $ss;
  my $un = $off->{interval} * 8;
  (my $t = $Opt->{Timing}) =~ s/(\d).*/$1/;
  foreach (1..$t) {
    _vline($lx, $ly, $ly + $h, $Thin, BLACK);
    $lx += $un;
  }
  #
  # The bar start vline is a bit of a problem. If the previous
  # bar is physically just before this one AND it has a repeat
  # end marker, it would get overwritten.
  #
  $ly -= ($Medium / 2);
  if ($x == INDENT || ($bar->{prev}{rep} ne 'End')) {
    _vline($x, $ly, $ly + $h + $Medium, $Thick, BLACK);
  }
  _vline($x + $w, $ly, $ly + $h + $Medium, $Thick, BLACK);

  #
  # Handle anything that needs to go above the Bar.
  #
  $ly = $y - ($Line / 2);
  my $txt = '';
  if ($bar->{volta} ne 'None') {
    _hline($x, $ly, $x + $off->{width}, $Line, $self->{Hclr});
    if ($bar->{volta} ne 'Center') {
      if ($bar->{volta} =~ /Left|Both/) {
	_vline($x, $ly + ($Line/2), $xy->{staffY} + INDENT, $Line, $self->{Hclr});
      }
      if ($bar->{volta} =~ /Right|Both/) {
	_vline($x+$w, $ly + ($Line/2), $xy->{staffY} + INDENT, $Line, $self->{Hclr});
      }
    }
  }
  if ($bar->{header} ne '') {
    if ($bar->{justify} eq 'Right') {
      $lx = $x + $w - _measure($self, $bar->{header}, HEADER, $self->{Hsz},
			       $self->{hscale}[HEADER]) - ($Line * 2);
    } else {
      $lx = $x;
    }
    _adjTextAdd($self, $lx, $ly - $self->{Hcap} - $Line,
		$bar->{header}, $Tab->{headFont}, HEADER, $self->{Hsz}, $self->{Hclr});
  }
  #
  # Handle start of repeat and/or repeat count.
  #
  if ($bar->{rep} eq 'Start') {
    repStart($x, $xy->{staffY}, $off->{staffHeight}, $self->{Hclr});
  }
  if ($bar->{rep} eq 'End') {
    repEnd($x + $w, $xy->{staffY}, $off->{staffHeight}, $self->{Hclr});
  }
  notes($self, $xy, $bar);
}

sub repStart {
  my($x,$y,$h,$clr) = @_;

  $y -= $h;
  _vline($x, $y, $y + $h, $Thick, $clr);
  $x += $Line;
  _vline($x, $y, $y + $h, $Medium, $clr);
  $x += $Line;
  $GfxPtr->fillcolor($clr);
  my $dy = $h / ($Nstring - 1);
  $dy *= 2 if ($Nstring > 4);
  $dy += ($Thick * 2);
  $GfxPtr->circle($x, $y + $dy, $Thick);
  $GfxPtr->fill();
  $GfxPtr->circle($x, $y + $h - $dy, $Thick);
  $GfxPtr->fill();
}

sub repEnd {
  my($x,$y,$h,$clr) = @_;

  $y -= $h;
  _vline($x, $y, $y + $h, $Thick, $clr);
  $x -= ($Line);
  _vline($x, $y, $y + $h, $Medium, $clr);
  $x -= $Line;
  $GfxPtr->fillcolor($clr);
  my $dy = $h / ($Nstring - 1);
  $dy *= 2 if ($Nstring > 4);
  $dy += ($Thick * 2);
  $GfxPtr->circle($x, $y + $dy, $Thick);
  $GfxPtr->fill();
  $GfxPtr->circle($x, $y + $h - $dy, $Thick);
  $GfxPtr->fill();
}

sub notes {
  my($self,$xy,$bar) = @_;

  my $off = $bar->{offset};
  my $x = $xy->{pos0};
  my $ss = $off->{staffSpace};
  my $u = $off->{interval};

  foreach my $n (@{$bar->{notes}}) {
    if ($n->{string} eq 'r') {
      my $y = $xy->{staff0} + $off->{staffHeight};
      my $num = $n->{fret};
      if ($num <= 2) {
	my $ht = $ss / 4;
        $y -= ($num == 1) ? ($ss + $ht) : $ss * 2;
        _bg(BLACK, $x + ($u * $n->{pos}) - $u, $y, ($u * 2), $ht);
      } else {
	_textCenter($self,
		    $x + ($u * $n->{pos}),
		    $y - ($off->{staffHeight} / 2),
		    chr(64 + $num),
		    RESTS, $Tab->{symSize}, BLACK);
      }
    } else {
      my $y = $xy->{staff0};
      my($fidx,$fh,$offset,$clr);
      if ($n->{font} eq 'Normal') {
	$fidx = NOTES;
	$fh = $self->{Nsz};
	$offset = $self->{Ncap};
	$clr = $self->{Nclr};
      } else {
	$fidx = SNOTES;
	$fh = $self->{Ssz};
	$offset = $self->{Scap};
	$clr = $self->{Sclr};
      }
      $offset /= 2;
      my $adj = 1.0;
      $n->{x} = $x + ($u * $n->{pos});
      my $ht = $fh;
      $ht *= ($adj = 0.8) if ($n->{fret} eq 'X');
      $n->{y} = $y + ($ss * $n->{string}) - ($offset * $adj);
      my $hs = $self->{hscale}[$fidx];
      if ($n->{fret} eq 'X') {
	$clr = BLACK;
      } elsif ($n->{fret} > 9) {
	$self->{hscale}[$fidx] = 75;
      }
      _textCenter($self, $n->{x}, $n->{y}, $n->{fret}, $fidx, $ht, $clr);
      $self->{hscale}[$fidx] = $hs;
      if ($n->{op} ne '') {
	$n->{adj} = $adj;
	if ($n->{op} =~ /s|h/) {
	  slideHam($self,$n,$u,$ss);
	} elsif ($n->{op} =~ /b|r/) {
	  bendRel($self,$n,$u,$ss);
	}
      }
    }
  }
}

sub slideHam {
  my($self,$note,$u,$ss) = @_;

  my($fidx,$fh,$clr,$x,$xl,$y,$hu);
  if ($note->{font} eq 'Normal') {
    $fidx = NOTES;
    $fh = $self->{Nsz};
    $clr = $self->{Nclr};
  } else {
    $fidx = SNOTES;
    $fh = $self->{Ssz};
    $clr = $self->{Sclr};
  }

  $y = $note->{y};
  $hu = $u / 2;
  if ($note->{topos} > BARZERO && $note->{topos} < BAREND) {
    $x = $note->{x};
    $xl = $x + ($u * ($note->{topos} - $note->{pos}));
    _textCenter($self, $xl, $y, $note->{tofret}, $fidx, ($fh * $note->{adj}), $clr);
  } else {
    $xl = $note->{x};
    $x = $xl + ($u * ($note->{topos} - $note->{pos}));
  }
  $x -= $hu;
  $xl += $hu;
  my $y1 = $y;
  $GfxPtr->strokecolor($self->{Hclr});
  $GfxPtr->linewidth($Thick);
  if ($note->{op} eq 's') {
    $GfxPtr->linewidth($Line);
    $y  += ($note->{fret} < $note->{tofret}) ? ($ss * 1.1) : ($ss * 1.4);
    $y1 += ($note->{fret} < $note->{tofret}) ? ($ss * 1.4) : ($ss * 1.1);
    $GfxPtr->move($x, $y);
    $GfxPtr->line($xl, $y1);
  } else {
    $y += ($ss * 1.1);
    $y1 += ($ss * 1.8);
    $GfxPtr->move($x, $y);
    $GfxPtr->spline($x + (($xl - $x) / 2), $y1, $xl, $y);
    $GfxPtr->stroke();
    $GfxPtr->move($xl, $y);
    $GfxPtr->spline($x + (($xl - $x) / 2), $y1 + 1, $x, $y);
  }
  $GfxPtr->stroke();
}

sub bendRel {
  my($self,$note,$u,$ss) = @_;

  my $x = $note->{x};
  my $y = $note->{y};
  my $hu = $u / 2;
  $GfxPtr->linewidth($Thick);
  $GfxPtr->strokecolor($self->{Hclr});
  if ($note->{op} eq 'b') {
    $hu += $u;
    $x -= $u;
    $y += ($ss * 1.1);
    $GfxPtr->move($x, $y);
    $GfxPtr->spline($x + $hu, $y, $x + $hu, $y + ($u * 2));
  } else {
    $GfxPtr->linewidth($Fat);
    my $tx = ($note->{release} - $note->{pos}) * $u;
    my $hlf = $tx / 2;
    $tx += $x;
    $y += ($ss * 1.1);
    $GfxPtr->move($x, $y);
    $GfxPtr->spline($x + $hlf, $y, $x + $hlf, $y + ($u * 2));
    $GfxPtr->spline($x + ($hlf * 2), $y + ($u * 2), $x + ($hlf * 2), $y);
    my($fidx,$fh,$clr);
    if ($note->{font} eq 'Normal') {
      $fidx = NOTES;
      $fh = $self->{Nsz};
      $clr = $self->{Nclr};
    } else {
      $fidx = SNOTES;
      $fh = $self->{Ssz};
      $clr = $self->{Sclr};
    }
    _textCenter($self, $tx, $y - ($ss * 1.1), $note->{fret}, $fidx, $fh, $clr);
  }
  $GfxPtr->stroke();
}

sub lyrics {
  my($self,$lidx,$x,$y) = @_;

  if (defined $Tab->{lyrics}[$lidx]) {
    my $lyr = \@{$Tab->{lyrics}[$lidx]};
    #
    # Lyrics - This is REAL messy (see _adjTextAdd) because the same font
    # displays differently (length-wise) on a screen and on a PDF page :-(.
    #
    $y -= ($Tab->{pOffset}{lyricY} - 3);
    foreach my $idx (0..($Opt->{LyricLines} - 1)) {
      $y -= ($self->{Wsz} + 0.5);
      my $lyric = $lyr->[$idx];
      if (defined $lyric && $lyric ne '') {
	_adjTextAdd($self, $x, $y, $lyric, $Tab->{wordFont}, WORDS, $self->{Wsz}, $self->{Wclr});
      }
      $y -= $Tab->{lyricSpace};
    }
  }
}

sub _adjTextAdd {
  my($self,$x,$y,$txt,$fnt,$fidx,$sz,$bg) = @_;

  my $can = $Tab->{pCan};
  my $id = $can->create_text(0,0, -text => $txt, -anchor => 'nw', -font => $fnt);
  my($x1,$y1,$len,$y2) = split(/ /, $can->bbox($id));
  $can->delete($id);
  my $plen = _measure($self, $txt, $fidx, $sz, 100);
  $len /= $plen;
  $self->{hscale}[$fidx] = int(($len * 100) + 0.5);
  _textAdd($self, $x, $y, $txt, $fidx, $sz, $bg);
  $self->{hscale}[$fidx] = 100;
}

sub _textAdd {
  my($self,$x,$y,$txt,$fidx,$sz,$bg) = @_;

  $TextPtr->hscale($self->{hscale}[$fidx]);
  $TextPtr->font($self->{font}[$fidx], $sz);
  $TextPtr->fillcolor($bg);
  $TextPtr->translate($x, $y);
  # Returns the width of the text added
  int($TextPtr->text($txt) + 0.5);
}

sub _textRight {
  my($self,$x,$y,$txt,$fidx,$sz,$bg) = @_;

  $TextPtr->hscale($self->{hscale}[$fidx]);
  $TextPtr->font($self->{font}[$fidx], $sz);
  $TextPtr->fillcolor($bg);
  $TextPtr->translate($x, $y);
  # Returns the width of the text added
  int($TextPtr->text_right($txt) + 0.5);
}

sub _textCenter {
  my($self,$x,$y,$txt,$fidx,$sz,$bg) = @_;

  $TextPtr->hscale($self->{hscale}[$fidx]);
  $TextPtr->font($self->{font}[$fidx], $sz);
  $TextPtr->fillcolor($bg);

  $TextPtr->translate($x, $y);
  $TextPtr->text_center($txt);
}

sub _measure {
  my($self,$txt,$fidx,$sz,$scale) = @_;

  $TextPtr->hscale($scale);
  $TextPtr->font($self->{font}[$fidx], $sz);
  my $tx = $TextPtr->advancewidth($txt);
  int($tx + 0.5);
}

sub _hline {
  my($x,$y,$x1,$t,$clr) = @_;

  $GfxPtr->linewidth($t);
  $GfxPtr->strokecolor($clr);
  $GfxPtr->fillcolor($clr);
  $GfxPtr->move($x, $y);
  $GfxPtr->hline($x1);
  $GfxPtr->stroke();
}

sub _vline {
  my($x,$y,$y1,$t,$clr) = @_;

  $GfxPtr->linewidth($t);
  $GfxPtr->strokecolor($clr);
  $GfxPtr->fillcolor($clr);
  $GfxPtr->move($x, $y);
  $GfxPtr->vline($y1);
  $GfxPtr->stroke();
}

sub _bg {
  my($bg,$x,$y,$width,$ht) = @_;

  if ($bg ne '') {
    $GfxPtr->fillcolor($bg);
    $GfxPtr->rect($x, $y, $width, $ht);
    $GfxPtr->fill();
  }
}

sub close {
  my($self) = shift;
  $self->{pdf}->update();
  $self->{pdf}->end();
}

1;
