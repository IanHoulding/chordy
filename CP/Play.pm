package CP::Play;

# This file is part of Chordy.
#
###############################################################################
# Copyright (c) 2018 Ian Houlding
# All rights reserved.
# This program is free software.
# You can redistribute it and/or modify it under the same terms as Perl itself.
###############################################################################

use strict;
use warnings;

use CP::Global qw/:OS :WIN :OPT :TAB/;
use CP::Cconst qw/:COLOUR :TAB :PLAY/;

BEGIN {
  my $os = Tkx::tk_windowingsystem();
  if ($os eq 'win32') {
    require Win32::Sound;
  } elsif ($os eq 'aqua') {
  }
}

use CP::Tab;
use CP::Bar;

use Time::HiRes qw(usleep);

my $Rate = 8000;
my @Bars = ();
my %Note = ();
my $WAV = undef;

my $Tick = '';
foreach my $tick (qw/-0.02573 0.09296 -0.14412 -0.10788 0.41505 0.14239 -0.78552 -0.16894 1.00000
		  0.23279 -0.94127 -0.34776 0.88395 0.06776 -0.44626 0.18490 -0.05709 0.25090
		  0.12904 0.41834 -0.12845 -0.28892 0.12337 -0.05322 -0.10887 0.08143 0.28843
		  0.00267 -0.35518 -0.03203 0.22992 0.01731 -0.02241 -0.09759 -0.13836 0.25675
		  0.15917 -0.29398 -0.19234 0.31417 0.16480 -0.17135 -0.17291 -0.03234 0.05648
		  0.22214 0.08178 -0.24230 -0.07503 0.14135 0.00356 -0.20740 0.16615 0.14745
		  -0.11161/) {
  $Tick .= pack("s", $tick);
}

# 5 String Bass tuning: B E A D G
my @baseNotes = (qw/30.868 41.203 55 73.416 97.999/);

my $BarIdx = 0;
my $Beat = 0;

my $BpChan = 16;
my $Nchan = 1;

my $Paused = 0;

my $AfterID = 0;

sub play {
  return if ($OS ne 'win32');
  if (! $Paused && ! defined $WAV) {
    newWav();
    my $fst = ($Tab->{select1}) ? $Tab->{select1} : $Tab->{bars};
    my $lst = ($Tab->{select2}) ? $Tab->{select2} : $Tab->{lastBar};
    @Bars = ();
    %Note = ();
    makeNotes($fst, $lst);
    $BarIdx = 0;
    $Beat = 0;
    note();
  }
}

sub newWav {
  $WAV = new Win32::Sound::WaveOut($Rate, $BpChan, $Nchan);
}

sub pause {
  $WAV->Pause();
}

sub resetWav {
  $WAV->Pause();
  $WAV->Reset();
}

sub load {
  my($idx) = shift;

  $WAV->Load($Note{$idx});
  $WAV->Write();
}

sub unload {
  $WAV->Unload();
  $WAV->CloseDevice();
}

sub note {
  $AfterID = Tkx::after(int(7500/$Tab->{tempo}), \&note); # Same as: 600000/($Tab->{tempo}*8)
  if ($Tab->{play} == PLAY || $Tab->{play} == LOOP || $Tab->{play} == MET) {
    $Paused = 0 if ($Paused);
    if ($BarIdx == @Bars) {
      if ($Tab->{play} == LOOP) {
	$BarIdx = 0;
      } else {
	stop();
	return;
      }
    }
    my $iv = $Bars[$BarIdx];
    if (defined $iv->{$Beat} && $Tab->{play} != MET) {
      resetWav();
      my $idx = $iv->{$Beat};
      if ($idx ne 'r') {
	load($idx);
      }
    }
    if (($Beat % 8) == 0) {
      #
      # Draw the vertical red "beat" bar.
      #
      my $bar = $iv->{bar};
      if ($bar->{pnum} != $Tab->{pageNum}) {
	CP::Tab::newPage($bar->{pnum});
      }
      my($can,$X,$Y,$off) = ($bar->{canvas},$bar->{x},$bar->{y},$bar->{offset});
      my $ly = $Y + $off->{staffY} - 4;
      my $y2 = $Y + $off->{staff0} + 4;
      my $lx = $X + $off->{pos0} + ($off->{interval} * $Beat);
      $can->delete('beat');
      $can->create_line($lx, $ly, $lx, $y2,
			-width => 2, -fill => RED, -tags => 'beat');
      if ($Tab->{play} == MET) {
	resetWav();
	$WAV->Load($Tick);
	$WAV->Write();
      }
    }
    if (++$Beat == 32) {
      $Beat = 0;
      $BarIdx++;
    }
  } elsif ($Tab->{play} == STOP) {
    stop();
    return;
  } elsif ($Tab->{play} == PAUSE) {
    pause() if ($Paused == 0);
    $Paused++;
  }
}

sub stop {
  Tkx::after_cancel($AfterID);
  my $can = $Bars[0]->{bar}{canvas};
  $can->delete('beat') if (defined $can);
  CP::Tab::ClearSel();
  if ($OS eq 'win32') {
    resetWav();
    unload();
  } elsif ($OS eq 'aqua') {
  }
  $WAV = undef;
  @Bars = ();
  %Note = ();
}

sub makeNotes {
  my($first,$last) = @_;

  my $time = $Rate * 2;  # how long we want a note to last
  my $two12 = 2 ** (1/12);
  for(my $bar = $first; $bar != 0; $bar = $bar->{next}) {
    my $IV = {};
    $IV->{bar} = $bar;
    foreach my $nt (@{$bar->{notes}}) {
      my $str = $nt->{string};
      my $int = $nt->{pos};
      my $frt = $nt->{fret};
      next if ($frt eq 'X');
      if ($str eq 'r') {
	$IV->{$int} = 'r';
      } elsif ($frt ne 'X') {
	#
	##### ONLY DONE FOR BASSes AT THE MO'
	#
	$str += 1 if ($Opt->{Instrument} eq 'Bass4');
	my $n = "$str.$frt";
	$IV->{$int} = "$n";
	if (!defined $Note{$n}) {
	  $Note{$n} = '';
	  my($c1,$c2,$c3) = (0,0,0);
	  # formula for any given note is:
	  #   fn = f0 * (a)^n
	  # where:
	  #   fn = the frequency of the note n frets away from f0.
	  #   f0 = the frequency of one fixed note which must be defined.
	  #   a = (2)^1/12 = the twelth root of 2 = 1.059463094359...
	  #   n  = the number of frets away from the fixed note you are.
	  my $f1 = $baseNotes[$str];
	  $f1 *= ($two12 ** $frt) if ($frt > 0);
	  my $i1 = $f1 / $Rate;
	  my $i2 = $i1 * 2; #($f1 * 2) / $Rate;     # 2nd harmonic
	  my $i3 = $i1 * 3; #($f1 * 3) / $Rate;     # 3rd harmonic
	  my $vol = 32766;
	  for my $i (reverse 1..$time) { # Generate $time samples ( = 2 second)
	    # Calculate the pitch
	    # (range 0..255 for 8 bits)
	    my $v1 = sin($c1*6.28) * $vol;
	    my $v2 = sin($c2*6.28) * ($vol/2); # 2nd harmonic
	    my $v3 = sin($c3*6.28) * ($vol/3); # 3rd harmonic
	    my $v = int(($v1 + $v2 + $v3) * 0.54); # 0.54 = 1 / (1 + 0.5 + 0.33)
	    # "pack" it
	    $Note{$n} .= pack("s", $v);
	    $c1 += $i1;
	    $c2 += $i2;
	    $c3 += $i3;
	    $vol -= ($vol * (1.5 / $i));
	  } # end for
	} # end if (!defined $Note{$n})
      } # end elsif ($frt ne 'X')
    }
    push(@Bars, $IV);
    last if ($bar == $last);
  }
}

sub makeTick {
} 
1;
