package CP::TabWin;

# List of tags used for various elements within the display
# where a # indicates the bar's page index.
#
#  EDIT PAGE
#  barn       Bar # header for edit display
#       b#    Lines which create bars on the page display
#  ebl        Lines which create the edit display bar
#       bg#   Bar background rectangle (always below b#/ebl)
#       det#  Detection rectangle (for page view) that is raised to the top
#
#  rep           Repeat start/end indicator
#  rep           Repeat end indicator
#       lyr#     Lyric lines
#  fret bar#     All notes/rests in a bar
#       bar#     All bar headers and repeats
#
#  pcnt     Bar numbers down the side of the page display
#  phdr     Everything in the page header - Title, Key, etc.

use strict;

use Tkx;
use CP::Cconst qw/:SHFL :TEXT :SMILIE :COLOUR :TAB :PLAY/;
use CP::Global qw/:FUNC :OS :OPT :WIN :XPM :CHORD :SCALE :TAB/;
use CP::Offset;
use CP::Cmsg;
use CP::Bar;
use CP::HelpTab;

my $helpWin = '';

#
# This whole module relies on the external global $Tab object being initialised.
#
sub editorWindow {
  if ($Tab->{eFrm} eq '') {
    my $outer = $MW->new_ttk__frame(qw/-relief raised -borderwidth 2/, -padding => [4,4,4,4]);
    $outer->g_pack(qw//);

    my $menuF = $outer->new_ttk__frame();
    $menuF->g_pack(qw/-side left -fill y/, -padx => [0,4]);

    menu_buttons($menuF);

    my $leftF = $outer->new_ttk__frame(qw/-borderwidth 0/);
    $leftF->g_pack(qw/-side left -anchor nw/);

    my $rightF = $outer->new_ttk__frame(qw/-borderwidth 0/);
    $rightF->g_pack(qw/-side left -anchor nw/, -padx => [8,0]);

    {
      $EditBar = CP::Bar->new($Tab->{eOffset});
      $EditBar->{pidx} = -1;

      my $efb = $leftF->new_ttk__frame(qw/-borderwidth 0/, -padding => [8,0,4,4]);
      $efb->g_pack(qw/-side top -fill x/);
      eButtons($efb);

      $Tab->{eFrm} = $leftF->new_ttk__frame(); #qw/-relief solid -borderwidth 1/);
      $Tab->{eFrm}->g_pack(qw/-side top -expand 0/);
      editCanvas();

      my $frnum = $leftF->new_ttk__frame(qw/-borderwidth 0/);
      $frnum->g_pack(qw/-side top -expand 1 -fill x/, -padx => [4,0]);
      editBarFret($frnum);

      my $frsb = $leftF->new_ttk__labelframe(-text => ' Slide/Hammer - Bend/Release ',-padding => [4,0,4,4]);
      $frsb->g_pack(qw/-side top -expand 1 -fill x/, -padx => [4,0]);
      editBarSHBR($frsb);

      my $baropt = $leftF->new_ttk__labelframe(-text => ' Bar Options ', -padding => [4,0,4,4]);
      $baropt->g_pack(qw/-side top -expand 1 -fill x/, -padx => [4,0], -pady => [12,0]);
      editBarOpts($baropt);

      my $pgopt = $leftF->new_ttk__labelframe(-text => ' Page Options ', -padding => [4,0,4,4]);
      $pgopt->g_pack(qw/-side top -expand 1 -fill x/, -padx => [4,0], -pady => [8,0]);
      editPageOpts($pgopt);
    }

    {
      my $pfm = $rightF->new_ttk__frame();
      $pfm->g_pack(qw/-side top -anchor nw -expand 0 -fill y/, -pady => [4,0]);

      my $pfn = $Tab->{nFrm} = $pfm->new_ttk__frame();
      $pfn->g_pack(qw/-side left -anchor nw -expand 1 -fill y/);

      $Tab->{nCan} = $pfn->new_tk__canvas(
	-bg => MWBG,
	-relief => 'solid',
	-borderwidth => 0,
	-highlightthickness => 0,
	-selectborderwidth => 0,
	-width => BNUMW,
	-height => $Media->{height});
      $Tab->{nCan}->g_pack(qw/-side top -expand 0 -fill y/);

      $Tab->{pFrm} = $pfm->new_ttk__frame(qw/-borderwidth 0/);
      $Tab->{pFrm}->g_pack(qw/-side left -anchor nw -expand 1 -fill y/);
      pageCanvas();
###
      my $pft = $pfm->new_ttk__frame(qw/-relief raised -borderwidth 2/, -padding => [0,2,0,2]);
      $pft->g_pack(qw/-side right -anchor n/, -padx => [8,4]);
      pButtons($pft);

###
      if ($OS eq 'win32') {
	my $pfb = $rightF->new_ttk__frame(qw/-borderwidth 0/);
	$pfb->g_pack(qw/-side bottom -anchor nw -expand 1 -fill x/, -pady => [0,4]);
	pagePlay($pfb);
      }

    }
  } else {
    editCanvas();
    pageCanvas();
  }
}

sub menu_buttons {
  my($frame) = shift;

  my $topF = $frame->new_ttk__frame(qw/-relief raised -borderwidth 2/, -padding => [0,2,0,0]);
  $topF->g_pack(qw/-side top -pady 4/);
  my $midF = $frame->new_ttk__labelframe(-text => ' Edit ', -padding => [0,0,0,2]);
  $midF->g_pack(qw/-side top -pady 8/);
  my $botF = $frame->new_ttk__frame(qw/-relief raised -borderwidth 2/, -padding => [4,2,4,2]);
  $botF->g_pack(qw/-side bottom/);

  #   Image       Balloon        Function           row,column,rowspan,columnspan
  my $items = [
    [['open',       'Open Tab',    'main::openTab',   0,0,1,1],
     ['close',      'Close Tab',   'main::closeTab',  0,1,1,1]],
    [['new',        'New Tab',     'main::newTab',    1,0,1,1],
     ['delete',     'Delete Tab',  'main::delTab',    1,1,1,1]],
    [['SEP',        '',            '',                2,0,1,2]],
    [['save',       'Save File',   'main::saveTab',   3,0,1,1],
     ['saveAs',     'Save File As','main::saveTabAs', 3,1,1,1]],
    [['Rename Tab', 'Rename Current Tab', 'main::renameTab', 4,0,1,2]],
    [['Export Tab', 'Export Current Tab', 'main::exportTab', 5,0,1,2]],
    [['SEP',        '',            '',                6,0,1,2]],
    [['viewPDF',    'View PDF',    'main::viewPDF',   7,0,1,1],
     ['saveclose',  'Save, Make and Close', 'main::saveCloseTab', 7,1,4,1]],
    [['makePDF',    'Make PDF',    'main::makePDF',   8,0,1,1]],
    [['printPDF',   'Print PDF',   'main::printPDF',  9,0,1,1]],
    [['batch',      'Batch Make PDF', 'CP::TabPDF::batch', 10,0,1,1]],
    [['SEP',        '',            '',               11,0,1,2]],
    [['exit',       'Exit',        'main::exitTab',  12,0,1,2]],
    [['SEP',        '',            '',               13,0,1,2]],
    [['viewlog',    'View Error Log',  'viewElog',   14,0,1,1],
     ['clearlog',   'Clear Error Log', 'clearElog',  14,1,1,1]],
    [['cleartabs',  'Delete Tab Backups', sub{DeleteBackups('.tab',$Path->{Temp})}, 15,0,1,2]],
      ];
  foreach my $r (@{$items}) {
    foreach my $c (@{$r}) {
      oneMbutton($topF, @{$c});
    }
  }

  foreach my $t (["Collection", '', 'main::collectionSel', 0,0,1,1],
		 ["Media",      '', 'main::mediaSel',      1,0,1,1],
		 ["Fonts",      '', 'main::fontEdit',      2,0,1,1] ) {
    oneMbutton($midF, @{$t});
  }

  CORE::state $helpWin = '';
  foreach my $t (['About', 'Show Version', 'showVersion', 0,0,1,1],
		 ['Help',  'Tab Help', sub{$helpWin=CP::HelpTab::help($helpWin)}, 1,0,1,1] ) {
    oneMbutton($botF, @{$t});
  }
}

sub oneMbutton {
  my($frm,$img,$desc,$f,$row,$col,$rspn,$cspn) = @_;

  my $but;
  my @anc = ();
  if ($img eq 'SEP') {
    $but = $frm->new_ttk__separator(qw/-orient horizontal/);
    @anc = qw/-sticky we/;
  } else {
    my $func = (ref($f) eq 'CODE') ? $f : \&$f;
    if (makeImage("$img", \%XPM) ne '') {
      $but = $frm->new_ttk__button(-image => $img, -command => $func);
      balloon($but, $desc);
    } else {
      $but = $frm->new_ttk__button(-text => $img, -command => $func);
    }
    @anc = qw/-padx 4/;
  }
  $but->g_grid(-row => $row, -column => $col,
	       -rowspan => $rspn, -columnspan => $cspn,
	       -pady => 4, @anc);
}

sub showVersion{
  message(SMILE, "Version $Version\nian\@houlding.me.uk");
}

############################
############################
##
## Edit Frame (left) Section
##
############################
############################
sub eButtons {
  my($frm) = @_;

  my @eb = (
    [[' >>> Cancel  << ', 'Cancel', 'Red',   2],
    ],
    [[' Clear Bar ', 'Clear',  'Red',   2],
    ],
    [['Set Background',     'Background',  'Green', 1],
     [' Clear Background ', 'ClearEditBG', 'Red',   1],
    ],
    [[' Insert Before ', 'InsertBefore', 'Green', 1],
     ['Insert After',    'InsertAfter',  'Green', 1],
    ],
    [['Update', 'Update', 'Green', 1],
     ['Save', 'Save', 'Green', 1],
    ],
      );
  my $col = 0;
  foreach my $t (@eb) {
    my $rs = @{$t};
    foreach my $row (0..1) {
      next if ($row == $rs);
      my($txt,$func,$fg,$rowsp) = @{$t->[$row]};
      my $b = $frm->new_ttk__button(
	-text => $txt,
	-style => "$fg.TButton",
	-command => sub{$EditBar->$func()});
      $b->g_grid(-sticky => 'ew', -padx => 6, -pady => 2,
		 -row => $row, -column => $col, -rowspan => $rowsp);
    }
    $col++;
  }
}

sub editCanvas {
  my $ecan = $Tab->{eCan};
  my $eh = $Tab->{eOffset}{height} + (INDENT * 2);
  my $ew = (INDENT * 2) + POSIX::ceil($Tab->{eOffset}{width}) + INDENT;
  $Tab->{eFrm}->configure(-width => $ew, -height => $eh);
  if ($ecan ne '') {
    $ecan->delete('all');
    $ecan->configure(-width => $ew, -height => $eh);
  } else {
    $ecan = $Tab->{eCan} = $Tab->{eFrm}->new_tk__canvas(
      -width => $ew,
      -height => $eh,
      -bg => "WHITE",
      -relief => 'solid',
      -borderwidth => 1);
    $ecan->g_pack(qw//);
  }

  my $hd = int($Tab->{barnSize} * 1.4);
  # Green area at the top for the Bar number
  $ecan->create_rectangle(0, 0, $ew+3, $hd, -fill => DPOPBG, -width => 0);
  $ecan->create_line(0, $hd, $ew+3, $hd, -fill => BROWN, -width => 1);
  $Tab->{editX} = 0;
  $Tab->{editY} = $hd;
  $EditBar->{canvas} = $ecan;
  $EditBar->{x} = INDENT * 2;
  $EditBar->{y} = $Tab->{editY} + INDENT;
  $EditBar->outline();
}

sub editBarFret {
  my($frm) = shift;

  ####################
  # Fret Numbers .....
  ####################
  my $fr1 = $frm->new_ttk__labelframe(-text => ' Fret Number ', -padding => [4,0,4,4]);
  $fr1->g_grid(qw/-row 0 -column 0 -sticky n -pady 4/, -padx => [0,4]);

  my $i = -1;
  foreach my $row (0..2) {
    foreach my $clm (0..8) {
      my $txt = ($row | $clm) ? $i : 'X';
      my $rb = $fr1->new_ttk__radiobutton(
	-text => "$txt",
	-width => 2,
	-variable => \$Tab->{fret},
	-value => $txt,
	-style => 'Toolbutton',
	);
      $rb->g_grid(-row => $row, -column => $clm, -padx => 3, -pady => 3);
      $i++;
    }
  }

  #############
  # Rests .....
  #############
  my $fr2 = $frm->new_ttk__labelframe(-text => ' Rest ', -padding => [0,0,4,4]);
  $fr2->g_grid(qw/-row 0 -column 1 -sticky new -pady 4/, -padx => [4,0]);

  my @r = (qw/1 2 4 8 16 32/);
  foreach my $row (0..1) {
    my $col = 0;
    foreach (0..2) {
      my $r = shift(@r);
      my $val = "r$r";
      my $lbl = ($r > 1) ? "1/$r" : $r;
      my $but = "b$r";
      my $lb = $fr2->new_ttk__label(-text => $lbl, -width => 4, -anchor => 'e');
      $lb->g_grid(-sticky => 'e', -row => $row, -column => $col++);
      makeImage("$but", \%XPM);
      my $rb = $fr2->new_ttk__radiobutton(
	-image => $but,
	-variable => \$Tab->{fret},
	-value => $val,
	-style => 'Toolbutton',
	  );
      $rb->g_grid(-sticky => 'w', -row => $row, -column => $col++, -padx => 2, -pady => 3);
    }
  }
}

sub editBarSHBR {
  my($frm) = shift;

  ###########################
  # Slide/Hammer Bend/Release
  ###########################
  my $col = 0;
  foreach my $b (['Slide','s'], ['Hammer','h'], ['Bend','b'], ['Bend/Release','r',]) {
      my $rb = $frm->new_ttk__radiobutton(
	-text => $b->[0],
	-variable => \$Tab->{sh},
	-value => $b->[1],
	-style => 'Toolbutton',
	-command => \&CP::Tab::checkSelect);
    $rb->g_grid(qw/-sticky w -padx 10 -row 0 -column/ => $col++);
  }
}

###################
# Bar Options .....
###################
sub editBarOpts {
  my($frm) = @_;

  my $lb1 = $frm->new_ttk__label(-text => 'Volta Bracket');

  my $vvar = 'None';
  my $mb1 = $frm->new_ttk__button(
    -textvariable => \$EditBar->{volta},
    -width => 7,
    -style => 'Menu.TButton',
    -command => sub{
      popMenu(\$EditBar->{volta},
	      sub{$EditBar->topBar()},
	      [qw/None Left Center Right Both/]);
    });
  my $lb2 = $frm->new_ttk__label(-text => 'Header Text');
  my $ent = $frm->new_ttk__entry(
    -width => 20,
    -validate => 'key',
    -validatecommand => [\&CP::Tab::topVal, Tkx::Ev("%P")]);
  $EditBar->{topEnt} = $ent;
  $ent->configure(-invalidcommand => sub{Tkx::bell();$ent->configure(-validate => 'key');});
  my $lb3 = $frm->new_ttk__label(-text => 'Justify -');
  my $mb2 = $frm->new_ttk__button(
    -textvariable => \$EditBar->{justify},
    -width => 6,
    -style => 'Menu.TButton',
    -command => sub{
      popMenu(\$EditBar->{justify},
	      sub{$EditBar->topText()},
	      [qw/Left Right/]);
    });
  my $lb4 = $frm->new_ttk__label(-text => 'Repeat');
  my $mb3 = $frm->new_ttk__button(
    -textvariable => \$EditBar->{rep},
    -width => 7,
    -style => 'Menu.TButton',
    -command => sub{
      popMenu(\$EditBar->{rep},
	      sub{$EditBar->repeat()},
	      [qw/None Start End/]);
    });
  my $lb5 = $frm->new_ttk__label(-text => 'Note Font');
  my $mb4 = $frm->new_ttk__button(
    -textvariable => \$Tab->{noteFsize},
    -width => 8,
    -style => 'Menu.TButton',
    -command => sub{
      popMenu(\$Tab->{noteFsize},
	      undef,
	      [qw/Normal Small/]);
    });
  my $lb6 = $frm->new_ttk__label(-text => "Bar Starts:");
  my $cb1 = $frm->new_ttk__checkbutton(
    -text => 'Line',
    -variable => \$EditBar->{newline},
    -command => sub{$EditBar->{newpage} = 0 if ($EditBar->{newline} == 1);});
  my $cb2 = $frm->new_ttk__checkbutton(
    -text => 'Page',
    -variable => \$EditBar->{newpage},
    -command => sub{$EditBar->{newline} = 0 if ($EditBar->{newpage} == 1);});

  $lb1->g_grid(qw/-row 0 -column 0 -sticky e/, -padx => [0,2],  -pady => [0,4]); #VB
  $mb1->g_grid(qw/-row 0 -column 1 -sticky w/, -padx => [0,16], -pady => [0,4]);
  $lb2->g_grid(qw/-row 0 -column 2 -sticky e/, -padx => [0,2],  -pady => [0,4]); #HT
  $ent->g_grid(qw/-row 0 -column 3 -columnspan 2 -sticky w/, -padx => [0,0],  -pady => [0,4]);
  $lb3->g_grid(qw/-row 0 -column 5 -sticky e/, -padx => [2,2],  -pady => [0,4]); #Just
  $mb2->g_grid(qw/-row 0 -column 6 -sticky w/, -padx => [0,0],  -pady => [0,4]);

  $lb4->g_grid(qw/-row 1 -column 0 -sticky e/, -padx => [0,2],  -pady => [0,4]); #Rep
  $mb3->g_grid(qw/-row 1 -column 1 -sticky w/, -padx => [0,16], -pady => [0,4]);
  $lb5->g_grid(qw/-row 1 -column 2 -sticky e/, -padx => [0,2],  -pady => [0,4]); #NF
  $mb4->g_grid(qw/-row 1 -column 3 -sticky w/, -padx => [0,0],  -pady => [0,4]);
  $lb6->g_grid(qw/-row 1 -column 4 -sticky e/); # Bar Starts
  $cb1->g_grid(qw/-row 1 -column 5/);
  $cb2->g_grid(qw/-row 1 -column 6 -sticky w/);
}

####################
# Page Options .....
####################
sub editPageOpts {
  my($subfrm) = @_;

  my @menu_opt = qw/
      -width 3
      -relief solid
      -borderwidth 1
      -anchor c
      -direction flush
      -indicatoron 0
      -tearoff 0
      -pady 0/;
  my $frt = $subfrm->new_ttk__frame();
  $frt->g_pack(qw/-side top -expand 1 -fill both/);

  my $hl = $subfrm->new_ttk__separator(qw/-orient horizontal/);
  $hl->g_pack(qw/-side top -expand 1 -fill x -pady 4/);

  my $frm = $subfrm->new_ttk__frame(-padding => [0,0,0,4]);
  $frm->g_pack(qw/-side top -expand 1 -fill both/);

  my $frb = $subfrm->new_ttk__labelframe(-text => ' Transpose ', -padding => [4,0,4,0]);
  $frb->g_pack(qw/-side top -expand 1 -fill both/);

##########
  
  my $lb2 = $frt->new_ttk__label(-text => 'Tab File Name');
  my $en2 = $frt->new_ttk__entry(-textvariable => \$Tab->{fileName}, -width => 40,
				 -state => 'disabled');
  my $lb3 = $frt->new_ttk__label(-text => 'PDF File Name');
  my $en3 = $frt->new_ttk__entry(-textvariable => \$Tab->{PDFname}, -width => 40);
  my $lb4 = $frt->new_ttk__label(-text => 'Title');
  my $en4 = $frt->new_ttk__entry(-textvariable => \$Tab->{title}, -width => 40);
  $en4->g_bind("<KeyRelease>" => sub{CP::Tab::pageTitle();$Tab->{edited}++;});
  my $lb5 = $frt->new_ttk__label(-text => 'Set Key as');
  my $me1 = $frt->new_ttk__button(
    -textvariable => \$Tab->{key},
    -width => 3,
    -style => 'Menu.TButton',
    -command => sub{popMenu(\$Tab->{key},
			    sub{CP::Tab::pageKey();$Tab->{edited}++;},
			    [' ', qw/Ab A A# Bb B C C# Db D D# Eb E F F# Gb G G#/]);
    });
  my $lb6 = $frt->new_ttk__label(-text => 'Heading Note');
  my $en6 = $frt->new_ttk__entry(
    -textvariable => \$Tab->{note},
    -width        => 30);
  $en6->g_bind("<KeyRelease>" => sub{CP::Tab::pageNote();$Tab->{edited}++;});

  $lb2->g_grid(qw/-row 0 -column 0 -sticky e/, -padx => [0,2], -pady => [0,4]);  # TFN
  $en2->g_grid(qw/-row 0 -column 1 -sticky w/, -padx => [0,0], -pady => [0,4]);
  $lb3->g_grid(qw/-row 1 -column 0 -sticky e/, -padx => [0,2], -pady => [0,4]);  # PDF FN
  $en3->g_grid(qw/-row 1 -column 1 -sticky w/, -padx => [0,0], -pady => [0,4]);
  $lb4->g_grid(qw/-row 2 -column 0 -sticky e/, -padx => [0,2], -pady => [0,4]);  # Title
  $en4->g_grid(qw/-row 2 -column 1 -sticky w/, -padx => [0,0], -pady => [0,4]);
  $lb6->g_grid(qw/-row 3 -column 0 -sticky e/, -padx => [0,2], -pady => [0,4]);  # HN
  $en6->g_grid(qw/-row 3 -column 1 -sticky w/, -padx => [0,0], -pady => [0,4]);
  $lb5->g_grid(qw/-row 3 -column 2 -sticky e/, -padx => [18,2], -pady => [0,4]);  # SKAs
  $me1->g_grid(qw/-row 3 -column 3 -sticky w/, -padx => [0,0], -pady => [0,4]);
##########

  my $inl = $frm->new_ttk__label(-text => 'Instrument');
  my $inm = $frm->new_ttk__button(
    -textvariable => \$Opt->{Instrument},
    -style => 'Menu.TButton',
    -width => 8,
    -command => sub{popMenu(\$Opt->{Instrument},
			    sub{CP::Tab::drawEditWin();$Tab->{edited}++;},
			    $Opt->{Instruments});
		    $Opt->save();
    });
###
  my $esl = $frm->new_ttk__label(-text => 'Edit Scale');
  my $esm = $frm->new_ttk__button(
    -textvariable => \$Opt->{EditScale},
    -style => 'Menu.TButton',
    -width => 3,
    -command => sub{popMenu(\$Opt->{EditScale},
			    sub{CP::Tab::drawEditWin()},
			    [qw/3 3.5 4 4.5 5 5.5 6/]);
		    $Opt->save();
    });
###
  my $tml = $frm->new_ttk__label(-text => 'Timing');
  my $tmm = $frm->new_ttk__button(
    -textvariable => \$Opt->{Timing},
    -style => 'Menu.TButton',
    -width => 4,
    -command => sub{popMenu(\$Opt->{Timing},
			    sub{CP::Tab::drawEditWin();$Tab->{edited}++;},
			    [qw{2/4 3/4 4/4}]);
    });
###
  my $lll = $frm->new_ttk__label(-text => "Lyric Lines");
  my $llm = $frm->new_ttk__button(
    -textvariable => \$Opt->{LyricLines},
    -style => 'Menu.TButton',
    -width => 3,
    -command => sub{popMenu(\$Opt->{LyricLines},
			    sub{CP::Tab::drawEditWin();$Tab->{edited}++;},
			    [qw/0 1 2 3/]);
    });
######
  my $bsl = $frm->new_ttk__label(-text => "Bars/Stave");
  my $bsm = $frm->new_ttk__button(
    -textvariable => \$Opt->{Nbar},
    -style => 'Menu.TButton',
    -width => 3,
    -command => sub{popMenu(\$Opt->{Nbar},
			    sub{CP::Tab::drawEditWin();$Tab->{edited}++;},
			    [qw/3 4 5 6 7 8/]);
		    $Opt->save();
    });
###
  my $ssl = $frm->new_ttk__label(-text => "Stave Line Spacing");
  my $ssm = $frm->new_ttk__button(
    -textvariable => \$Opt->{StaffSpace},
    -style => 'Menu.TButton',
    -width => 3,
    -command => sub{popMenu(\$Opt->{StaffSpace},
			    sub{CP::Tab::drawEditWin();$Tab->{edited}++;},
			    [qw/8 9 10 11 12 13 14 15 16/]);
		    $Opt->save();
    });
###
  my $sgl = $frm->new_ttk__label(-text => "Inter-Stave Gap");
  my $sgm = $frm->new_ttk__button(
    -textvariable => \$Tab->{staveGap},
    -style => 'Menu.TButton',
    -width => 3,
    -command => sub{popMenu(\$Tab->{staveGap},
			    sub{
			      CP::Tab::drawEditWin();
			      $Tab->{edited}++;
			    },
			    [qw/0 2 4 6 8 10 12 14 16 18 20/]);
    });
###
  my $lsl = $frm->new_ttk__label(-text => "Lyric Spacing");
  my $lsm = $frm->new_ttk__button(
    -textvariable => \$Tab->{lyricSpace},
    -style => 'Menu.TButton',
    -width => 3,
    -command => sub{popMenu(\$Tab->{lyricSpace},
			    sub{CP::Tab::drawEditWin();$Tab->{edited}++;},
			    [qw/0 2 4 6 8 10 12 14 16 18 20/]);
    });

  $inl->g_grid(qw/-row 0 -column 0 -sticky e/, -pady => [0,4]);
  $inm->g_grid(qw/-row 0 -column 1 -sticky w/, -padx => [2,10], -pady => [0,4]);
  $tml->g_grid(qw/-row 1 -column 0 -sticky e/, -pady => [0,4]);
  $tmm->g_grid(qw/-row 1 -column 1 -sticky w/, -padx => [2,10], -pady => [0,4]);

  $esl->g_grid(qw/-row 0 -column 2 -sticky e/, -pady => [0,4]);
  $esm->g_grid(qw/-row 0 -column 3 -sticky w/, -padx => [2,10], -pady => [0,4]);
  $bsl->g_grid(qw/-row 1 -column 2 -sticky e/, -pady => [0,4]);
  $bsm->g_grid(qw/-row 1 -column 3 -sticky w/, -padx => [2,10], -pady => [0,4]);

  $ssl->g_grid(qw/-row 0 -column 4 -sticky e/, -pady => [0,4]);
  $ssm->g_grid(qw/-row 0 -column 5 -sticky w/, -padx => [2,10], -pady => [0,4]);
  $sgl->g_grid(qw/-row 1 -column 4 -sticky e/, -pady => [0,4]);
  $sgm->g_grid(qw/-row 1 -column 5 -sticky w/, -padx => [2,10], -pady => [0,4]);

  $lll->g_grid(qw/-row 0 -column 6 -sticky e/, -pady => [0,4]);
  $llm->g_grid(qw/-row 0 -column 7 -sticky w/, -padx => [2,0], -pady => [0,4]);
  $lsl->g_grid(qw/-row 1 -column 6 -sticky e/, -pady => [0,4]);
  $lsm->g_grid(qw/-row 1 -column 7 -sticky w/, -padx => [2,0], -pady => [0,4]);


##########

  my $lb7 = $frb->new_ttk__label(-text => 'Adjust Strings');
  $lb7->g_grid(qw/-row 0 -column 0 -sticky e/, -pady => [0,4]);

  my $cb = $frb->new_ttk__checkbutton(-variable => \$Opt->{Refret});
  $cb->g_grid(qw/-row 0 -column 1 -sticky w/, -pady => [0,4]);

  my $lb8 = $frb->new_ttk__label(-text => 'Semi-tones');
  $lb8->g_grid(qw/-row 0 -column 2 -sticky e/, -padx => [20,0], -pady => [0,4]);

  my $me7 = $frb->new_ttk__button(
    -textvariable => \$Tab->{trans},
    -width => 3,
    -style => 'Menu.TButton',
    -command => sub{
      popMenu(\$Tab->{trans},
	      sub {},
	      [qw/+12 +11 +10 +9 +8 +7 +6 +5 +4 +3 +2 +1 0
	       -1 -2 -3 -4 -5 -6 -7 -8 -9 -10 -11 -12/]);
    });
  $me7->g_grid(qw/-row 0 -column 3 -padx 5/, -pady => [0,4]);

  my $bu1 = $frb->new_ttk__button(
    -text    => ' Go ',
    -width   => 4,
    -command => sub{
      CP::Tab::transpose();
      $Tab->{edited}++;
    });
  $bu1->g_grid(qw/-row 0 -column 4 -padx 4 -sticky w/, -pady => [0,4]);

  my $lb9 = $frb->new_ttk__label(-text => 'One String');
  $lb9->g_grid(qw/-row 0 -column 5 -sticky e/, -padx => [20,0], -pady => [0,4]);

  my $bu2 = $frb->new_ttk__button(
    -text    => ' Up ',
    -width   => 4,
    -command => sub{
      CP::Tab::ud1string(+5);
      $Tab->{edited}++;
    });
  $bu2->g_grid(qw/-row 0 -column 6 -padx 4 -sticky e/, -pady => [0,4]);

  my $bu3 = $frb->new_ttk__button(
    -text    => ' Down ',
    -width   => 6,
    -command => sub{
      CP::Tab::ud1string(-5);
      $Tab->{edited}++;
    });
  $bu3->g_grid(qw/-row 0 -column 7 -padx 4 -sticky e/, -pady => [0,4]);
}

#############################
#############################
##
## Page Frame (right) Section
##
#############################
#############################

my %UPDN;

$UPDN{'up'} = <<'EOXPM';
/* XPM */
static char *up[] = {
"14 13 3 1",
". s None c None",
"x c #600060",
"c c #90c0c0",
"......cc......",
".....cxxc.....",
".....xxxx.....",
"....cxxxxc....",
"....xxxxxx....",
"...cxxxxxxc...",
"......xx......",
"......xx......",
"......xx......",
"......xx......",
"......xx......",
"......xx......",
"......xx......"};
EOXPM

$UPDN{'down'} = <<'EOXPM';
/* XPM */
static char *down[] = {
"14 13 3 1",
". s None c None",
"x c #600060",
"c c #90c0c0",
"......xx......",
"......xx......",
"......xx......",
"......xx......",
"......xx......",
"......xx......",
"......xx......",
"...cxxxxxxc...",
"....xxxxxx....",
"....cxxxxc....",
".....xxxx.....",
".....cxxc.....",
"......cc......"};
EOXPM

sub pButtons {
  my($frm) = shift;

  my @tb = (['Edit Bar',        \&CP::Tab::editBar,     'Green'],
	    ['Clone Bar(s)',    \&CP::Tab::Clone,       'Green'],
	    ['Copy Bar(s)',     \&CP::Tab::Copy,        'Green'],
	    ['SEP'],
	    ['Paste Over',      \&CP::Tab::PasteOver,   'Green'],
	    ['Paste Before',    \&CP::Tab::PasteBefore, 'Green'],
	    ['Paste After',     \&CP::Tab::PasteAfter,  'Green'],
	    ['SEP'],
	    ['Clear Selection', \&CP::Tab::ClearSel,    'Red'],
	    ['Clear Bar(s)',    \&CP::Tab::CleanBar,    'Red'],
	    ['Delete Bar(s)',   \&CP::Tab::DeleteBars,  'Red'],
	    ['SEP'],
	    ['Set Background',  \&CP::Tab::setBG,       'Green'],
	    ['Clear Background',\&CP::Tab::clearBG,     'Red'],
	    ['SEP'],
	    ['- Lyrics -',          0,                  'Green'],
            ['SEP'],
	    ['<<< Prev Page',        \&CP::Tab::PrevPage,    'Blue'],
	    ['>>> Next Page',        \&CP::Tab::NextPage,    'Blue'],
	    );
  my $row = 0;
  foreach my $r (@tb) {
    my($txt,$func,$fg) = @{$r};
    if ($txt eq 'SEP') {
      my $sep = $frm->new_ttk__separator(qw/-orient horizontal/);
      $sep->g_grid(-row => $row, qw/-sticky we -pady 8/);
    } elsif ($txt eq '- Lyrics -') {
      my $sfrm = $frm->new_ttk__frame(-padding => [0,2,0,2]);
      $sfrm->g_grid(-row => $row);

      makeImage("up", \%UPDN);
      my $up = $sfrm->new_ttk__button(-image => 'up', -command => \&CP::Tab::lyricUp);
      $up->g_grid(qw/-row 0 -column 0/);

      my $lb = $sfrm->new_ttk__label(-text => $txt);
      $lb->g_grid(qw/-padx 2 -row 0 -column 1/);

      makeImage("down", \%UPDN);
      my $dn = $sfrm->new_ttk__button(-image => 'down', -command => \&CP::Tab::lyricDown);
      $dn->g_grid(qw/-row 0 -column 2/);
    } else {
      my $bu = $frm->new_ttk__button(
	-text => $txt,
	-style => "$fg.TButton",
	-command => [$func, $Tab]);
      $bu->g_grid(-row => $row, qw/-sticky we -padx 4 -pady 4/);
    }
    $row++;
  }
}

sub pageCanvas {
  my $can = $Tab->{pCan};
  if ($can ne '') {
    if ($Opt->{LyricLines}) {
      # We need to preserve lyrics across Canvases
      CP::Tab::getLyrics();
    }
    $can->delete('all');
    $can->configure(-width => $Media->{width}, -height => $Media->{height});
  } else {
    $can = $Tab->{pCan} = $Tab->{pFrm}->new_tk__canvas(
      -bg => "WHITE",
      -width => $Media->{width},
      -height => $Media->{height},
      -relief => 'solid',
      -borderwidth => 1);
    $can->g_pack(qw/-side top -expand 0/);
  }

  my $off = $Tab->{pOffset};
  my $w = $off->{width};
  my $h = $off->{height};
  my $y = $Tab->{pageHeader} + INDENT;
  my $pidx = 0;
  foreach my $r (0..($Tab->{rowsPP} - 1)) {
    my $x = INDENT;
    if ($Opt->{LyricLines}) {
      $Tab->{lyricText}[$r] = lyricBox($x, $y + $off->{lyricY});
    }
    foreach (1..$Opt->{Nbar}) {
      CP::Bar::outline($Tab, $pidx++, $x, $y);
      $x += $w;
    }
    $y += $h;
  }
  CP::Tab::showLyrics() if ($Opt->{LyricLines});
}

sub lyricBox {
  my($x,$y) = @_;

  my $w = $Media->{width} - (INDENT * 2);
  my $h = $Tab->{pOffset}{lyricHeight};
  my $can = $Tab->{pCan};
  my $frm = $can->new_ttk__frame(-width => $w, -height => $h, -padding => [0,0,0,0]);
  my $wid = $frm->new_tk__text(
    qw/-wrap none -borderwidth 0 -padx 0 -pady 0/,
    -height => $Opt->{LyricLines},
    -spacing1 => 0,
    -spacing2 => 0,
    -spacing3 => $Tab->{lyricSpace},
    -font => $Tab->{wordFont},
    -bg => MWBG,
    -fg => $Tab->{wordColor});
  $wid->g_pack(qw/-anchor nw -fill x/);
  my $win = $can->create_window(
    $x, $y,
    -window => $frm,
    -width => $w,
    -height => $h,
    -anchor => 'nw');
  $wid->configure(-yscrollcommand => sub{yscroll($wid)}, -width => $w);
  $can->lower($win);
  return($wid);
}

sub yscroll {
  my($wid) = @_;

  my ($line,$col)= split(/\./, $wid->index('insert'));
  if ($line == ($Opt->{LyricLines} + 1)) {
    $wid->yview('scroll', -1, 'units');
    $wid->delete('end - 1 chars', 'end');
  }
}

my %CNTRL;

$CNTRL{'play'} = <<'EOXPM';
/* XPM */
static char *play[] = {
"18 18 3 1",
"  c None",
"x c #600060",
"c c #90c0c0",
"                  ",
"  c               ",
"  xc              ",
"  xxxc            ",
"  xxxxxc          ",
"  xxxxxxxc        ",
"  xxxxxxxxxc      ",
"  xxxxxxxxxxxc    ",
"  xxxxxxxxxxxxxc  ",
"  xxxxxxxxxxxxxc  ",
"  xxxxxxxxxxxc    ",
"  xxxxxxxxxc      ",
"  xxxxxxxc        ",
"  xxxxxc          ",
"  xxxc            ",
"  xc              ",
"  c               ",
"                  "};
EOXPM

$CNTRL{'pause'} = <<'EOXPM';
/* XPM */
static char *pause[] = {
"18 18 2 1",
"  c None",
"x c #600060",
"                  ",
"                  ",
"   xxxx    xxxx   ",
"   xxxx    xxxx   ",
"   xxxx    xxxx   ",
"   xxxx    xxxx   ",
"   xxxx    xxxx   ",
"   xxxx    xxxx   ",
"   xxxx    xxxx   ",
"   xxxx    xxxx   ",
"   xxxx    xxxx   ",
"   xxxx    xxxx   ",
"   xxxx    xxxx   ",
"   xxxx    xxxx   ",
"   xxxx    xxxx   ",
"   xxxx    xxxx   ",
"                  ",
"                  "};
EOXPM

$CNTRL{'stop'} = <<'EOXPM';
/* XPM */
static char *stop[] = {
"18 18 2 1",
"  c None",
"x c #600060",
"                  ",
"                  ",
"                  ",
"   xxxxxxxxxxxx   ",
"   xxxxxxxxxxxx   ",
"   xxxxxxxxxxxx   ",
"   xxxxxxxxxxxx   ",
"   xxxxxxxxxxxx   ",
"   xxxxxxxxxxxx   ",
"   xxxxxxxxxxxx   ",
"   xxxxxxxxxxxx   ",
"   xxxxxxxxxxxx   ",
"   xxxxxxxxxxxx   ",
"   xxxxxxxxxxxx   ",
"   xxxxxxxxxxxx   ",
"                  ",
"                  ",
"                  "};
EOXPM

$CNTRL{'loop'} = <<'EOXPM';
/* XPM */
static char *loop[] = {
"18 18 3 1",
"  c None",
"x c #600060",
"c c #90c0c0",
"             c    ",
"             xc   ",
"             xxc  ",
"   cxxxxxxxxxxxxc ",
"  cxxxxxxxxxxxxxxc",
"  xxxxxxxxxxxxxxc ",
"  xxxc       xxc  ",
"  xxxc       xc   ",
"              c   ",
"    c             ",
"   cx       cxxx  ",
"  cxx       cxxx  ",
" cxxxxxxxxxxxxxx  ",
"cxxxxxxxxxxxxxxc  ",
" cxxxxxxxxxxxxc   ",
"  cxx             ",
"   cx             ",
"    c             "};
EOXPM

$CNTRL{'metronome'} = <<'EOXPM';
/* XPM */
static char * metronome_xpm[] = {
"18 18 17 1",
"  c None",
". c #090C08",
"+ c #2B332F",
"@ c #4E5652",
"# c #74653B",
"$ c #8E6327",
"% c #6C7572",
"& c #98712C",
"* c #83775B",
"= c #a09050",
"- c #D8A860",
"; c #859190",
"> c #F97913",
", c #FF7C10",
"' c #9EA8A7",
") c #FFAA11",
"! c #E4C649",
"     -&&&&&-      ",
"     =======      ",
"    ;;%@@@%%      ",
"    ;;%...%%%     ",
"    ';%+++%%%  =- ",
"    ';%+++%%% =-  ",
"    ';%+++%%%,,   ",
"    ';%+++%%=,,   ",
"   '';%+++%=-=    ",
"   ';;%+++=-@%    ",
"   ';;%++=-%%%    ",
"   ';;%+=-%;;%%   ",
"  '';;%=-+%;;;%   ",
"  $$$$=-$$$$$$=   ",
"  =-&&&&&&&&&$$   ",
"  --&&&&&&&&&$$   ",
"  --&&&&&&&&&&$   ",
"   @+++++++++@    "};

EOXPM

sub pagePlay {
  my($fr) = shift;

  use CP::Play;

  my $lb = $fr->new_ttk__label(-text => 'Tempo: ');
  $lb->g_grid(qw/-row 0 -column 0 -sticky e/, -padx => [BNUMW*2,10]);

  my $sc;
  $sc = $fr->new_tk__scale(
    -variable => \$Tab->{tempo},
    -fg => DRED,
    -from => 40,
    -to => 200,
    -tickinterval => 40,
    -borderwidth => 0,
    -resolution => 1,
    -showvalue => 1,
    -length  => '6c',
    -orient  => 'horizontal',
    -bg => MWBG,
    -troughcolor => DRED,
    -command => sub {
      $sc->m_configure(-troughcolor => sprintf("#%02x0000", $Tab->{tempo} + 55));
      CP::Tab::pageTempo();
      $Tab->{edited} = 1 if ($Tab->{loaded});});
  $sc->g_grid(qw/-row 0 -column 1/);

  makeImage("stop", \%CNTRL);
  my $st = $fr->new_ttk__button(
    -image => 'stop',
    -command => sub{$Tab->{play} = STOP});
  $st->g_grid(qw/-row 0 -column 2 -padx 5/, -pady => [15,0]);

  makeImage("play", \%CNTRL);
  my $pl = $fr->new_ttk__button(
    -image => 'play',
    -command => sub{$Tab->{play} = PLAY; CP::Play::play();});
  $pl->g_grid(qw/-row 0 -column 3 -padx 5/, -pady => [15,0]);

  makeImage("pause", \%CNTRL);
  my $pa = $fr->new_ttk__button(
    -image => 'pause',
    -command => sub{$Tab->{play} = PAUSE});
  $pa->g_grid(qw/-row 0 -column 4 -padx 5/, -pady => [15,0]);

  makeImage("loop", \%CNTRL);
  my $pp = $fr->new_ttk__button(
    -image => 'loop',
    -command => sub{$Tab->{play} = LOOP; CP::Play::play();});
  $pp->g_grid(qw/-row 0 -column 5 -padx 5/, -pady => [15,0]);

  makeImage("metronome", \%CNTRL);
  my $pp = $fr->new_ttk__button(
    -image => 'metronome',
    -command => sub{$Tab->{play} = MET; CP::Play::play();});
  $pp->g_grid(qw/-row 0 -column 6 -padx 5/, -pady => [15,0]);
}

1;
