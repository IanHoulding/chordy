package CP::Tab;

# This file is part of Chordy.
#
###############################################################################
# Copyright (c) 2018 Ian Houlding
# All rights reserved.
# This program is free software.
# You can redistribute it and/or modify it under the same terms as Perl itself.
###############################################################################

# List of tags used for various elements within the display
# where a # indicates the bar's page index.
#
#  EDIT PAGE
#  barn       Bar # header for edit display
#       b#    Lines which create bars on the page display
#  ebl        Lines which create the edit display bar
#       bg#   Bar background rectangle (always below b#/ebl)
#       det#  Detection rectangle (for page view) that is raised to the top
#
#  rep           Repeat start/end indicator
#  rep           Repeat end indicator
#       lyr#     Lyric lines
#  fret bar#     All notes/rests in a bar
#       bar#     All bar headers and repeats
#
#  pcnt     Bar numbers down the side of the page display
#  phdr     Everything in the page header - Title, Key, etc.

use strict;
use warnings;

use CP::Cconst qw/:SHFL :TEXT :SMILIE :COLOUR :FONT :TAB/;
use CP::Global qw/:FUNC :OPT :WIN :XPM :CHORD :SCALE :TAB/;
use Tkx;
use File::Basename;
use File::Slurp;
use CP::Offset;
use CP::Bar;
use CP::Cmsg;
use CP::TabWin;
use POSIX;

our(@pageXY, $SaveID);

@Tuning = ();

# $fn is a complete path + file_name
#
sub new {
  my($proto,$fn) = @_;

  if (! defined $Tab) {
    my $class = ref($proto) || $proto;
    $Tab = {};
    bless $Tab, $class;
  }
  cleanTab();

  if (defined $fn) {
    $Tab->{fileName} = fileparse($fn);
    ($Tab->{title} = $Tab->{fileName}) =~ s/\.tab$//;
    $Tab->{PDFname} = $Tab->{title}.'.pdf';
    main::tabTitle($Tab->{fileName});
    load($fn);
    offsets();
    for(my $bar = $Tab->{bars}; $bar; $bar = $bar->{next}) {
      $bar->{offset} = $Tab->{pOffset};
    }
    if ($Opt->{AutoSave}) {
      $SaveID = Tkx::after(($Opt->{AutoSave} * 60000), \&save);
    }
  } else {
    $Tab->{title} = $Tab->{fileName} = '';
    offsets();
  }
  setXY();
  CP::TabWin::editorWindow(); # Also makes the Lyric Boxes
  if (defined $fn) {
    indexBars();
    pageHdr();
    newPage(0);
  }
}

sub cleanTab {
  $Opt->{AutoSave} = 0;
  # These keys do NOT get reset:
  #   eFrm eCan nFrm nCan pFrm pCan pOffset eOffset
  #
  $Tab->{pageNum} = 0;   # Keeps track of the current page we're working on. (1st page = 0)
  $Tab->{nPage} = 0;     # Keeps track of the number of pages.
  $Tab->{sh} = '';       # Can be one of 's', 'h', 'b' or 'r'.
  $Tab->{fret} = '';
  $Tab->{select1} = 0;
  $Tab->{select2} = 0;
  $Tab->{fileName} = '';
  $Tab->{PDFname} = '';
  $Tab->{title} = '';
  $Tab->{key} = '';
  $Tab->{note} = '';
  $Tab->{selected} = 0;
  $Tab->{trans} = 0;
  $Tab->{edited} = 0;
  $Tab->{bars} = 0;
  $Tab->{lastBar} = 0;
  $Tab->{staveGap} = 0;
  $Tab->{lyricSpace} = 0;
  $Tab->{lyricEdit} = 0;
  $Tab->{lyricText} = []; # array of Text widgets for 1 page
  $Tab->{lyrics} = [];    # array containing ALL the lyrics
  $Tab->{rests} = {};
  $Tab->{pstart} = [];
  $Tab->{noteFsize} = 'Normal';
}

#
# Each bar occupies a discreate rectangle on the page. This sub
# works out all the sizes and thence offset values (these are all
# in points).
# Everything changes if lyricLines, StaffSpace, bars/stave or any font changes.
#
sub offsets {
  # We can't work out the offsets unless
  # we have the correct Font distances.
  makeFonts();

  my %s = ();
  $s{scale} = 1;
  # thick must be twice thin
  $s{thick} = 2;
  $s{thin} = 1;
  $Tab->{pageHeader} = $Tab->{titleSize} + 4; # 2 above and 2 below
  $Tab->{barTop} = $Tab->{pageHeader} + INDENT;

  $s{width} = int(($Media->{width} - (INDENT * 2)) / $Opt->{Nbar});
  (my $t = $Opt->{Timing}) =~ s/(\d).*/$1/;
  $s{interval} = $s{width} / (($t * 8) + 3);
  # Distance between lines of a Staff.
  $s{staffSpace} = $Opt->{StaffSpace};
  $s{staffHeight} = $s{staffSpace} * ($Nstring - 1);

  # Something like a "Verse" above a bar is:
  #    Volta bar + The Text + 1/2 Note text size
  $s{staffX} = 0;
  $s{headY}  = $s{thick} + $Tab->{headSize};
  $s{staffY} = $s{header} = ceil($s{headY} + ($Tab->{noteCap} / 2));
  $s{staff0} = $s{header} + $s{staffHeight};

  # This is the TOP of the lyric area
  $s{lyricY} = int($s{staff0} + ceil($Tab->{noteSize} / 2));

  $s{lyricHeight} = 0;
  if ($Opt->{LyricLines}) {
    my $wid = $MW->new_tk__text(
      -height => $Opt->{LyricLines},
      -borderwidth => 0,
      -selectborderwidth => 0,
      -spacing1 => 0,
      -spacing2 => 0,
      -spacing3 => $Tab->{lyricSpace},
      -font => $Tab->{wordFont});
    $s{lyricHeight} = Tkx::winfo_reqheight($wid);
    $wid->g_destroy();
  }

  my $pht = $Media->{height} - $Tab->{barTop};

  $s{height} = $s{lyricY} + $s{lyricHeight} + $Tab->{staveGap};

  $Tab->{rowsPP} = int($pht / $s{height});
  $Tab->{barsPP} = $Opt->{Nbar} * $Tab->{rowsPP};
  $s{pos0} = $s{interval} * 2;

  if (! defined $Tab->{pOffset}) {
    $Tab->{pOffset} = CP::Offset->new(\%s);
  } else {
    $Tab->{pOffset}->update(\%s);
  }
  #
  # Now scale everything up for the Edit Bar
  #
  my $editsc = $Opt->{EditScale};
  foreach my $v (qw/interval pos0 scale staffHeight staffSpace width/) {
    $s{$v} *= $editsc;
  }
  # These entries are not just multiples of the Edit Scale
  # because they depend on the font size differences.
  $s{thick} = 4;
  $s{thin} = 2;
  $s{headY}  = $s{thick} + $Tab->{eheadSize};
  $s{staffY} = $s{header} = ceil($s{headY} + ceil($Tab->{enoteCap} / 2)); 
  $s{staff0} = $s{staffY} + $s{staffHeight};
  $s{height} = $s{staff0} + $Tab->{enoteCap};
  if (! defined $Tab->{eOffset}) {
    $Tab->{eOffset} = CP::Offset->new(\%s);
  } else {
    $Tab->{eOffset}->update(\%s);
  }
}

#
# Set up fonts
#
# As far as I can tell, text positioning is based on (Base Line - Descender).
# So if we use '-anchor => sw' in create_text, the lowest Descender (eg: the tail of a 'y')
# will sit on the 'y' coord. The text base line will be fnt->{des} above that and the top
# of the text will be fnt->{size) above the -anchor line.
# The formula ((fnt->{des} + $fnt->{size}) gives the MAXIMUM height of any text
# ie. Caps + descenders;
#
# One minor benefit of the Canvas text is that printing fret numbers is easy
# as create_text with no -anchor will center the number around the x,y co-ords.
#
sub makeFonts {
  my $es = $Opt->{EditScale};
  foreach my $fnt ([qw/Title  title/],
		   [qw/Header head/],
		   [qw/Notes  note/],
		   [qw/SNotes snote/],
		   [qw/Words  word/]) {
    my($med,$type) = @$fnt;
    my($fam,$size,$wt,$sl,$clr,$esize);
    my $font = "${type}Font";
    if (defined $Tab->{$font} && $Tab->{$font} =~ /{([^}]*)}\s+(\d+)\s+(\S+)\s+(\S+)\s+(\S+)/) {
      $fam = $1;
      $size = $2;
      $wt = $3;
      $sl = $4;
      $clr = $5;
    } else {
      my $fp = $Media->{$med};
      $fam = $fp->{family};
      $size = $fp->{size};
      $wt = $fp->{weight};
      $sl = $fp->{slant};
      $clr = $fp->{color};
    }
    $esize = int($size * $es);
    # Remove the colour element to form a pure font spec.
    $Tab->{"$font"} = "{$fam} $size $wt $sl";
    my $dsc = Tkx::font_metrics($Tab->{"$font"}, '-descent');
    $Tab->{"${type}Cap"} = $size - $dsc;
    $Tab->{"${type}Size"} = $size;
    $Tab->{"e$font"} = "{$fam} $esize $wt $sl";
    $dsc = Tkx::font_metrics($Tab->{"e$font"}, '-descent');
    $Tab->{"e${type}Cap"} = $esize - $dsc;
    $Tab->{"e${type}Size"} = $esize;

    $Tab->{"${type}Color"} = $clr;
  }
  
  my $keyS = int($Tab->{titleSize} * KEYMUL);
  $Tab->{keyFont} = newFont($Tab->{titleFont}, $keyS);
  $Tab->{keySize} = $keyS;

  $keyS = int($Tab->{titleSize} * PAGEMUL);
  $Tab->{pageFont} = newFont($Tab->{titleFont}, $keyS);
  $Tab->{pageSize} = $keyS;

  $Tab->{barnFont} = "{Times New Roman} 12 bold roman";
  $Tab->{barnSize} = 12;

  my $size = int($Opt->{StaffSpace} * 2.5);

  $Tab->{symFont} = "{".RESTFONT."} $size normal roman";
  $Tab->{symSize} = $size;
  $size *= $Opt->{EditScale};
  $Tab->{esymFont} = "{".RESTFONT."} $size normal roman";
  $Tab->{esymSize} = $size;
}

sub newFont {
  my($font,$size) = @_;

  if ($size < 1.0) {
    (my $orgs = $font) =~ s/(\{[^}]*\})\s+(\d+)\s+(.*)/$2/;
    $size = int($orgs * $size);
  }
  $font =~ s/(\{[^}]*\})\s+(\d+)\s+(.*)/$1 $size $3/;
  $font;
}

# Work out the X/Y co-ords for each bar.
sub setXY {
  my $off = $Tab->{pOffset};
  my $pidx = 0;
  my $w = $off->{width};
  my $h = $off->{height};
  my $y = $Tab->{barTop};
  foreach my $r (1..$Tab->{rowsPP}) {
    my $x = INDENT;
    foreach my $c (1..$Opt->{Nbar}) {
      $pageXY[$pidx++] = [$x,$y];
      $x += $w;
    }
    $y += $h;
  }
}

# indexBars() sorts out the bar page index's. It does nothing
# to the lyrics, they're left where they were.
sub indexBars {
  my($pnum,$pidx,$row,$col) = (0,0,0,0);
  $Tab->{nPage} = 1;
  $Tab->{pstart} = [];
  my $pcan = $Tab->{pCan};
  #
  # We use 2 bar index numbers:
  #   bidx - the index of the actual bar from the first. Currently ony used
  #          in the EditBar header.
  #   pidx - the 'page' index, ie. where, physically, on the page the bar is.
  #
  my $bidx = 1;
  for(my $bar = $Tab->{bars}; $bar != 0; $bar = $bar->{next}) {
    $bar->{bidx} = $bidx++;
    #
    # If we've just done a load() then the Page Canvas gets deleted
    # and remade so we need to update the bar's canvas pointer.
    #
    $bar->{canvas} = $pcan;
    #
    # Handle the case where we want to start a new line.
    #
    if ($bar->{newpage}) {
      $row = $col = $pidx = 0;
      $pnum++;
      $Tab->{nPage} = $pnum + 1;
    }
    #
    # Handle the case where we want to start a new line.
    #
    elsif ($bar->{newline} && $col) {
      if (++$row == $Tab->{rowsPP}) {
	$row = $pidx = 0;
	$pnum++;
	$Tab->{nPage} = $pnum + 1;
      } else {
	$pidx = $Opt->{Nbar} * $row;
      }
      $col = 0;
    } else {
      $pidx = ($Opt->{Nbar} * $row) + $col;
    }
    $Tab->{pstart}[$pnum] = $bar if ($pidx == 0);
    $bar->{pnum} = $pnum; # first page is 0
    ($bar->{x},$bar->{y}) = @{$pageXY[$pidx]};
    $bar->{pidx} = $pidx;
    if (++$col == $Opt->{Nbar} && $bar->{next} != 0) {
      if (++$row == $Tab->{rowsPP}) {
	$row = $pidx = 0;
	$pnum++;
	$Tab->{nPage} = $pnum + 1;
      }
      $col = 0;
    }
  }
}

sub startEdit {
  if ($Opt->{AutoSave}) {
    $SaveID = Tkx::after(($Opt->{AutoSave} * 60000), \&save);
  }
  $Tab->{edited} = 0;
}

sub load {
  my($fn) = @_;

  $Tab->{bars} = $Tab->{lastBar} = 0;
  open IFH, "<", $fn;
  my $newline = my $newpage = my $barp = my $bar = 0;
  my $rep = 'None';
  my $volta = 'None';
  my $bg = my $hdr = '';
  my $just = 'Left';
  while (<IFH>) {
    (my $line = $_) =~ s/\r|\n//g;
    next if ($line =~ /^\s*\#/ || $line eq "");
    if ($line =~ /^\s*\{(.*)\}\s*$/g) {
      # Handle any directives.
      my ($cmd, $txt) = split(':', $1, 2);
      if ($cmd =~ /^key$/i)               {($Tab->{key} = $txt) =~ s/\s//g;}
      elsif ($cmd =~ /^title$/i)          {$Tab->{title} = $txt;}
      elsif ($cmd =~ /^PDFname$/i)        {
	$Tab->{PDFname} = ($txt eq '') ? $Tab->{title}.'.pdf' : $txt;
      }
      elsif ($cmd =~ /^newline$/i)        {$newline = 1;}
      elsif ($cmd =~ /^newpage$/i)        {$newpage = 1;}
      elsif ($cmd =~ /^instrument$/i)     {$Opt->{Instrument} = ucfirst(lc($txt)); readChords();}
      elsif ($cmd =~ /^bars_per_stave$/i) {$Opt->{Nbar} = $txt + 0;}
      elsif ($cmd =~ /^timing$/i)         {$Opt->{Timing} = $txt}
      elsif ($cmd =~ /^staff_space$/i)    {$Opt->{StaffSpace} = $txt + 0;}
      elsif ($cmd =~ /^stave_gap$/i)      {$Tab->{staveGap} = $txt + 0;}
      elsif ($cmd =~ /^lyric_space$/i)    {$Tab->{lyricSpace} = $txt + 0;}
      elsif ($cmd =~ /^note$/i)           {$Tab->{note} = $txt;}
      elsif ($cmd =~ /^tempo$/i)          {$Tab->{tempo} = $txt + 0;}
      elsif ($cmd =~ /^lyric_lines$/i)    {$Opt->{LyricLines} = $txt + 0;}
      elsif ($cmd =~ /^header$/i)         {($hdr = $txt) =~ s/'.'//;}
      elsif ($cmd =~ /^justify$/i)        {$just = ucfirst(lc($txt));}
      elsif ($cmd =~ /^volta$/i)          {$volta = ucfirst(lc($txt));}
      elsif ($cmd =~ /^repeat$/i)         {$rep = ucfirst(lc($txt));}
      elsif ($cmd =~ /^background$/i) {
	if ($txt =~ /(\d+),(\d+),(\d+)$/) {
	  $bg = sprint("#%02x%02x%02x", $1, $2, $3);
	} elsif ($txt =~ /(#[\da-fA-F]{6})$/) {
	  $bg = $1;
	}
      }
      elsif ($cmd =~ /^lyric$/i) {
	my($stave,$line,$lyr) = split(':', $txt, 3);
	$Tab->{lyrics}[$stave][$line] = $lyr;
      }
    } else {
      while ($line =~ /\[([^\]]*)\]/g) {
	my $notes = $1;
	$bar = CP::Bar->new();
	if ($barp == 0) {
	  $Tab->{bars} = $barp = $bar;
	} else {
	  $barp->{next} = $bar;
	  $bar->{prev} = $barp;
	  $barp = $bar;
	}
	$barp->{newline} = $newline;
	$barp->{newpage} = $newpage;
	$barp->{volta} = $volta;
	$barp->{header} = $hdr;
	$barp->{justify} = $just;
	$barp->{rep} = $rep;
	$barp->{bg} = $bg;
	while ($notes =~ /([r\d])\(([^\)]*)\)/g) {
	  my $n = $2;
	  my $string = $1;
	  $string -= 1 if ($string ne 'r');
	  foreach my $s (split(' ', $n)) {
	    push(@{$barp->{notes}}, CP::Note->new($barp, $string, $s));
	  }
	}
	$newline = $newpage = 0;
	$rep = $volta = 'None';
	$bg = $hdr = '';
	$just = 'Left';
      }
    }
  }
  # Legacy fix :(
  $Opt->{Timing} .= '/4' if (length($Opt->{Timing}) == 1);
  $Tab->{lastBar} = $bar;
  close(IFH);
  guessKey() if ($Tab->{key} eq '');
  $Tab->{loaded} = 1;
}

sub guessKey {
  for(my $bar = $Tab->{bars}; $bar != 0; $bar = $bar->{next}) {
    foreach my $n (sort {$a->{pos} <=> $b->{pos}} @{$bar->{notes}}) {
      if ($n->{string} ne 'r') {
	my $idx = (idx($Tuning[$n->{string}]) + $n->{fret}) % 12;
	my $c = $Scale->[$idx];
	if ($c =~ /[a-g]/) {
	  $c = uc($c);
	  $c .= ($Scale == \@Fscale) ? 'b' : '#';
	}
	$Tab->{key} = $c;
	return;
      }
    }
  }
}

my $Sip = 0;   # Save In Progress

sub saveAs {
  my $fn = Tkx::tk___getSaveFile(
    -initialdir => $Path->{Tab},
    -initialfile => $Tab->{fileName},
    -defaultextension => '.tab',
    -filetypes => [['Tab Files', '.tab'],['All Files', '*',]]
      );
  if ($fn ne '') {
    my($fileName,$path) = fileparse($fn);
    save($fileName, $path);
  }
}

sub save {
  my($fileName,$path,$bu) = @_;

  $path = $Path->{Tab} if (! defined $path);
  $bu = 1 if (!defined $bu);
  if ($Sip == 0 && $fileName ne '') {
    $Sip++;
    getLyrics() if ($Opt->{LyricLines});
    my $tmpTab = "$Path->{Temp}/$fileName";
    if (open OFH, '>', $tmpTab) {
      print OFH '{title:'.$Tab->{title}."}\n";
      print OFH '{PDFname:'.$Tab->{PDFname}."}\n" if ($Tab->{PDFname} ne '');
      print OFH '{instrument:'.$Opt->{Instrument}."}\n";
      print OFH '{key:'.$Tab->{key}."}\n"   if ($Tab->{key} ne '');
      print OFH '{note:'.$Tab->{note}."}\n" if ($Tab->{note} ne '');
      print OFH '{tempo:'.$Tab->{tempo}."}\n";
      print OFH '{bars_per_stave:'.$Opt->{Nbar}."}\n";
      print OFH '{timing:'.$Opt->{Timing}."}\n";
      print OFH '{staff_space:'.$Opt->{StaffSpace}."}\n";
      print OFH '{stave_gap:'.$Tab->{staveGap}."}\n";
      print OFH '{lyric_space:'.$Tab->{lyricSpace}."}\n";
      print OFH '{lyric_lines:'.$Opt->{LyricLines}."}\n";
      for(my $bar = $Tab->{bars}; $bar != 0; $bar = $bar->{next}) {
	print OFH "{newline}\n"                   if ($bar->{newline});
	print OFH "{newpage}\n"                   if ($bar->{newpage});
	print OFH '{header:'.$bar->{header}."}\n" if ($bar->{header} ne '');
	print OFH "{justify:Right}\n"             if ($bar->{justify} eq 'Right');
	print OFH "{volta:".$bar->{volta}."}\n"   if ($bar->{volta} ne 'None');
	print OFH "{repeat:".$bar->{rep}."}\n"    if ($bar->{rep} ne 'None');
	print OFH "{background:".$bar->{bg}."}\n" if ($bar->{bg} ne '');
	print OFH "[";
	foreach my $n (@{$bar->{notes}}) {
	  my $fs = '';
	  if ($n->{string} eq 'r') {
	    $fs = "r($n->{fret},$n->{pos})";
	  } else {
	    my $fnt = ($n->{font} eq 'Small') ? 'f' : '';
	    $fs = ($n->{string}+1).'('.$fnt.$n->{fret};
	    $fs .= ','.$n->{pos};
	    if ($n->{op} ne '') {
	      $fs .= $n->{op};
	      if ($n->{op} =~ /s|h/) {
		$fs .= $n->{tofret}.','.$n->{topos};
	      } elsif ($n->{op} =~ /b|r/) {
		$fs .= $n->{bend};
		if ($n->{op} eq 'r') {
		  $fs .= ','.$n->{release};
		}
	      }
	    }
	  }
	  print OFH "$fs)";
	}
	print OFH "]\n";
      }
      my $stave = 0;
      my $nst = $Tab->{rowsPP} * $Tab->{nPage};
      while ($nst) {
	if (defined $Tab->{lyrics}[$stave]) {
	  foreach my $line (0..($Opt->{LyricLines} - 1)) {
	    if (defined $Tab->{lyrics}[$stave][$line]) {
	      print OFH "{lyric:$stave:$line:".$Tab->{lyrics}[$stave][$line]."}\n";
	    }
	  }
	}
	$stave++;
	$nst--;
      }

      close(OFH);
    } else {
      $Sip = 0;
      message(SAD, "Tab Save could not create temporary file:\n\"$tmpTab\"");
      return(0);
    }
    backupFile($path, $fileName, $tmpTab, 1) if ($bu);
    if ($Opt->{AutoSave}) {
      $SaveID = Tkx::after(($Opt->{AutoSave} * 60000), \&save);
    }
    $Tab->{edited} = $Sip = 0;
    return(1);
  }
  return(0);
}

sub pageHdr {
  pageKey();
  pageNote();
  pageTitle();
  pageNum();
  pageTempo();

  my $ln = $Tab->{pageHeader};
  $Tab->{pCan}->create_line(0, $ln, $Media->{width}, $ln, -fill => DBLUE, -tags => 'phdr');
}

sub clearHdr {
  $Tab->{pCan}->delete(qw/hdrk hdrn hdrt hdrp hdrb/);
}

sub pageKey {
  my $can = $Tab->{pCan};
  $can->delete('hdrk');
  if ($Tab->{key} ne '') {
    my $y = ($Tab->{pageHeader} / 2);
    my $id = $can->create_text(INDENT, $y,
      -text   => "Key:",
      -anchor => 'sw',
      -fill   => bFG,
      -font   => $Tab->{keyFont},
      -tags   => 'hdrk',
	);
    my($x1,$y1,$x2,$h) = split(/ /, $can->bbox($id));
    my $ch = [split('',$Tab->{key})];
    chordAdd($x2+2, $y, $ch, $Tab->{keyFont}, $Tab->{keySize});
  }
}

sub pageNote {
  my $can = $Tab->{pCan};
  $can->delete('hdrn');
  if ($Tab->{note} ne '') {
    $can->create_text(INDENT, $Tab->{pageHeader} - 2,
        -text   => $Tab->{note},
        -anchor => 'sw',
        -fill   => DGREEN,
        -font   => $Tab->{keyFont},
        -tags   => 'hdrn',
	);
  }
}

sub pageTitle {
  my $can = $Tab->{pCan};
  $can->delete('hdrt');
  $can->create_text(
    ($Media->{width} / 2), $Tab->{pageHeader} / 2,
    -text => $Tab->{title},
    -fill => $Tab->{titleColor},
    -font => $Tab->{titleFont},
    -tags => 'hdrt',
      );
}

sub pageNum {
  my $can = $Tab->{pCan};
  $can->delete('hdrp');
  my $id = $can->create_text(
    0, 0,
    -text => "Page ".($Tab->{pageNum}+1)." of ".$Tab->{nPage}." ",
    -fill => BROWN,
    -font => $Tab->{pageFont},
    -tags => 'hdrp',
      );
  my($x1,$y1,$x2,$y2) = split(/ /, $can->bbox($id));
  $can->coords($id, $Media->{width} - $x2, $Tab->{pageHeader} / 2);
}

sub pageTempo {
  if ($Tab->{title} ne '' && defined $Tab->{tempo}) {
    my $can = $Tab->{pCan};
    $can->delete('hdrb');
    my $x = ($Media->{width} / 2) - 12;
    my $y = $Tab->{pageHeader} + 8;
    my $sz = $Tab->{symSize};
    my $nsz = int(($sz * 0.6) + 0.5);
    (my $fnt = $Tab->{symFont}) =~ s/ $sz / $nsz /;
    my $wid = $can->create_text(
      $x, $y + 4,
      -text => 'O',
      -font => $fnt,
      -tags => 'hdrb');
    my($x1,$y1,$x2,$y2) = split(/ /, $can->bbox($wid));
    $can->create_text(
      $x + ($x2 - $x1), $y,
      -text => '= '.$Tab->{tempo},
      -font => $Tab->{pageFont},
      -anchor => 'w',
      -tags => 'hdrb');
  }
}

sub chordAdd {
  my($x,$y,$ch,$fnt,$siz) = @_;

  my $can = $Tab->{pCan};
  my @o = ('-fill', $Tab->{titleColor}, '-anchor', 'sw', '-tags', 'hdrk');
  my $id = $can->create_text($x, $y, -text => shift(@{$ch}), -font => $fnt, @o);
  if (@{$ch}) {
    my($x1,$y1,$x2,$y2) = split(/ /, $can->bbox($id));
    my $sfnt = newFont($fnt, int(($siz * 0.8) + 0.5));
    $can->create_text($x2, $y - 2, -text => join('', @{$ch}), -font => $sfnt, @o);
  }
}

sub pageBars {
  my $x = INDENT;
  my $off = $Tab->{pOffset};
  my $pidx = 0;
  my $h = $off->{height};
  my $y = $Tab->{barTop} + $off->{staffY} + ($off->{staffHeight} / 2);
  my @o = ('-fill', $Tab->{titleColor}, '-font', $Tab->{barnFont}, '-anchor', 'e', '-tags', 'pcnt');
  my $bar = $Tab->{pstart}[$Tab->{pageNum}];
  my $num = 1;
  for(my $b = $Tab->{bars}; $b && $b != $bar; $b = $b->{next}) {
    $num++;
  }
  my $bar0;
  my @lyric;
  my $pcan = $Tab->{pCan};
  my $ncan = $Tab->{nCan};
  $ncan->delete('pcnt');
  foreach my $row (0..($Tab->{rowsPP} - 1)) {
    # Bar numbers down the left side of the page.
    # These are the ACTUAL bar numbers as opposed to the bars per page.
    $ncan->create_text(BNUMW - 4, $y, -text => $num, @o);
    $y += $h;
    foreach my $col (0..($Opt->{Nbar} - 1)) {
      if ($bar && $bar->{pidx} == $pidx) {
	$bar->enable();
	$bar->show($bar);
	$bar = $bar->{next};
	$num++;
      } else {
	$pcan->itemconfigure("b$pidx", -fill => LGREY);
	my $tag = "det$pidx";
	$pcan->bind($tag, '<Button-1>', sub{});
	$pcan->bind($tag, '<Shift-Button-1>', sub{});
	# For Macs
	$pcan->bind($tag, '<Control-Button-1>', sub{});
	$pcan->bind($tag, '<Button-3>', sub{});
      }
      $pidx++;
    }
    last if ($bar == 0 || $bar->{newpage});
  }
}

sub clearLyrics {
  if ($Opt->{LyricLines}) {
    foreach my $row (0..($Tab->{rowsPP} - 1)) {
      if (defined $Tab->{lyricText}[$row]) {
	my $txt = $Tab->{lyricText}[$row];
	$txt->delete('1.0', 'end');
	$txt->edit_reset;
      }
    }
  }
}

#
# These next 2 subs handle placing Lyrics into the Text widgets for displaying
# and retrieving any Lyrics from the Text widgets into the Tab->{lyrics} array.
#
# Display any lyrics on a page.
#
sub showLyrics {
  my $pgidx = $Tab->{pageNum} * $Tab->{rowsPP};
  foreach my $row (0..($Tab->{rowsPP} - 1)) {
    if (defined $Tab->{lyrics}[$pgidx]) {
      my $str = join("\n", @{$Tab->{lyrics}[$pgidx]});
      $Tab->{lyricText}[$row]->insert('1.0', $str) if ($str ne '');
    }
    $pgidx++;
  }
}

#
# Go through a page and collect any Lyrics.
#
sub getLyrics {
  my $pgidx = $Tab->{pageNum} * $Tab->{rowsPP};
  foreach my $row (0..($Tab->{rowsPP} - 1)) {
    if (defined $Tab->{lyricText}[$row]) {
      my $str = $Tab->{lyricText}[$row]->get('1.0', 'end');
      chomp($str);
      @{$Tab->{lyrics}[$pgidx++]} = split("\n", $str);
    }
  }
}

# Moving lyrics up one Stave is essentially a rotate left.
# Whatever moves up (and hence off) the first page is
# appeneded to the end of the Lyric array.
sub lyricUp {
  if ($Opt->{LyricLines}) {
    getLyrics();
    my $ly = shift(@{$Tab->{lyrics}});
    push(@{$Tab->{lyrics}}, $ly);
    clearLyrics();
    showLyrics();
  }
}

sub lyricDown {
  if ($Opt->{LyricLines}) {
    getLyrics();
    unshift(@{$Tab->{lyrics}}, []);
    clearLyrics();
    showLyrics();
  }
}

sub newPage {
  my($pn) = shift;

  getLyrics() if ($Opt->{LyricLines});
  clearTab();
  $Tab->{nPage} = ($pn + 1) if (($pn + 1) > $Tab->{nPage});

  $Tab->{pageNum} = $pn;    # first page is 0
  $Tab->{pstart}[$pn] = 0 if (! defined $Tab->{pstart}[$pn]);
  pageNum();
  pageBars();
  showLyrics() if ($Opt->{LyricLines});
}

#
# Clear out the Tab page but leave all the heading info
#
sub clearTab {
  ClearSel();
  $Tab->{nCan}->delete('pcnt');
  my $can = $Tab->{pCan};
  foreach my $id (0..($Tab->{barsPP}-1)) {
    $can->itemconfigure("bg$id", -fill => BLANK);
    $can->delete("bar$id"); #, "rep$id");
    $can->itemconfigure("b$id", -fill => LGREY);
    my $tag = "det$id";
    $can->bind($tag, '<Button-1>', sub{});
    $can->bind($tag, '<Shift-Button-1>', sub{});
    # For Macs
    $can->bind($tag, '<Control-Button-1>', sub{});
    $can->bind($tag, '<Button-3>', sub{});
  }
  clearLyrics();
}

sub setBG {
  my($a,$b) = diff();
  if ($a == 0) {
    message(QUIZ, "Please select one or more bars first.");
  } else {
    if ((my $bg = $a->bgGet()) ne '') {
      while ($a && $a->{prev} != $b) {
	$a->{bg} = $bg;
	$a->show($a);
	$a = $a->{next};
      }
    }
  }
  ClearSel();
}

sub clearBG {
  my($a,$b) = diff();
  if ($a == 0) {
    message(QUIZ, "Please select one or more bars first.");
  } else {
    while ($a != 0 && $a->{prev} != $b) {
      $a->{bg} = BLANK;
      $a->show($a);
      $a = $a->{next};
    }
  }
  ClearSel();
}

sub editBar {
  my($a,$b) = diff();
  if ($a == 0) {
    message(QUIZ, "Please select a bar to edit.");
  } else {
    if ($a != $b) {
      if (msgYesNo("Only the first Bar will be edited.\nContinue?") eq "No") {
	return;
      }
      my $can = $Tab->{pCan};
      while ($b != $a) {
	$can->itemconfigure("bg$b->{pidx}", -fill => $b->{bg});
	$b = $b->{prev};
      }
      $Tab->{select1} = $a;
      $Tab->{select2} = 0;
    }
    $a->Edit();
  }
}

sub Clone {
  my($a,$b) = diff();
  if ($a == 0) {
    message(QUIZ, "Please select one or more bars first.");
  } else {
    my $bp = $Tab->{lastBar};
    do {
      $bp->{next} = CP::Bar->new($Tab->{pOffset});
      $bp->{next}{prev} = $bp;
      $bp = $bp->{next};
      $a->copy($bp);
      $a = $a->{next};
    } while ($a && $a->{prev} != $b);
    $Tab->{lastBar} = $bp;
    $Tab->{edited}++;
    ClearSel();
    indexBars();
    newPage($Tab->{nPage} - 1);
  }
}

my @Copy = ();
sub Copy {
  my($a,$b) = diff();
  if ($a == 0) {
    message(QUIZ, "Please select one or more bars first.");
  } else {
    @Copy = ();
    do {
      my $copy = CP::Bar->new($Tab->{pOffset});
      $a->copy($copy);
      push(@Copy, $copy);
      $a = $a->{next};
    } while ($a && $a->{prev} != $b);
    ClearSel();
  }
}

sub PasteOver {
  if (@Copy == 0) {
    message(SAD, "No Bars in the Copy buffer.");
  } elsif ($Tab->{select1} == 0) {
    message(QUIZ, "Please select a destination bar.");
  } else {
    if ((my $dst = $Tab->{select1}) != 0) {
      my $lastdst;
      foreach my $src (@Copy) {
	if ($dst == 0) {
	  $dst = CP::Bar->new($Tab->{pOffset});
	  $Tab->{lastBar}{next} = $dst;
	  $dst->{prev} = $Tab->{lastBar};
	  $Tab->{lastBar} = $dst;
	}
	$src->copy($dst);
	$lastdst = $dst;
	$dst = $dst->{next};
      }
      $Tab->{edited}++;
      ClearSel();
      indexBars();
      newPage($lastdst->{pnum});
    }
  }
}

sub PasteBefore {
  if (@Copy == 0) {
    message(SAD, "No Bars in the Copy buffer.");
  } elsif ($Tab->{select1} == 0) {
    message(QUIZ, "Please select a destination bar.");
  } else {
    my $dst = $Tab->{select1}{prev};
    foreach my $src (@Copy) {
      my $bar = CP::Bar->new($Tab->{pOffset});
      $src->copy($bar);
      if ($dst == 0) {
	$Tab->{bars} = $bar;
      } else {
	$dst->{next} = $bar;
	$bar->{prev} = $dst;
      }
      $dst = $bar;
    }
    $dst->{next} = $Tab->{select1};
    $Tab->{select1}{prev} = $dst;
    $Tab->{edited}++;
    ClearSel();
    indexBars();
    newPage($dst->{pnum});
  }
}

sub PasteAfter {
  if (@Copy == 0) {
    message(SAD, "No Bars in the Copy buffer.");
  } elsif ($Tab->{select1} == 0) {
    message(QUIZ, "Please select a destination bar.");
  } else {
    my $dst = $Tab->{select1};
    my $next = $dst->{next};
    foreach my $src (@Copy) {
      my $bar = CP::Bar->new($Tab->{pOffset});
      $src->copy($bar);
      $dst->{next} = $bar;
      $bar->{prev} = $dst;
      $dst = $bar;
    }
    if ($next != 0) {
      $dst->{next} = $next;
      $next->{prev} = $dst;
    } else {
      $Tab->{lastBar} = $dst;
    }
    $Tab->{edited}++;
    ClearSel();
    indexBars();
    newPage($dst->{pnum});
  }
}

sub CleanBar {
  my($a,$b) = diff();
  if ($a == 0) {
    message(QUIZ, "Please select a bar first.");
  } else {
    my $can = $Tab->{pCan};
    do {
      $a->Clear();
      $a->clean();
      $can->itemconfigure("bg$a->{pidx}", -fill => $a->{bg});
      $a = $a->{next};
    } while ($a && $a->{prev} != $b);
    ClearSel();
  }
}

sub barSelect {
  my($bar) = shift;

  my $bg = $bar->{bg};
  my $tag = "bg$bar->{pidx}";
  my $can = $Tab->{pCan};
  $can->g_bind('<KeyRelease>', sub{});
  if ($Tab->{select1} == $bar) {
    $can->itemconfigure($tag, -fill => $bg);
    $Tab->{select1} = 0;
  } else {
    if ($Tab->{select2} != 0) {
      ClearSel();
    }
    if ($Tab->{select1} != 0) {
      $can->itemconfigure("bg$Tab->{select1}{pidx}", -fill => $bg);
    }
    $can->itemconfigure($tag, -fill => SELECT);
    $can->g_bind('<KeyRelease>', [sub{checkDel($_[0])}, Tkx::Ev('%k')]);
    $Tab->{select1} = $bar;
  }
  $Tab->{select2} = 0;
}

sub checkDel {
  my($key) = shift;

  print "key code=".ord($key)."\n";
}

sub rangeSelect {
  my($bar) = shift;

  $Tab->{select2} = $bar;
  my($a,$b) = diff();
  if ($a != 0) {
    my $can = $Tab->{pCan};
    do {
      $can->itemconfigure("bg$a->{pidx}", -fill => SELECT);
      $a = $a->{next};
    } while ($a && $a->{prev} != $b);
  }
}

sub ClearSel {
  my($a,$b) = diff();
  if ($a != 0) {
    my $can = $Tab->{pCan};
    do {
      $can->itemconfigure("bg$a->{pidx}", -fill => $a->{bg});
      $a = $a->{next};
    } while ($a && $a->{prev} != $b);
  }
  $Tab->{select1} = $Tab->{select2} = 0;
}

sub DeleteBars {
  my($a,$b) = diff();
  if ($a == 0) {
    message(QUIZ, "Please select one or more bars first.");
  } else {
    my $msg = "Are you sure you want to\ndelete the selected Bar";
    $msg .= 's' if ($a != $b);
    if (msgYesNo($msg) eq 'Yes') {
      if ($a->{prev} == 0) {
	$Tab->{bars} = $b->{next};
	$b->{next}{prev} = 0 if ($b->{next} != 0);
      } elsif ($b->{next} == 0) {
	$Tab->{lastBar} = $a->{prev};
	$a->{prev}{next} = 0;
      } else {
	$a = $a->{prev};
	$b = $b->{next};
	$a->{next} = $b;
	$b->{prev} = $a;
      }
    }
    indexBars();
    newPage($Tab->{pageNum});
  }
}

sub PrevPage {
  ClearSel();
  if ($Tab->{pageNum} > 0) {
    newPage($Tab->{pageNum} - 1);
  }
}

sub NextPage {
  ClearSel();
  newPage($Tab->{pageNum} + 1);
}

sub drawEditWin {
  main::tabTitle($Tab->{fileName});
  readChords();
  offsets();
  setXY();
  CP::TabWin::editorWindow();
  indexBars();
  pageHdr();
  newPage(0);
}

sub checkSelect {
  my $id = $Tab->{selected};
  if ($id != 0) {
    my $t = $Tab->{eCan}->type($id);
    if ($t eq 'image') {
      my $rst = "r$Fpos{$id}{fret}";
      $Tab->{eCan}->itemconfigure($id, -image => "edit$rst");
    } else {
      $Tab->{eCan}->itemconfigure($id, -fill => $Tab->{noteColor});
    }
    $Tab->{selected} = 0;
    $EditBar->{sid} = $EditBar->{eid} = 0;
  }
}

my $lasttop = '';
sub topVal {
  my($val) = @_;

  my $rx = dx($Tab->{eCan}, $val, $Tab->{eheadFont});
  my $ret = 0;
  if ($rx < ($EditBar->{offset}{width} + 10)) {
    $lasttop = $val;
    $ret = 1;
  } else {
    $val = $lasttop;
  }
  $EditBar->{header} = $val;
  $EditBar->topText();
  return($ret);
}

# select1 & select2 are Bar object refs.
sub diff {
  my $a = $Tab->{select1};
  my $b = $Tab->{select2};
  if ($a == 0) {
    return($b, $b);
  } elsif ($b == 0) {
    return($a, $a);
  }
  my $bpp = $Tab->{barsPP};
  my($apn,$bpn) = (0,0);
  $apn = $a->{pidx} + ($a->{pnum} * $bpp) if ($a != 0);
  $bpn = $b->{pidx} + ($b->{pnum} * $bpp) if ($b != 0);
  return(($apn > $bpn) ? ($b,$a) : ($a,$b));
}

sub dx {
  my($can,$txt,$fnt) = @_;

  my $wid = $can->create_text(
    0, 0,
    -text   => $txt,
    -font   => $fnt,
    -anchor => 'nw',
    -tags   => 'tmp');
  my($lx,$ly,$rx,$ry) = split(/ /, $can->bbox($wid));
  $can->delete('tmp');
  $rx;
}

sub transpose {
  if ($Tab->{trans} != 0) {
    my $bar  = ($Tab->{select1}) ? $Tab->{select1} : $Tab->{bars};
    my $last = ($Tab->{select2}) ? $Tab->{select2} : $Tab->{lastBar};
    my $nstr = $Nstring - 1;
    while ($bar && $bar->{prev} != $last) {
      my $tr = $Tab->{trans} + 0;
      foreach my $n (@{$bar->{notes}}) {
	if ($n->{string} ne 'r') {
	  $n->{fret} += $tr;
	  $n->{tofret} += $tr if ($n->{op} =~ /s|h/);
	  if ($Opt->{Refret}) {
	    foreach my $ups (5, 10) {
	      if ($tr >= $ups && $n->{string} < $nstr) {
		$n->{string} += 1;
		$n->{fret} -= 5;
		$n->{tofret} -= 5;
	      }
	    }
	    my $fretdiff = $n->{tofret} - $n->{fret};
	    while ($n->{fret} < 0) {
	      if ($n->{string} == 0) {
		# Shift up an octave.
		$n->{string} += 1;
		$n->{fret} += 7;
	      } else {
		$n->{string} -= 1;
		$n->{fret} += 5;
	      }
	    }
	    $n->{tofret} = $n->{fret} + $fretdiff if ($n->{op} =~ /s|h/);
	  }
	}
      }
      $bar = $bar->{next};
    }
    setKey($Tab->{trans});
    newPage($Tab->{pageNum});
  }
  $Tab->{trans} = 0;
}

sub ud1string {
  my($adj) = shift;

  my $bar  = ($Tab->{select1}) ? $Tab->{select1} : $Tab->{bars};
  my $last = ($Tab->{select2}) ? $Tab->{select2} : ($Tab->{select1}) ? $Tab->{select1} : $Tab->{lastBar};
  while ($bar && $bar->{prev} != $last) {
    foreach my $n (@{$bar->{notes}}) {
      my $str = $n->{string};
      if ($str ne 'r') {
	if (($adj > 0 && ($str + 1) == $Nstring) || ($adj < 0 && ($str - 1) < 0)) {
	  $n->{fret} += $adj;
	  $n->{tofret} += $adj;
	} else {
	  $n->{string} += ($adj < 0) ? -1 : 1;
	}
	if ($adj > 0 && $n->{fret} < 0 && $str > 0) {
	  $n->{fret} += 5;
	  $n->{string} -= 1;
	}
      }
    }
    $bar = $bar->{next};
  }
  setKey($adj);
  newPage($Tab->{pageNum});
}

sub setKey {
  my($shft) = shift;

  if ($Tab->{key} ne '') {
    my $idx = idx($Tab->{key}) + $shft;
    $idx %= 12;
    my $c = $Scale->[$idx];
    if ($c =~ /[a-g]/) {
      $c = uc($c);
      $c .= ($Scale == \@Fscale) ? 'b' : '#';
    }
    $Tab->{key} = $c;
  } else {
    guessKey();
  }
  pageKey();
}

sub idx {
  my($key) = shift;

  my $i = 0;
  my $k = substr($key, 0, 1);
  while ($i < 12) {
    last if ($k eq $Scale->[$i]);
    $i++;
  }
  if ($key =~ /\#/) {
    $i++;
  }
  elsif ($key =~ /b/) {
    $i--;
  }
  return($i % 12);
}

1;
