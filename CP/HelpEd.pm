package CP::HelpEd;

use CP::Global qw/:OS :OPT :FUNC :XPM/;
use CP::Help;

sub help {
  my($win) = shift;
  if ($win eq '') {
    makeImage("checkbox", \%XPM);
    foreach my $i (qw/bracket bracketsz braceclr bracesz/) {
      my $ht = Tkx::image_height($i) + 8;
      my $wd = Tkx::image_width($i) + 6;
      my $name = "H$i";
      my $subr = 'Tkx::'.$i.'_data';
      no strict 'refs';
      my $data = &$subr(-background => $Opt->{PushBG});
      Tkx::image_create_photo($name, -height => $ht, -width => $wd);
      $subr = 'Tkx::'.$name.'_put';
      &$subr(BLACK, -to => (0,0,$wd,$ht));
      &$subr($Opt->{PushBG}, -to => (1,1,$wd-1,$ht-1));
      &$subr($data, -to => (3,4));
    }
    $win = CP::Help->new("Editor Help");
    $win->add(
[
 "<O TO:H: Cpgedi - A ChordPro Editor > ",
 "This was Gedi (Gregs EDItor) Ver. 1.0, Copyright 1999 Greg London",
 "This program is free software. You can redistribute it and/or modify it under",
 "the same terms as Perl itself. Special Thanks to Nick Ing-Simmons.\n",
 "Modified by Ian Houlding (2015-19) for use with Chordy running under Tkx (Tcl/Tk).",
 "Not much (if any) of the original code survives :-)\n",

 "<O TO:H: Table Of Contents >",
 "<O OV:S:\nOverview>",

 "<O CM:S:\nCommands (top row)>",
 "<O LS:b: Line Spacing >",
 "<X open> <O OP:B:Open>",
 "<X new> <O NW:B:New>",
 "<X close> <O CL:B:Close>",
 "<X save> <O SV:B:Save>",
 "<X saveAs> <O SA:B:Save As>",
 "<X text> <O AA:B:Editor Font>",
 "<X textsize> <O Aa:B:Font Size>",
 "<X textfg> <O Ac:B:Font Colour>",
 "<X textbg> <O Ab:B:Editor Background>",
 "<X exit> <O EX:B:Exit>",
 "<X Find> <O FD:B:Find>",
 "<X FindNext> <O FN:B:Find Next>",
 "<X FindPrev> <O FP:B:Find Prev>",
 "<X checkbox> <O IC:B:Ignore Case>",

 "<O CM:S:\nCommands (bottom row)>",
 "<O TC:b: Text to ChordPro >",
 "<X cut> <X copy> <X paste> <O CT:B:Cut - Copy - Paste>",
 "<X include> <O IN:B:Include>",
 "<X wrap> <O WR:B:Wrap>",
 "<X SelectAll> <O SL:B:Select All>",
 "<X Unselect> <O DA:B:Deselect All>",
 "<X Undo> <O UD:B:Undo>",
 "<X Redo> <O RD:B:Redo>",
 "<X settags> <O ST:B:Reformat Buffer>",
]);
    if ($OS ne 'aqua') {
      $win->add([
	"<X Replace> <O FR:B:Find & Replace>",
	"<X ReplaceAll> <O RA:B:Find & Replace All>",
		]);
    }

    $win->add(
[
 "<O CH:S:\nChords>",
 "<X bracket> <O BR:B:Chord Colour>",
 "<X bracketsz> <O BS:B:Chord Size>",
 "<X bracketoff> <O BO:B:Chord Offset>",

 "<O DT:S:\nDirectives>",
 "<X braceclr> <O DR:B:Directive Colour>",
 "<X bracesz> <O DS:B:Directive Size>",
 "<O TT:B:title>",
 "<O CA:B:capo>",
 "<O KY:B:key>",
 "<O NT:B:note>",
 "<O NP:B:new page>",
 "<O HL:B:horizontal line>",
 "<O CD:B:chord>",
 "<O DF:B:define>",
 "<O FT:B:chord/text font>",
 "<O FS:B:chord/text size>",
 "<O FC:B:chord/text colour>",
 "<O SG:B:Start/End of Grid>",
 "<O SC:B:Start/End of Verse>",
 "<O SC:B:Start/End of Chorus>",
 "<O SC:B:Start/End of Bridge>",
 "<O CS:B:Verse/Chorus/Bridge>",
 "<O HT:B:highlight>",
 "<O CO:B:comment (plus italic & box)>",
 "<O SB:B:start background>",
 "<O EB:B:end background>",
 "<O CC:B:colour selector>",

 "<O GH:S:\nGoto & Help>",
 "<O GT:B:Go To>",
 "<O HP:B:Help> ",

 "<T OV><S Overview>\n",

 "The basic layout of a ChordPro file is lines of lyrics with chords embedded - for example:\n",
 "<C     [D]Amarillo By [F#m]Morning, [G]Up from San An[D]tone.>\n",
 "You'll note that each chord is enclosed in a pair of [ ] brackets. When converted to PDF the chords will appear above the appropriate lyric so the final D will appear above the 't' of Antone. It's more usual to write the lyric as An-[D]tone for readability.\n",
 "<C       D           F><U #m><C       G              D> ",
 "<C       Amarillo By Morning, Up from San An-tone.>\n",
 "The only restriction is that the first character after the opening [ must be an upper case letter between A and G inclusive if it is to be recognised as a legitimate chord. This letter can be optionally followed by a 'b' to indicate a flat or a # to indicate a sharp. <A Anything> else that follows will be printed in the PDF on the same line as the chords. This does mean that you can have a line in the ChordPro file that has NO lyrics and looks like this:\n",
 "<C     [C][      ][D][      ][E][      ][F]>\n",
 "    which will print as:\n",
 "<C     C      D      E      F>\n",
 "It also enables you to put (sort of) playing directives into a line of lyrics, for example:\n",
 "<C     [N/C]Throughout the [(What chord)]days, Our true love [A]ways>\n",
 "will print as:\n",
 "<C     N/C            (What chord)        A> ",
 "<C     Throughout the days, Our true love ways>\n",
 "Where the <C N/C> indicates \"No Chords\" ie Don't play anything. Note also that all of <C (What chord)> gets printed above the lyrics.",
 "Obviously, this only works where the first character is <A not> A to G :-) Even then it's not a problem until you transpose the piece at which point your spelling takes a dive because the first A to G will get shifted to the new key!\n",

 "Chordy also accepts augmented chords like [C/G] - both parts of the chord will be transposed if required. In the final PDF or printout, anything that makes up a legitimate chord definition will have the major base printed in the standard font and anything which modifies the major chord will be printed as a superscript (smaller font and raised above the text base line). If a space is encountered, the font will immediately revert to the standard size:\n",
 "<C     [D#]Some lyrics, [C/G]Lots more [G#m7 softly]lyrics>\n",
 "Displays as:\n",
 "<C     D><U #><C            C/G       G><U #m7><C  softly> ",
 "<C     Some lyrics, Lots more lyrics>\n",
 "The spec. for ChordPro defines quite a number of directives that can be embedded in the ChordPro file. This app only handles a specific subset of the directives but it provides extra functionality over the basic set. Most (but not all) directives have a short and a long form shown as <I {short|long: ...}>. Each directive must appear as the first (and only) text on any given line. You can embed comments into a ChordPro file by placing a '#' as the first character on a line followed by any text. This text will not appear on any output created using the ChordPro file.\n",

 "The basic intention with this Editor was to make creating/editing ChordPro files easier. To this end the window is split into 5 main areas:\n",
 "<I Edit area (bottom right)> ",
 "Fairly obviously where all the text to be edited lives.\n",
 "<I Command Menu (top right)> ",
 "All the commands (Open/Close/Save etc.) are available via buttons and/or text boxes.\n",
 "<I Chords (top left)> ",
 "A series of buttons that let you enter chords (in ChorPro format) into the text area.\n",
 "<I Directives (middle left)> ",
 "Another set of buttons that insert <A Directives> into the text area.\n",
 "<I Counters, Goto & Help (bottom left)> ",
 "An area that shows you which line/column the cursor is at and a count of the number of lines in the total text.\n",

 "<T CM><S Commands (top row)>\n",

 "<T LS><b  Line Spacing > ",
 "<M>Increases or decreases the distance between lines to make it more (or less) readable.\n",

 "<T OP><X open> <I Open> ",
 "<M>Pops up a dialog showing all current ChordPro files.\n",

 "<T NW><X new> <I New> ",
 "<M>Asks for a name for the new file - no extension needed.\n",

 "<T CL><X close> <I Close> ",
 "<M>Closes the current edit session and wipes the text area clean. Prompts to save the file if any changes have been made.\n",

 "<T SV><X save> <I Save> ",
 "<M>Saves the current edit session to disk. If a name has not been previously defined you will prompted for one.\n",

 "<T SA><X saveAs> <I Save As> ",
 "<M>Saves the current edit session to a new file and leaves you editing the new file.\n",

 "<T AA><X text> <I Editor Font> ",
 "<M>Allows you to change the font used in the text area.\n",

 "<T Aa><X textsize> <I Font Size> ",
 "<M>Changes the size of the text area font.\n",

 "<T Ac><X textfg> <I Font Colour> ",
 "<M>Changes the colour of the text area font.\n",

 "<T Ab><X textbg> <I Editor Background> ",
 "<M>Lets you define the background colour of the text area.\n",

 "<T EX><X exit> <I Exit> ",
 "<M>Same as <I Close> and then exits from the editor.\n",

 "<T FD><X Find> <I Find> ",
 "<M>Given that you have entered something into the <A Find:> box, the editor will search forward for the text - and wrap back to the top if necessary.\n",

 "<T FN><X FindNext> <I Find Next> ",
 "<M>Searches forward for the next occurance of the text.\n",

 "<T FP><X FindPrev><I  Find Prev> ",
 "<M>Searches backward for the previous occurance of the text.\n",

 "<T IC><X checkbox><I  Ignore Case> ",
 "<M>If checked, will do a case insensitive search.\n",

 "<T CM><S Commands (bottom row)>\n",

 "<T TC><b  Text to ChordPro > ",
 "<M>This button lets you convert a text file with chords above the lyrics into ChordPro format ie. with the chords surrounded by <B [> <B ]> and in front of the relevant lyric. There are a number of sites on the WEB that provide lyrics in this format and this saves an awful lot of tedious typing!",
 "<M>Simply cut and paste the text into the text area and hit the button.\n",

 "<T CT><X cut> <X copy> <X paste><I  Cut - Copy - Paste> ",
 "<M>Highlight some text and they do exactly what you'd expect.\n",

 "<T IN><X include><I  Include> ",
 "<M>This will pop up a dialog to select another file and it will be inserted into the text area at the current cursor position.\n",

 "<T WR><X wrap><I  Wrap> ",
 "<M>Lets you specify the point at which a line will wrap on the screen - word or character boundary or none.\n",

 "<T SL><X SelectAll><I  Select All> ",
 "<M>Highlights all text as a selection.\n",

 "<T DA><X Unselect><I  Deselect All> ",
 "<M>Removes any current selections.\n",

 "<T UD><X Undo><I  Undo> ",
 "<M>Undoes any typing/changes made to the text area. My well undo more than you expect.\n",

 "<T RD><X Redo><I  Redo> ",
 "<M>Any Undos are reinstated.\n",

 "<T ST><X settags><I  Reformat Buffer> ",
 "<M>Removes all formatting from all the text and then reformats it from start to end.\n",
]);
    if ($OS ne 'aqua') {
      $win->add([
      "<T FR><X Replace><I  Find and Replace> ",
      "<M>Given that there is some text in the <A Find:> box, the editor will search forward for the text and prompt you to replace it with whatever is in the <A Replace with:> box. You can type <A Y> or <A y> to confirm or <A N> or <A n> to skip the replacement.\n",

      "<T RA><X ReplaceAll><I  Replace All> ",
      "<M>Same as above except the editor will search for the next occurance and prompt you to replace the text. Typing anything other than the <A Yes/No> response will abort the Find/Replace.\n"]);
    }
    $win->add(
[
 "<T CH><S Chords>\n",
 "Clicking on any of the buttons will place the appropriate chord into the text area wherever the cursor is. If you hover over a chord you'll get a pop up that lists all the currently known chords with the selected base.\n",
 "<T BR><X bracket><I  Chord Colour> ",
 "<M>Changes the colour of all Chords displayed in the edit area.\n",

 "<T BS><X bracketsz><I  Chord Size> ",
 "<M>Changes the size that Chords are displayed at.\n",

 "<T BO><X bracketoff><I  Chord Offset> ",
 "<M>Because of the way text baselines are handled, Chords can appear below or above the lyric text baseline. This allows you to adjust the Chord baseline\n",

 "<T DV><S Directives>\n",
 "A ChordPro formatted file consists of lyrics and chords but can also include <A directives> that tell a formatting program how to display the information. <R Chordy> has most (but not all) of the ChordPro directives inplemented plus some variations - mainly to control colours.",
 "Each directive <B MUST> be the first and <B ONLY> item on any given line and is a directive (optionally with arguments) enclosed in <B {> <B }> braces.\n",
  "<T BR><X braceclr><I  Directive Colour> ",
  "<M>Changes the colour of all Directives displayed in the edit area.\n",

 "<T BS><X bracesz><I  Directive Size> ",
 "<M>Changes the size that Directives are displayed at.\n",

 "<T TT><I {t|title:text...}> ",
 "<M>Specifies the title of the song. It will appear centered at the top of each page. This overides any title generated by default from the file name.\n",

 "<T CA><I {capo:X}> ",
 "<M>Will appear as <D Capo: X> at the top left of each page.\nIf no {key:X} directive is present, this will have no effect but if there is, then the 'Key' will stay the same but the chords will all be transposed accordingly.\n",

 "<T KY><I {key:xx}> ",
 "<M>Defines the music key. Flats are indicated using lower case B (b) and sharps are the usual #.\n",

 "<T NT><I {note:text...}> ",
 "<M>Should be VERY succinct (2/3 words at most) as this will appear in the heading area on the left of the page. If there is a {capo} directive present it will override the {note} directive which will not be displayed. The alternative is to use something like: {note:Capo 2 Fender Strat}\n",

 "<T NP><I {np|new_page}> ",
       "<I {npp|new_physical_page}> ",
 "<M>Forces a page break. (Chordy will always keep a pair of cord/lyric lines on one page.)\n",

 "<T HL><I {hl|horizontal_line:height length colour}> ",
 "<M>THIS IS NOT A STANDARD ChordPro DIRECTIVE!",
 "<M>This will draw a horizontal line on the page. The default is 1 point (1/72 of an inch) high the width of the page and transparent. The actual values you can place after the ':' are one or more of:",
 "<M>     <D line height>, <D line length> and <D line colour> (each separated by a space).\nAll lengths are in points (72 per inch) and the colour is defined in either of the usual ways.\nIf there is just one number before a colour definition, it is taken as being the line height - the width being the default of the page width.\nAlthough this is designed to draw a line, if you leave out the colour value this directive will insert a vertical space into the page so, for example, {hl:18} will insert a blank line about 1/4\" high into the page.\n",

 "<T CD><I {chord:xx...}> ",
 "<M>Will display a chord fingering layout and within Chordy is independent of the \"Chord Index\" selection.\nYou can specify as many chords as you like within the same directive, just separate each one with a space.\nA number indicating the first fret is displayed alongside the layout - NOTE: the fingerboard nut is NOT considered to be a fret. Above each string, an 'o' indicates an open string and an 'x' indicates an unplayed string.\nIf this directive is immediately followed by another <D {chord}> the next image will be displayed alongside the previous one. Seperating {chord} directives with a blank line will cause the chord after the blank line to be placed at the start of the next line.\n",

 "<T DF>",
 "<I {define:><A name> <A base-fret> <I x> <A frets> <I y y y y y y}> ",
 "<M>This directive defines a new chord fingering or will redefine an existing one. On its own it does nothing to the resulting PDF file unless a \"Chord Index\" is selected within Chordy or a {chord:} directive is used.\n\"name\" is a standard chord ie. Asus4, B(addE), B/F#\nThe words <A base-fret> and <A frets> are key words and should be entered verbatim.\nThe <A frets> entries start at the lowest string (bass E on a 6 string guitar) and can be '-', 'x' or 'X' for 'not played', '0' for an open string or a number (sort of) relative to the <A base-fret>. Not the way I would have designed the numbering but - hey - why have standards if you don't adhere to them!\nHere's a simple example of how it works - suppose you have a cord which only involves the bass E & A strings and is played by having your finger between the nut and the first fret (F) on the E string and between the 2nd and 3rd fret (C) on the A string. The <A base-fret> would be \"1\" and so would the first value in the <A frets> list. The second value would be 3. If you now want to redefine this chord such that the 3rd fret on the E string (G) is used, the <A frets> numbers stay the same but the <A base-fret> now becomes 3. So a <A frets> number of <D 1> is equal to the <A base-fret> when actually played - makes any sense?\nIf you are defining a chord for a 4 string instrument then only insert 4 <A frets> values instead of 6.\n",

 "<T FT><I {chordfont:...}> ",
       "<I {textfont:...}> ",
 "<M>If there is nothing after the <D :> (ie a blank directive) then the font will revert to it's default value.",
 "<M>The 2 <A xxxx><I font> directives are standard in ChordPro but have been enhanced in this version. The normal ChordPro also has {textsize:xx} and {chordsize:xx} directives (see below) but these have been incorporated into these enhanced <A xxxx><I font> directives.\nThe syntax to use is:\n<R   {xxxxfont:FontName Size Weight Slant}>\nWhere:\n<R   FontName> must be enclosed in <R {}> braces if it contains spaces ie:",
 "<R     {xxxxfont:{Times New Roman} 14 bold italic}> ",
 "<R   Size> must be a number between <R 6> and <R 66> (which is HUGE!)",
 "<R   Weight> is either <R bold> or <R normal>.",
 "<R   Slant> is either <R italic> or <R roman>.\n",

 "<M>You can have one or more of the entries but they MUST be in the order shown above - so all the following are valid examples:",
 "<R       {xxxxfont:{Times New Roman}}>     (This has the same effect as the standard ChordPro directive) ",
 "<R       {xxxxfont:{Times New Roman} 14}> ",
 "<R       {xxxxfont:{Times New Roman} 14 bold}> ",
 "\nWhat are not valid (and will be ignored) are entries like this:",
 "<R       {xxxxfont:Times New Roman 14}> - <R Times> will be taken as the font name and <R New> should be the size but isn't a number.",
 "<R       {xxxxfont:{Times New Roman} bold}> - the font size (a number) should come after the font name.\n",

 "<T FS><I {chordsize:...}> ",
       "<I {textsize:...}> ",
 "<M>These change the size of the appropraite font. A blank directive reverts the size (and only the size) to it's default value.\n",

 "<T FC><I {chordcolour:...}> ",
       "<I {textcolour:...}> ",
 "<M>These change the foreground colour of the appropriate font. As above, a blank directive reverts the font colour to it's default value.\n",

 "<T SG><I {start_of_grid:...}> ",
       "<I {end_of_grid}> ",
 "<M>For a full explanation see:",
 "<M>      https://github.com/ChordPro/chordpro/wiki/Directives-env_grid",
 "<M>The <A start> directive can contain one or more of the following:",
 "<R       X>\t\tThe number of cells (measures x beats) on one line (defaults to 4 beats/measure).",
 "<R       MxB>\t\tThe number of Measures and Beats/Measure on one line.",
 "<R       L+(X/MxB)>\t\tThe number of cells in the left margin plus the cell count as above.",
 "<R       L+(X/MxB)+R>\t\tThe number of cells in the left & right margins plus the cell count as above.",
 "<M>Note the use of <A x> and <A +> between components and also that there are <A NO> spaces between any of the numbers.",
 "<M>The left margin of a grid line can contain text which will overide any <R L> component above.",
 "<M>This is also true for the right margin - the margins will be adjusted to accomodate the longest section of text.",
 "<M>You can follow any of the above with a space and then <A ANY> text which will be displayed above the Grid as if it were a <R {comment:...}> directive.",
 "<M>A Grid line may contain any of the following:",
 "<R       |>\t\tBar separator.",
 "<R       ||>\t\tSection separator.",
 "<R       |:>\t\tStart of repeat.",
 "<R       :|>\t\tEnd of repeat.",
 "<R       :|:>\t\tEnd of a repeat and start of another one.",
 "<R       |.>\t\tEnd of Bar line.",
 "<R       .>\t\tCell marker where \"a cell does not need to contain a chord\" (whatever that means!)",
 "<R       />\t\tCell marker (a chord is played)",
 "<R       %>\t\tRepeat the last Measure (no other .",
 "<R       %%>\t\tRepeat the last 2 Measures.",
 "<M>So a Grid definition might look like the following:",
 "<R {start_of_grid 1+4x2+4}> ",
 "<C A    || G7 . | % .  |  %% .  | . .  |> ",
 "<C      | C7 .  | %  . || G7 .  | % .  ||> ",
 "<C      |: C7 . | %  . :|: G7 . | % . :| repeat 4 times> ",
 "<C Coda | D7 .  | Eb7  |   D7   | G7 . | % . |.> ",
 "<R {end_of_grid}> \n",
 
 "<T SC><I {sov|start_of_verse}> ",
       "<I {soc|start_of_chorus}> ",
       "<I {sob|start_of_bridge}> ",
       "<I {eov|end_of_verse}> ",
       "<I {eoc|end_of_chorus}> ",
       "<I {eob|end_of_bridge}> ",
  "<M>These should be placed immediately before and after any lyrics that form a verse, chorus or bridge section. You can then subsequently use just the {verse}, {chorus} or {bridge} directive on its own and the lyrics enclosed by the {soX}/{eoX} pair will be displayed.",
  "<M>The one enhancement to the standard ChordPro directive is that you can follow the directive with a numeric argument to indicate the verse/chorus/bridge number: {sov:1}",
  "<M>This is useful where (in this example) the first verse is later repeated and you can therefore use the directive {verse:1} to repeat this specific verse.\n",

 "<T CS><I {verse}> ",
       "<I {chorus}> ",
       "<I {bridge}> ",
  "<M>Display any lines of the song previously saved by the {soX}/{eoX} directives above. If you specify a colour, only this section will have that background.",
  "<M>As indicated above, a Chordy enhancement to this directive is the ability to specify a specific verse/chorus/bridge section to repeat by having a numeric argument after the directive ie: {chorus:2}.",
  "<M>If you specify a verse/chorus/bridge number (ie {verse:1}) and Chordy detects there are no lines of lyrics defined it will act as if you had just specified {verse}, {chorus} or {bridge}.\n",
 
 "<T HT><I {highlight:text...}> ",
 "<M>Draws <Y  text >  using the Highlight font which by default is Bold+Italic and with a <Y  dayglo yellow > background.\n",

 "<T CO><I {c|comment:text...}> ",
 "<T CO><I {ci|comment_italic:text...}> ",
 "<T CO><I {cb|comment_box:text...}> ",
 "<M>These 3 use the Comment font with the {ci:} using the Italic version (if available). The {cb:} version places an outlined box around the comment. By default, all 3 display with a <L pale blue background>\n",
 "<M>The chorus, highlight and comment directives can all be modified to change their background colour. This is done using an rgb (red,green,blue) format where you specify the ammount of each primary colour as a number between 0 and 255 where 0 is no colour and 255 is lots! So, for example, a pure green would be specified as 0,255,0 - white would be 255,255,255 and black would be 0,0,0. To use this mechanism in a directive just place the comma seperated numbers as the last thing before the closing }. For example: {c:Some text255,0,255} will display <P  Some text > against a dayglo purple background - as if you would!! When this mechanism is used, the colour stays in effect until changed by another directive of the same class ie. chorus, highlight or comment.",
 "<M>An alternative to using the rgb format is to use the hexadecimal format. This takes the form of a '#' character followed by 3 bytes indicating the red, green and blue values. For example #FF00FF is maximum red (FF), no green (00), maximum blue (FF). This is the format returned by the Colour Selector - see below. The purple example above would be written as:",
 "      {c:Some text#FF00FF}\n",

 "<T SB><I {sbg|start_background:colour}> ",
 "<T EB><I {ebg|end_background}> ",
 "<M>ANOTHER NON-STANDARD ChordPro DIRECTIVE!",
 "<M>Placed before and after one or more lines of lyrics/chords will cause them to be displayed with the background colour defined in {sbg}. The colour can be defined either as an RGB value (see above) or with a leading '#' as a 3 byte hexadecimal number. The editor has a colour chooser that allows you to select a colour and supplies the correct string for you. The background colour is canceled by anything other than a line of lyrics/chords.\nBe aware that a line with just a single space character will NOT cancel the background colour and can be used to insert a (seemingly) blank line into a coloured section.\n",

 "<T CC><I Colour Selector> ",
 "<M>In the colour editor you get 3 sliders that go from 0 to 255 for each primary colour where 0 is no colour and 255 is lots. The resulting mix is shown in a box on the right of the window. This box can have both the background and foreground colours changed but what will be used depends on what mode the editor is in (see the heading top right).",
 "<M>Below the sliders is an entry area where you can modify the Hex values for a given colour. If a colour matches one of those listed on the left, that name will appear in this entry box.\n",
 "<M>Below this are three buttons which give you quick access to the current Verse, Chorus, Bridge, Highlight and Comment colours.",
 "<M>The \"My Colours\" box gives you the ability to mix and save 16 different colours you might want to use on a regular basis. Clicking on one of the 16 buttons will set that fore/back-ground colour. If you then change the colour with the sliders, you can change the selected colour swatch with the <R Set Colour> button. These colours are only saved if you hit the <R OK> button.\n",
 "<M>Beside the <I Colour Selector> button are 5 quick entry buttons for the current <R Highlight>, <R Comment>, <R Verse>, <R Chorus> and <R Bridge> colours.\n",

 "<T GH><S Goto & Help>\n",
 "<T GT><b  Go To > ",
 "<M>Enter a line number into the text box and hit the button. The insert cursor will be placed at the start of that line.\n",
 "<T HP><b  Help > ",
 "<M>I know you'll find it hard to believe, but that's how you got here!",
]);
  }
  $win->show();
  $win;
}

1;
