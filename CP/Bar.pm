package CP::Bar;

# This file is part of Chordy.
#
###############################################################################
# Copyright (c) 2018 Ian Houlding
# All rights reserved.
# This program is free software.
# You can redistribute it and/or modify it under the same terms as Perl itself.
###############################################################################

# These are fixed distances and are common to all Bars:
#   header        space above the top Staff line
#   height        total y distance allocated to a Bar
#   interval      distance from one Note to the next
#   scale         1 for the page view, 3 - 5 for edit view
#   staffHeight   distance between the top and bottom Staff lines
#   staffSpace    distance between each Staff Line
#   width         total x distance allocated to a Bar
#
# These are absolute screen positions on the overall Canvas
#   x             left position of the Bar rectangle
#   y             top position of the Bar rectangle
#   staffX        same as x screen position
#   staffY        position of the top Staff line
#   staff0        position of the bottom Staff line (staffY + staffHeight)
#   pos0          x position of the first Note (x + (interval * 2))

# tab     back pointer to the containing Tab
# pidx    page index for the Bar
# bg      Bar background colour
# volta   type of header bar - left, right or just horizontal bar
# header  Text to show above the bar
# justify where to position the header text
# rep     repeat indicator - Left, Right or None
# lyric   array of lyrics to display below the Bar
# sid     used for Slide/Hammer
# eid      ""
# notes   array of Notes/Rests to be displayed in this Bar

use strict;
use warnings;

use Tkx;
use CP::Cconst qw/:TEXT :SMILIE :COLOUR :TAB/;
use CP::Global qw/:OPT :WIN :CHORD :TAB/;
use CP::Cmsg;
use CP::FgBgEd;
use CP::Tab;
use CP::Note;

sub new {
  my($proto,$offp,$can) = @_;

  my $class = ref($proto) || $proto;
  my $self = {};

  $self->{prev} = 0;
  $self->{next} = 0;
  $self->{canvas} = $Tab->{pCan};    # This is the majority - only changed once for the EditBar.
  $self->{offset} = $offp if (defined $offp);
  $self->{pnum} = 0;
  $self->{pidx} = 0;
  $self->{sid}  = 0;
  $self->{eid}  = 0;
  $self->{rep}  = 'None';
  $self->{bg}   = BLANK;
  $self->{header}  = '';
  $self->{justify} = 'Left';
  $self->{volta}   = 'None';
  $self->{newline} = 0;
  $self->{newpage} = 0;
  $self->{notes} = [];

  bless $self, $class;
  return($self);
}

sub printbar {
  my($self) = shift;

  printf "%3d x=%3d  y=%3d\n", $self->{pidx}, $self->{x}, $self->{y};
}

# This sub can be called in 2 ways:
# 1) Where we pass in a bar object that we want to display.
#    In this case we only pass in the bar object.
#    Currently ONLY for the EditBar.
# 2) Where we don't have a bar object but still want the bar outline.
#    In this case we pass in the tab object, page index and x/y values.
#    Currently ONLY for Page bar outlines.
#
sub outline {
  my($self) = shift;

  my($pidx,$X,$Y,$off,$tt,$thick,$thin,$can);
  if (ref($self) eq 'CP::Bar') {
    ($pidx,$X,$Y,$off) = ($self->{pidx},$self->{x},$self->{y},$self->{offset});
    $can = $self->{canvas};
  } else {
    $pidx = shift;
    $X = shift;
    $Y = shift;
    $off = $self->{pOffset}; # $self is actually $Tab.
    $can = $self->{pCan};
  }
  my $w = $off->{width};
  # Background rectangle for bg colour.
  if (($pidx % $Opt->{Nbar}) == 0) {
    $can->create_rectangle(
      $X - 2, $Y, $X + $w + 2, $Y + $off->{height} - $off->{lyricHeight},
      -width => 0, -tags => "bg$pidx");
  } elsif (($pidx % $Opt->{Nbar}) == ($Opt->{Nbar} - 1)) {
      $can->create_rectangle(
	$X, $Y, $X + $w + 2, $Y + $off->{height} - $off->{lyricHeight},
	-width => 0, -tags => "bg$pidx");
  } else {
    $can->create_rectangle(
      $X, $Y, $X + $w, $Y + $off->{height} - $off->{lyricHeight},
      -width => 0, -tags => "bg$pidx");
  }
  ($thick,$thin) = ($off->{thick},$off->{thin});
  my $fill = ($pidx < 0) ? BLACK : LGREY;
  my $tag = "b$pidx";
  # Staff Lines
  my $ss = $off->{staffSpace};
  my $ly = $Y + $off->{staffY};
  foreach (1..$Nstring) {
    $can->create_line(
      $X, $ly, $X + $w, $ly,
      -width => $thin, -fill => $fill, -tags => $tag);
    $ly += $ss;
  }
  # Bar Lines
  my $lx = $X + $off->{pos0};
  $ly = $Y + $off->{staffY};
  my $y2 = $Y + $off->{staff0};
  my $un = $off->{interval} * 8;
  (my $t = $Opt->{Timing}) =~ s/(\d).*/$1/;
  foreach (1..$t) {
    $can->create_line(
      $lx, $ly, $lx, $y2,
      -width => $thin, -fill => $fill, -tags => $tag);
    $lx += $un;
  }

  $lx = $X; # + ($thick / 2);
  # First thick vertical line at beginning of a row
  if (($pidx % $Opt->{Nbar}) == 0 || $pidx == -1) {
    $can->create_line(
      $lx, $ly, $lx, $y2,
      -width => $thick, -fill => $fill, -tags => $tag);
  }
  $lx += $w; # ($w - $thick);
  # Thick line at the end of a bar
  $can->create_line(
    $lx, $ly, $lx, $y2,
    -width => $thick, -fill => $fill, -tags => $tag);

  if ($pidx < 0) {
    markers($self, $tag);
  } else {
    # Detection rectangle to edit a Bar
    $can->create_rectangle(
      $X, $Y, $X + $w, $Y + $off->{lyricY},
      -width => 0,
      -fill => BLANK,
      -tags => "det$pidx", );
  }
}

#
# This should ONLY be called on the Edit Bar.
#
sub markers {
  my($self,$tag) = @_;

  #
  # Make tick marks at every (visible) demi-semi-quaver
  # position and detection rectangles at each position.
  #
  my($X,$Y,$off) = ($self->{x},$self->{y},$self->{offset});
  my $ns = $Nstring - 1;
  my $y = $Y + $off->{staff0};
  my $u = $off->{interval};
  my $hu = $u / 2;
  my $ss = $off->{staffSpace};
  my $hss = $ss / 2;
  (my $t = $Opt->{Timing}) =~ s/(\d).*/$1/;
  my $hsq = ($t * 8) - 1;
  my $can = $self->{canvas};
  foreach my $str (0..$ns) {
    my $len = ($str == $ns) ? +5 : -5;
    my $x = $X + $off->{pos0};
    my $y1 = $y - $hss;
    my $y2 = $y + $hss;
    foreach my $pos (0..$hsq) {
      if (($pos % 8) != 0) {
	my $l = (($pos % 2) == 0) ? $len * 2 : $len;
	$can->create_line($x,$y, $x,$y+$l, -width => 1, -fill => BLACK);
      }
      # Create and bind the detection rectangles
      my $a = $can->create_rectangle(
	$x-$hu, $y1, $x+$hu, $y2,
	-width => 0); #, -tags => "r$str"."c$pos");
      $can->bind($a, "<Button-1>", sub{note($self, $str, $pos)});
      $x += $u;
    }
    $y -= $ss;
  }
  # Make 2 further detection rectangles, one around each end of the bar.
  # These will be used for Slides/Hammers that cross bar boundaries. 
  my $x = $self->{x};
  $y = $Y + $off->{staffY};
  foreach my $tg (qw/shs she/) {
    my $a = $can->create_rectangle(
      $x-$u, $y, $x+$u, $y+$off->{staffHeight},
      -width => 0, -tags => $tag);
    $can->bind($a, "<Button-1>", sub{endSHBR($self, $tg)});
    $x += $off->{width};
  }
}

# This is ONLY called for the EditBar.
sub Background {
  my($self) = @_;

  if ((my $bg = bgGet($self)) ne '') {
    $self->{bg} = $bg;
    $self->{canvas}->itemconfigure("bg-1", -fill => $bg);
  }
}

sub bgGet {
  my($self) = shift;

  $self->{bg} = WHITE if ($self->{bg} eq '');
  $ColourEd = CP::FgBgEd->new() if (! defined $ColourEd);
  $ColourEd->title("Background Colour");
  my($fg,$bg) = $ColourEd->Show($Tab->{noteColor}, $self->{bg}, BACKGRND);
  $bg;
}

# Never called on the EditBar
sub enable {
  my($self) = shift;

  my $pidx = $self->{pidx};
  my $can = $Tab->{pCan};
  $can->itemconfigure("b$pidx", -fill => BLACK);
  my $tag = "det$pidx";
  $can->bind($tag, '<Button-1>', sub{CP::Tab::barSelect($self)});
  $can->bind($tag, '<Shift-Button-1>', sub{CP::Tab::rangeSelect($self)});
  $can->bind($tag, '<Button-3>', sub{CP::Tab::barSelect($self);Edit($self);});
  # For Macs
  $can->bind($tag, '<Control-Button-1>', sub{CP::Tab::barSelect($self);Edit($self);});
}

#
# Come here when one of the Edit Bar detection rectangles is clicked.
#
sub note {
  my($self,$string,$pos) = @_;

  my $id = $Tab->{selected};
  my $can = $self->{canvas};
  if ($id != 0) {
     if ($Fpos{$id}{string} eq 'r') {
      $can->itemconfigure($id, -fill => BLACK);
    } else {  # a note, not a rest.
      $can->itemconfigure($id, -fill => $Tab->{noteColor});
      # {fret} stays the same ...
      $Fpos{$id}{string} = $string;
    }
    my $oldid = $id;
    my $n = $Fpos{$id};
    $n->clear();             # remove the fret # from the display.
    if ($n->{op} =~ /s|h/) {
      my $diff = $n->{pos} - $pos;
      $n->{topos} -= $diff;
    }
    $n->{pos} = $pos;
    $id = $n->show('fret');
    $Fpos{$id} = $n;
    delete($Fpos{$oldid});
    $Tab->{selected} = 0;
  } elsif ($Tab->{fret} ne '') {
    my $n = CP::Note->new($CP::Tab::EditBar, 1, '');
    if ($Tab->{fret} =~ /r(\d+)/) {
      $n->{string} = 'r';
      $n->{fret} = $1;
      foreach my $k (keys %Fpos) {
	if ($Fpos{$k}{pos} == $pos) {
	  $can->delete($k);
	  delete $Fpos{$k};
	}
      }
      $n->{pos} = $pos;
    } else {
      $n->{string} = $string;
      $n->{fret} = $Tab->{fret};
      $n->{pos} = $pos;
      $n->{font} = $Tab->{noteFsize};
    }
    $id = $n->show('fret');
    $Fpos{$id} = $n;
  }
}

sub note_sel {
  my($self,$id) = @_;

  if ($Tab->{sh} ne '') {
    if ($Tab->{sh} =~ /s|h/) {
      slideHammer($self, $id);
    } elsif ($Tab->{sh} =~ /b|r/) {
      bendRelease($self, $id);
    }
  } else {
    my $selid = $Tab->{selected};
    my $can = $self->{canvas};
    if ($selid != 0) {
      my $clr = ($Fpos{$selid}{string} eq 'r') ? BLACK : $Tab->{noteColor};
      $can->itemconfigure($selid, -fill => $clr);
      $Tab->{selected} = 0;
    } else {
      $can->itemconfigure($id, -fill => RED);
      $Tab->{selected} = $id;
    }
  }
}

sub endSHBR {
  my($self,$end) = @_;  # $end is 'shs' or 'she'

  if ($Tab->{sh} =~ /s|h|r/ && $self->{sid} != 0) {
    my $id = $self->{sid};
    if ($Tab->{sh} eq 'r') {
      if ($end eq 'shs') {
	$Fpos{$id}{release} = $Fpos{$id}{pos};
	$Fpos{$id}{pos} = BARZERO;
      } else {
	$Fpos{$id}{release} = BAREND;
      }
      showBR($self, $id, $id);
    } else {
      if ($end eq 'shs') {
	$Fpos{$id}{topos} = BARZERO;
      } else {
	$Fpos{$id}{topos} = BAREND;
      }
      $Fpos{$id}{tofret} = $Fpos{$id}{fret};
      showSH($self, $id, $id);
    }
  }
}

sub note_del {
  my($self,$id) = @_;

  $Fpos{$id}->clear();
  delete $Fpos{$id};
  $Tab->{selected} = 0;
}

sub slideHammer {
  my($self,$id) = @_;

  my($id1,$id2) = (0,0);
  if ($self->{sid} == 0) {
    $self->{sid} = $id;
    $self->{canvas}->itemconfigure($id, -fill => $Tab->{headColor});
  } elsif ($self->{sid} == $id) {
    clearSH($self);
  } else {
    $id1 = $self->{sid};
    $id2 = $id;
  }
  if ($id2 != 0) {
    if ($Fpos{$id1}{string} != $Fpos{$id2}{string}) {
      message(SAD, "Fret positions MUST be on the same string!");
      clearSH($self);
    } else {
      showSH($self, $id1, $id2);
    }
  }
}

sub showSH {
  my($self,$id1,$id2) = @_;

  my $ns = ($Fpos{$id1}{pos} < $Fpos{$id2}{pos}) ? $Fpos{$id1} : $Fpos{$id2};
  my $ne = ($Fpos{$id2}{pos} > $Fpos{$id1}{pos}) ? $Fpos{$id2} : $Fpos{$id1};
  $ns->{op} = $Tab->{sh};
  if ($id1 ne $id2) {
    $ns->{tofret} = $ne->{fret};
    $ns->{topos} = $ne->{pos};
  }
  note_del($self, $id1);
  note_del($self, $id2) if ($id2 ne $id1);
  my $id = $ns->show('fret');
  $Fpos{$id} = $ns;
  clearSH($self);
}

sub bendRelease {
  my($self,$id) = @_;

  my($id1,$id2) = (0,0);
  if ($self->{sid} == 0) {
    $self->{sid} = $id1 = $id;
    $self->{canvas}->itemconfigure($id, -fill => $Tab->{headColor});
  } elsif ($self->{sid} == $id || $Tab->{sh} eq 'b') {
    clearSH($self);
  } else {
    $id1 = $self->{sid};
    $id2 = $id;
  }
  if ($id1 != 0 && $Tab->{sh} eq 'b') {
    my $ns = $Fpos{$id1};
    $ns->{op} = $Tab->{sh};
    $ns->{bend} = 1;               ### THIS NEEDS TO BE SET IN NOTE.PM!!
    note_del($self, $id1);
    my $id = $ns->show('fret');
    $Fpos{$id} = $ns;
    clearSH($self);
  } elsif ($id2 != 0 && $Tab->{sh} eq 'r') {
    if ($Fpos{$id1}{string} != $Fpos{$id2}{string}) {
      message(SAD, "Fret positions MUST be on the same string!");
      clearSH($self);
    } else {
      showBR($self, $id1, $id2);
    }
  }
}

sub showBR {
  my($self,$id1,$id2) = @_;

  my $ns = ($Fpos{$id1}{pos} < $Fpos{$id2}{pos}) ? $Fpos{$id1} : $Fpos{$id2};
  my $ne = ($Fpos{$id2}{pos} > $Fpos{$id1}{pos}) ? $Fpos{$id2} : $Fpos{$id1};
  $ns->{op} = $Tab->{sh};
  $ns->{bend} = 1;               ### THIS NEEDS TO BE SET IN NOTE.PM!!
  $ns->{release} = $ne->{pos};
  note_del($self, $id1);
  note_del($self, $id2) if ($id2 ne $id1);
  my $id = $ns->show('fret');
  $Fpos{$id} = $ns;
  clearSH($self);
}

sub clearSH {
  my($self) = @_;

  my $can = $self->{canvas};
  $can->itemconfigure($self->{sid}, -fill => $Tab->{noteColor});
  $can->itemconfigure($self->{eid}, -fill => $Tab->{noteColor});
  $self->{sid} = $self->{eid} = 0;
  my $selid = $Tab->{selected};
  if ($selid != 0) {
    $can->itemconfigure($selid, -fill => $Tab->{noteColor});
    $Tab->{selected} = 0;
  }
  $Tab->{sh} = '';
}

sub Edit {
  my($self) = shift;

  Clear($EditBar);
  show($self,$EditBar);
}

sub InsertBefore {
  save_bar(BEFORE);
}

sub InsertAfter {
  save_bar(AFTER);
}

sub Save {
  save_bar(REPLACE);
}

sub Update {
  save_bar(UPDATE);
}

sub save_bar {
  my($insert) = shift;

  return if ($insert == UPDATE && $Tab->{select1} == 0);
  my($bar);
  if ($Tab->{select1} == 0) {
    $bar = CP::Bar->new($Tab->{pOffset});
    # special case for the very first Bar
    if ($Tab->{bars} == 0) {
      $Tab->{bars} = $Tab->{lastBar} = $bar;
    } else {
      $Tab->{lastBar}{next} = $bar;
      $bar->{prev} = $Tab->{lastBar};
    }
    $insert = REPLACE;
  } else {
    if ($insert != REPLACE && $insert != UPDATE) {
      my $bp = $Tab->{select1};
      $bar = CP::Bar->new($Tab->{pOffset});
      if ($insert == AFTER) {
	$bar->{prev} = $bp;
	$bar->{next} = $bp->{next};
	$bp->{next}{prev} = $bar;
	$bp->{next} = $bar;
      } else {
	if ($bp->{prev} == 0) {
	  $bar->{next} = $bp;
	  $bp->{prev} = $bar;
	  $Tab->{bars} = $bar;
	} else {
	  $bar->{prev} = $bp->{prev};
	  $bar->{next} = $bp;
	  $bp->{prev}{next} = $bar;
	  $bp->{prev} = $bar;
	}
      }
    } else {
      $bar = $Tab->{select1};
    }
  }
  $Tab->{lastBar} = $bar if ($bar->{next} == 0);
  foreach my $v (qw/newline newpage volta header justify rep bg/) {
    $bar->{$v} = $EditBar->{$v};
  }
  #
  # Each Fpos key is the Canvas ID
  # of the displayed fret number OR rest.
  #
  $bar->{notes} = [];
  foreach my $k (keys %Fpos) {
    my $n = CP::Note->new($bar, $Fpos{$k}{string}, '');
    foreach my $fk (keys %{$Fpos{$k}}) {
      $n->{$fk} = $Fpos{$k}{$fk};
    }
    $n->{bar} = $bar; # 'cos it just got splatted!
    push(@{$bar->{notes}}, $n);
  }
  if ($insert == UPDATE) {
    $bar->Clear();
    $bar->show($bar);
  } else {
    Clear($EditBar);
    ClearEditBG();
    clean($EditBar);
    CP::Tab::indexBars();
    CP::Tab::newPage($Tab->{pageNum});
  }
  $Tab->{edited}++;
}

sub Clear {
  my($self) = shift;

  my $can = $self->{canvas};
  if ($self->{pidx} < 0) {
    $can->delete('barn');
    $self->{topEnt}->delete(0, 'end');

    $self->{rep} = 'None';
    $self->{sid} = $self->{eid} = 0;
    repeat($self);

    foreach my $t (qw/topb toph topr/) {
      $can->delete($t);
    }
    foreach my $k (keys %Fpos) {
      if ($Fpos{$k}{op} ne '') {
	$can->delete("$Fpos{$k}{op}$k");
      }
      $can->delete("e$k");
    }
    %Fpos = ();
    # We don't clear the background on the EditBar.
    # We do that seperately with a call to Clear_EditBG().
  } else {
    my $pidx = $self->{pidx};
    $can->itemconfigure("bg$pidx", -fill => BLANK);
    $can->delete("bar$pidx"); #, "rep$pidx");
  }
}

# ONLY called on the EditBar
sub ClearEditBG {
  $EditBar->{canvas}->itemconfigure("bg-1", -fill => BLANK);
  $EditBar->{bg} = BLANK;
}

# Copy the displayable components from one bar to another.
sub copy {
  my($self,$dst) = @_;

  foreach (qw/bg header justify lyric newline newpage rep volta/) {
    $dst->{$_} = $self->{$_};
  }
  $dst->{notes} = [];
  foreach my $n (@{$self->{notes}}) {
    my $nn = CP::Note->new($dst, $n->{string}, '');
    foreach my $k (keys %{$n}) {
      $nn->{$k} = $n->{$k};
    }
    $nn->{bar} = $dst;
    push(@{$dst->{notes}}, $nn);
  }
}

sub clean {
  my($self) = shift;

  $self->{notes} = [];
  $self->{header} = '';
  $self->{rep} = $self->{volta} = 'None';
  $self->{justify} = 'Left';
  $self->{newline} = $self->{newpage} = 0;
  $self->{bg} = BLANK;
}

sub Cancel {
  my($self) = shift;

  Clear($self);
  clean($self);
  CP::Tab::ClearSel();
}

sub show {
  my($self,$dest) = @_;

  my $destidx = $dest->{pidx};
  my $can = $dest->{canvas};
  $dest->{bg} = $self->{bg};
  $can->itemconfigure("bg$destidx", -fill => $dest->{bg});
#  if ($dest->{bg} ne '' && ($self->{next} == 0 || $self->{next}{newline} || $self->{next}{newpage})) #{
#    for(my $idx = $destidx + 1; $idx % $Opt->{Nbar}; $idx++) {
#      $can->itemconfigure("bg$idx", -fill => $dest->{bg});
#    }
#  }
  if ($destidx < 0) {
    # Edit Bar.
    copy($self, $dest);
  }
  topBar($dest);
  repeat($dest);
  if ($destidx < 0) {
    # Edit Bar.
    $can->delete('barn');
    $can->create_text(
      $Tab->{editX} + 10, $Tab->{editY} + 1,
      -text => "Bar #: $self->{bidx}",
      -font => $Tab->{barnFont},
      -anchor => 'sw',
      -tags => 'barn');
    %Fpos = ();
    foreach my $n (@{$dest->{notes}}) {
      my $id = $n->show('fret');
      $Fpos{$id} = $n;
    }
    if ($dest->{header} ne '') {
      entryUpdate($dest->{topEnt}, $dest->{header});
    }
  } else {
    # View Page.
    topText($dest);
    foreach my $n (@{$dest->{notes}}) {
      $n->show("bar$destidx");
      $can->raise("det$destidx", "bar$destidx");
    }
  }
}

sub entryUpdate {
  my($ent,$txt) = @_;

  $ent->delete(0, 'end');
  if ($txt ne '') {
    # Side effect is the text is displayed in the bar
    # because of the "validate" routine but we have to
    # do it one character at a time.
    foreach (split('', $txt)) {
      $ent->insert('end', "$_");
    }
  }
}

sub repeat {
  my($self) = shift;

  my $off = $self->{offset};
  my $scale = $off->{scale};
  my $tag = ($scale == 1) ? "bar$self->{pidx}" : 'topr';
  $self->{canvas}->delete($tag) if ($scale > 1);
  repStart($self, $off, $scale, $tag) if ($self->{rep} eq 'Start');
  repEnd($self, $off, $scale, $tag) if ($self->{rep} eq 'End');
}

sub repStart {
  my($self,$off,$scale,$tag) = @_;

  my($X,$Y) = ($self->{x},$self->{y});
  my($thick,$thin) = ($off->{thick},$off->{thin});
  my $x = $X + $off->{staffX};
  my $y = $Y + $off->{staffY};
  my $y2 = $Y + $off->{staff0};
  my $clr = $Tab->{headColor};
  my $can = $self->{canvas};

  $can->create_line(
    $x, $y, $x, $y2,
    -width => $thick, -fill => $clr, -tags => $tag);

  $x += $thick;
  $can->create_line(
    $x, $y, $x, $y2,
    -width => $thin,  -fill => $clr, -tags => $tag);
  
  my $dy = $off->{staffSpace} + $thick;
  $dy += $off->{staffSpace} if ($Nstring > 4);
  my $dia = ($scale == 1) ? 1 : $thick + $thin;
  $x += $thin;
  $y += $dy;
  $can->create_oval(
    $x, $y, $x+$dia, $y+$dia,
    -width => 0, -fill => $clr, -tags => $tag);

  $y = $Y + $off->{staff0} - $dy - $dia;
  $can->create_oval(
    $x, $y, $x+$dia, $y+$dia,
    -width => 0, -fill => $clr, -tags => $tag);
}

sub repEnd {
  my($self,$off,$scale,$tag) = @_;

  my($X,$Y) = ($self->{x},$self->{y});
  my($thick,$thin) = ($off->{thick},$off->{thin});
  my $x = $X + $off->{staffX} + $off->{width};
  my $y = $Y + $off->{staffY};
  my $y2 = $Y + $off->{staff0};
  my $idx = $self->{pidx};
  my $clr = $Tab->{headColor};
  my $can = $self->{canvas};
  $can->create_line(
    $x, $y, $x, $y2,
    -width => $thick, -fill => $clr, -tags => $tag);

  $x -= ($thick + $thin);
  $can->create_line(
    $x, $y, $x, $y2,
    -width => $thin,  -fill => $clr, -tags => $tag);
  
  my $dy = $off->{staffSpace} + $thick;
  $dy += $off->{staffSpace} if ($Nstring > 4);
  my $dia = ($scale == 1) ? 1 : $thick + $thin;
  $x -= ($thin + 1);
  $y += $dy;
  $can->create_oval(
    $x, $y, $x-$dia, $y+$dia,
    -width => 0, -fill => $clr, -tags => $tag);

  $y = $Y + $off->{staff0} - $dy - $dia;
  $can->create_oval(
    $x, $y, $x-$dia, $y+$dia,
    -width => 0, -fill => $clr, -tags => $tag);
}

sub topText {
  my($self) = shift;

  my $off = $self->{offset};
  my $scale = $off->{scale};
  my $tag = ($scale == 1) ? "bar$self->{pidx}" : 'toph';
  my $can = $self->{canvas};
  $can->delete($tag) if ($scale > 1);
  if ($self->{header} ne '') {
    my $fnt = ($scale == 1) ? $Tab->{headFont} : $Tab->{eheadFont};
    my $x = $self->{x};
    my $wid = $can->create_text(
      0,0,
      -text => $self->{header},
      -anchor => 'sw',
      -fill => $Tab->{headColor},
      -font => $fnt,
      -tags => $tag);
    if ($self->{justify} eq 'Right') {
      $x += $off->{width};
      my($lx,$ly,$rx,$ry) = split(/ /, $can->bbox($wid));
      $x -= ($rx + $off->{thick});
    } else {
      $x += $off->{thick};
    }
    my $fh = ($scale == 1) ? $Tab->{headSize} : $Tab->{eheadSize};
    $can->coords($wid, $x, $self->{y} + $off->{headY});
  }
}

sub topBar {
  my($self) = shift;

  my $off = $self->{offset};
  my $scale = $off->{scale};
  my $tag = ($scale == 1) ? "bar$self->{pidx}" : 'topb';
  my $can = $self->{canvas};
  $can->delete($tag) if ($scale > 1);
  if ($self->{volta} ne 'None') {
    my $w = $off->{thick};
    my $x = $self->{x} + $off->{staffX};
    my $y = $self->{y} + ($w / 2);
    $can->create_line(
      $x, $y, $x + $off->{width}, $y,
      -width => $w, -fill => $Tab->{headColor}, -tags => $tag);
    if ($self->{volta} ne 'Center') {
      $x += ($w / 2);
      if ($self->{volta} =~ /Left|Both/) {
	$can->create_line(
	  $x, $y, $x, $self->{y} + $off->{staffY} - ($scale * 3),
	  -width => $w, -fill => $Tab->{headColor}, -tags => $tag);
      }
      if ($self->{volta} =~ /Right|Both/) {
	$x += ($off->{width} - $w);
	$can->create_line(
	  $x, $y, $x, $self->{y} + $off->{staffY} - ($scale * 3),
	  -width => $w, -fill => $Tab->{headColor}, -tags => $tag);
      }
    }
  }
}

1;
