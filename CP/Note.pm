package CP::Note;

# This file is part of Chordy.
#
###############################################################################
# Copyright (c) 2018 Ian Houlding
# All rights reserved.
# This program is free software.
# You can redistribute it and/or modify it under the same terms as Perl itself.
###############################################################################

use strict;

use CP::Cconst qw/:COLOUR :TAB/;
use CP::Global qw/:CHORD :TAB/;

# A Note is defined by the string that is played, the
# fret number and it's interval (timing) within the bar.
# A Note can be a single or compound entity.
# Compound entities include:
#                                         2nd parameter
#  s = Slide (up or down)   1(5,8s7,12)  fret at which interval to slide to
#  h = Hammer (up or down)  3(3,0h5,1)   fret at which interval to hammer/pull
#  b = Bend                 4(7,0b1)     ammount of bend in semitones
#  r = Bend/Release         4(7,0r1,8)   ammount of bend and when to release
#  v = Vibrato              2(5,12v24)   when to end the vibrato
#
# The fret number may be preceeded with an 'f' in which
# case it will be displayed in a smaller font.
#
# A note can also be a Rest in which case the "string" will be the letter 'r'.
# The values inside () become (duration,interval) where duration is one of:
# 1 1/2 1/4 1/8 1/16 or 1/32
#
# The text Tab file has strings starting at 1 (low E) but
# internally, strings start at 0.
#
sub new {
  my($proto,$bar,$string,$def) = @_;

  my $class = ref($proto) || $proto;
  my $self = {};
  $self->{bar} = $bar;
  $self->{font} = 'Normal';
  $self->{op} = '';
  if ($string =~ /r/i) {
    # Got a rest.
    $self->{string} = 'r';
    if ($def =~ /([\d\/]+),(\d+)/) {
      $self->{pos} = $2;
      ($self->{fret} = $1) =~ s/1\///;
    }
  } else {
    $self->{string} = $string;
    if ($def =~ /(f)?([-]?[X\d]+),(\d+)([bhrsv])?(\d+)?,?([-]?\d+)?/) {
      $self->{font} = 'Small' if ($1 eq 'f');
      $self->{fret} = $2;
      $self->{pos} = $3;
      if (defined $4) {
	$self->{op} = lc($4);
	my($p5,$p6) = ($5,$6);
	if ($self->{op} =~ /s|h/) {
	  $self->{tofret} = $p5;
	  $self->{topos} = $p6;
	} elsif ($self->{op} =~ /b|r/) {
	  $self->{bend} = $p5;
	  $self->{release} = $p6 if ($self->{op} eq "r");
	} elsif ($self->{op} eq "v") {
	  $self->{topos} = $p5;
	}
      }
    }
  }
  bless $self, $class;
  return($self);
}

sub printnote {
  my($self) = shift;

  if ($self->{string} =~ /r/i) {
    printf "  %2s %2d %2d\n", $self->{string}, $self->{fret}, $self->{pos};
  } else {
    printf "  %2d %2d %2d\n", $self->{string}, $self->{fret}, $self->{pos};
  }
}

# The Note's new string and pos have been set.
sub move {
  my($self) = @_;

  my $bar = $self->{bar};
  my $can = $bar->{canvas};
  my $off = $bar->{offset};
  my($ox,$oy) = $can->coords($self->{id});
  my $x = $bar->{x} + $off->{pos0} + ($off->{interval} * $self->{pos});
  my $y = $bar->{y} + $off->{staff0} - ($off->{staffSpace} * $self->{string}) - $off->{scale};
  my $dx = $x - $ox;
  my $dy = $y - $oy;
  $can->move($self->{id}, $dx, $dy);
  if ($self->{op} ne '') {
    $can->move("$self->{op}$self->{id}", $dx, $dy);
    $self->{topos} += ($dx / $off->{interval});
  }
}

#
# Show a fret position on the Page or Edit bar.
#
sub show {
  my($self,$tag) = @_;

  my $bar  = $self->{bar};
  my $can  = $bar->{canvas};
  my $pidx = $bar->{pidx};
  my $off  = $bar->{offset};
  my($y,$id,$fnt,$clr);
  my $x = $bar->{x} + $off->{pos0} + ($off->{interval} * $self->{pos});
  my $hh = $off->{staffHeight} / 2;
  my $ss = $off->{staffSpace};
  if ($self->{string} eq 'r') {
    # Rest
    my $num = $self->{fret};
    my $yadd = ($num == 1) ? $ss : ($num == 2) ? $ss * 2 : $hh;
    $fnt = ($pidx < 0) ? $Tab->{esymFont} : $Tab->{symFont};
    $y = $bar->{y} + $off->{staffY} + $yadd;
    $id = $can->create_text(
      $x,$y,
      -text => chr(64+$num),
      -font => $fnt,
      -fill => BLACK,
      -tags => $tag);
  } else {
    my $fr;
    if ($self->{fret} ne 'X') {
      $fr = abs($self->{fret});
    } else {
      $fr = 'X';
      $clr = BLACK;
    }
    if ($pidx >= 0) {
      $fnt = ($self->{font} eq 'Normal' && $fr < 10) ? $Tab->{noteFont} : $Tab->{snoteFont};
    } else {
      $fnt = ($self->{font} eq 'Normal' && $fr < 10) ? $Tab->{enoteFont} : $Tab->{esnoteFont};
    }
    $clr = ($self->{font} eq 'Normal') ? $Tab->{noteColor} : $Tab->{snoteColor};
    $y = $bar->{y} + $off->{staff0} - ($ss * $self->{string});
    if ($self->{fret} eq 'X') {
      $fnt = CP::Tab::newFont($fnt, 0.8);
    }
    $clr = RED if ($self->{fret} < 0);
    # If no anchor is given, the text is centered on a line vertically
    # and horizontally through the middle of the fret number.
    $id = $can->create_text(
      $x,$y,
      -text => $fr,
      -font => $fnt,
      -fill => $clr,
      -tags => $tag);
  }
  if ($pidx < 0) {
    $tag = "e$id";
    $can->itemconfigure($id, -tags => $tag);
  }
  $self->{id} = $id;
  $self->{x} = $x;
  $self->{y} = $y;
  if ($self->{op} ne '') {
    if ($self->{op} =~ /s|h/) {
      slideHam($self, $fnt, $clr, $tag);
    } elsif ($self->{op} =~ /b|r/) {
      bendRel($self, $fnt, $clr, $tag);
    }
  }
  if ($pidx < 0) {
    $can->bind($id, "<Button-1>", sub{$bar->note_sel($id);});
    $can->bind($id, "<Button-3>", sub{$bar->note_del($id);});
    # For Macs
    $can->bind($id, "<Control-Button-1>", sub{$bar->note_del($id);});
  }
  return($id);
}

sub clear {
  my($self) = @_;

  my $can = $self->{bar}{canvas};
  $can->delete("e$self->{id}");
  if ($self->{op} ne '') {
    $can->delete("$self->{op}$self->{id}");
  }
}

sub slideHam {
  my($self,$fnt,$clr,$tag) = @_;

  my $bar = $self->{bar};
  my $can = $bar->{canvas};
  my $off = $bar->{offset};
  $tag = "$self->{op}$self->{id}" if ($off->{scale} > 1);
  my $y = $self->{y};
  my $hu = $off->{interval} / 2;
  my $ss = $off->{staffSpace};
  my $x = my $x1 = $self->{x};
  $x -= $hu;
  my $ends = ($self->{topos} == BARZERO || $self->{topos} == BAREND);
  if (! $ends) {
    $x1 += ($off->{interval} * ($self->{topos} - $self->{pos}));
    my $id = $can->create_text(
      0, 0,
      -text => $self->{tofret},
      -font => $fnt,
      -fill => $clr,
      -tags => $tag,
      -anchor => 'n');
    my($xa,$ya,$xb,$yb) = split(/ /, $can->bbox($id));
    $can->coords($id, $x1, $y - ($yb / 2));
    if ($off->{scale} > 1) {
      $can->bind($id, "<Button-1>", sub{$bar->note_sel($self->{id});});
      $can->bind($id, "<Button-3>", sub{$bar->note_del($self->{id});});
      # For Macs
      $can->bind($id, "<Control-Button-1>", sub{$bar->note_del($self->{id});});
    }
  } else {
    $x  += ($off->{interval} * ($self->{topos} - $self->{pos}));
  }
  $x1 += $hu;
  my $y1 = $y;
  if ($self->{op} eq 's') {
    if ($ends) {
      $y -= ($ss * 0.6);
      $y1 -= ($ss * 0.6);
    } else {
      $y  -= ($self->{fret} < $self->{tofret}) ? ($ss * 0.6) : $ss;
      $y1 -= ($self->{fret} < $self->{tofret}) ? $ss : ($ss * 0.6);
    }
    $can->create_line(
      $x, $y, $x1, $y1,
      -fill => $Tab->{headColor},
      -width => $off->{thick}, -tags => $tag);
  } else {
    $y -= ($ss * 0.1);
    $y1 -= $ss;
    $can->create_arc(
      $x, $y, $x1, $y1,
      -extent => 180,
      -style => 'arc',
      -outline => $Tab->{headColor},
      -width => $off->{thick}, -tags => $tag);
  }
}

sub bendRel {
  my($self,$fnt,$clr,$tag) = @_;

  my $bar = $self->{bar};
  my $can = $bar->{canvas};
  my $off = $bar->{offset};
  $tag = "$self->{op}$self->{id}" if ($off->{scale} > 1);
  my $bi = $off->{interval};
  my $ss = $off->{staffSpace};
  my $y = $bar->{y} + $off->{staff0} - (($ss * $self->{string}) + ($ss * 0.6));
  my $x = $bar->{x} + $off->{pos0} + ($bi * $self->{pos});
  my $sc = $off->{scale};
  $sc++ if ($sc == 1);
  if ($self->{op} eq 'b') {
    $can->create_arc(
      $x - ($bi * 2), $y, $x + $bi, $y - $ss,
      -start => 270,
      -style => 'arc',
      -outline => $Tab->{headColor},
      -width => $sc, -tags => $tag);
  } else {
    my $ht = $ss * 1.2;
    my $hss = $ss * 0.6;
    my $clr = $Tab->{headColor};
    $bi = (($self->{release} - $self->{pos}) * $bi) / 3;
    if ($self->{pos} > BARZERO) {
      $can->create_arc(
	$x - $bi, $y, $x + $bi, $y - $ht,
	-start => 270,
	-style => 'arc',
	-outline => $clr,
	-width => $sc, -tags => $tag);
      $x += $bi;
      $can->create_rectangle(
	$x, $y - $hss + $sc, $x + $bi, $y - $hss,
	-width => 0,
	-fill => $clr,
	-tags => $tag);
    }
    if ($self->{release} < BAREND) {
      $y += $hss;
      $bi *= 2;
      $can->create_arc(
	$x, $y, $x + $bi - 1, $y - $ht + 1,
	-start => 0,
	-style => 'arc',
	-outline => $clr,
	-width => $sc, -tags => $tag);
      my $id = $can->create_text(
	0, 0,
	-text => $self->{fret},
	-font => $fnt,
	-fill => $clr,
	-tags => $tag,
	-anchor => 'n');
      my($xa,$ya,$xb,$yb) = $can->bbox($id);
      $can->coords($id, $x + $bi, $y - ($yb / 2));
    }
  }
}

1;
