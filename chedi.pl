#!/usr/bin/perl

# This file is part of Chordy.
#
###############################################################################
# Copyright (c) 2018 Ian Houlding
# All rights reserved.
# This program is free software.
# You can redistribute it and/or modify it under the same terms as Perl itself.
###############################################################################

BEGIN {
  if ($^O =~ /win32/i) {
    use FindBin 1.51 qw( $RealBin );
    use lib $RealBin;
    $ENV{PATH} = "C:\\Program Files\\Chordy\\Tcl\\bin;$ENV{PATH}";
  } elsif ($^O =~ /darwin/i) {
    use lib '/Applications/Chordy.app/lib';
  } else {
    use lib '/usr/local/lib/Chordy';
  }
}

use strict;
use warnings;

use CP::Cconst qw/:PATH/;
use CP::Global qw/:FUNC :WIN/;
use CP::CHedit;

use Getopt::Std;

our($opt_d);
getopts('d');

if (! -e ERRLOG) {
  open OFH, ">", ERRLOG;
  print OFH "Created: ".localtime."\n";
  close OFH;
}
if (!defined $opt_d) {
  open STDERR, '>>', ERRLOG or die "Can't redirect STDERR: $!";
  if ($^O eq 'darwin') {
    open STDOUT, ">&STDERR" or die "Can't dup STDOUT to STDERR: $!";
  }
}

CHedit('Save');

$SIG{CHLD} = sub {wait if (shift eq "CHLD");};

$MW->g_wm_deiconify();
$MW->g_raise();
Tkx::MainLoop();
