# Chordy
ChordPro manager and PDF generator

This started out as a need to be able to transpose ChordPro files as our vocalist kept wanting (needing) to shift keys.  
The sheet music handler on my tablet works with ChordPro files but the appearance was not very elegant so things sort  
of grew ... and grew ... and grew.  
The end result is Chordy - written in Perl and originally with a Tk GUI which recently migrated to Tcl/Tk so that it  
could be ported to a Mac environment.  
The Windows version runs standalone and does not need Perl installed (it uses the PAR packer to accumulate everything  
into a single package).  
The Mac version relies on Perl already being installed and also requires a copy of Active State Tcl to be installed.  
A copy of which is included in the .dmg image.
Both the Windows and the Mac installers come in a Release and an Upgrade version. Both will install a runable version  
of Chordy but the Release version also includes a number of ChordPro files - about half of which are only text files  
and have no ChordPro directives or Chords in them.  
  
Also included is a Tab editor - my first pass at writing one so it's fairly basic.  
  
If you want to play around with the code you'll need all the Perl .pl files plus all the modules in the CP folder.  
On top of that you'll need a number of extra Perl modules installed like Tkx - PDF::API2 - Font::TTF:Font - you'll  
soon find out what's missing :-)  
  
